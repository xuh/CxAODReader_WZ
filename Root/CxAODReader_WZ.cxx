#include "CxAODReader_WZ/CxAODReader_WZ.h"
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <string>
#include "CxAODTools_WWW/CommonProperties_WWW.h"
#include "CxAODTools_WWW/WWW2lepEvtSelection.h"
#include "CxAODTools/EventSelection.h"
#include "EventLoop/OutputStream.h"
#include "MuonEfficiencyCorrections/MuonTriggerScaleFactors.h"
#include "SampleHandler/DiskOutputLocal.h"
#include "SampleHandler/DiskOutputXRD.h"
#include "TSystem.h"
#include "CxAODReader_WZ/PreEvtSelection.h"

// this is needed to distribute the algorithm to the workers
ClassImp(CxAODReader_WZ)  //; - comment required for proper auto-formatting

    // bool m_debug = true;

    //Compare two jets pT
    bool compare_jet_pt(const xAOD::Jet* jet1, const xAOD::Jet* jet2) {
  return jet1->pt() > jet2->pt();
}

bool compare_WWW_pt(WWWParticle lep1, WWWParticle lep2) { return lep1.p4.Pt() > lep2.p4.Pt(); }
// Prompt Lepton Veto Working Points
// https://twiki.cern.ch/twiki/bin/view/AtlasProtected/RecommendedIsolationWPs
// Only consider lepton pT > 20GeV
bool PLVTight(float plv, float pt, ParticleType type, float iso) {
  pt = pt / 1e3;  // change MeV to GeV
  iso = iso / 1e3;

  if (!(iso < max(1.8, 0.15 * pt))) return false;
  if (plv <= -1.1) return false;

  if (type == ParticleType::ELECTRON) {
    if (pt >= 16.967 && plv < max(-0.88, -0.881497 + 2.29469 * exp(-pt / 11.5776))) return true;
  }
  if (type == ParticleType::MUON) {
    if (pt >= 18.603 && plv < max(-0.88, -0.929774 + 2.9159 * exp(-pt / 10.2339))) return true;
  }
  return false;
}

bool PLVLoose(float plv, float pt, ParticleType type, float iso) {
  pt = pt / 1e3;  // change MeV to GeV
  iso = iso / 1e3;

  if (!(iso < max(1.8, 0.15 * pt))) return false;
  if (plv <= -1.1) return false;

  if (type == ParticleType::ELECTRON) {
    if (pt >= 18.457 && plv < max(-0.88, -0.94386 + 3.03257 * exp(-pt / 28.0508))) return true;
  }
  if (type == ParticleType::MUON) {
    if (pt >= 18.452 && plv < max(-0.88, -0.958651 + 3.54785 * exp(-pt / 19.6155))) return true;
  }
  return false;
}

//Main CutFlow
EL::StatusCode CxAODReader_WZ::fill_WZ() {
  //Set the Variation (Variation is Systematic)
  m_tree->SetVariation(m_currentVar);  // Where is m_currentVar defined?
  m_tree->Reset();                     // What does this do?

  //Getting Event Info
  const xAOD::EventInfo* eventInfo = m_eventInfoReader->getObjects("Nominal");
  // string m_whichData = "2015";
  // m_config->getif< string >( "whichData", m_whichData ); // what does this do?
  // bool m_splitData = false; // what does this do?
  // m_config->getif< bool >( "splitData", m_splitData ); // what does this do?
  

  m_tree->weightMC = m_weight;  // get weight from ??
  std::vector<std::string> m_csVariations;
  m_config->getif<std::vector<std::string> >("csVariations", m_csVariations);

  if (m_isMC) {
    int DSID = m_eventInfo->mcChannelNumber();

    //If DSID matches Sherpa 2.2.1 sample then proceed with systematic weight application
    if (m_EvtWeightVar_WWW->isSherpaWZ(DSID)) {
      //---------------------------------------------------------
      //Check that the variations stored within csVariations vector, are present within the systematic variations stored within the event weight container
      for (auto& csVar : m_csVariations) {
        //Check that the EvtWeightVariations object contains a relevant systematic
        if (m_EvtWeightVar_WWW->hasVariation(csVar)) {
          //Extract the alternative weight
          float AltWeight = m_EvtWeightVar_WWW->getWeightVariation(m_eventInfo, csVar);

          //HistSvc.cxx applies m_weightSystsVar entries as a multiplicative factor
          //Therefore insert ratio of AltWeight/NomWeight (Alternative MC weight/Nominal MC weight)
          m_weightSysts.push_back({csVar + "__1up", AltWeight / Props::MCEventWeight.get(m_eventInfo)});

        }  // Logic End --->>> Cross-check valid Sherpa scale variation with csVariations vector

      }  // Loop End --->>> csVariations config vector search end
      //---------------------------------------------------------
    }
  }

  // int DSID = m_eventInfo->mcChannelNumber();
  // // std::cout<<std::endl;
  // if(m_isMC && m_currentVar == "Nominal"){
  //   if( m_EvtWeightVar->isSherpaVJets221( DSID ) && m_isMC){
  //     //std::cout<<"doing weight var"<<std::endl;
  //     for ( auto& csVar : m_EvtWeightVar->getListOfVariations() ){
  // 	if( m_EvtWeightVar->hasVariation(csVar) ){
  // 	  float WeightVar = m_EvtWeightVar->getWeightVariation(m_eventInfo, csVar);
  // 	  // std::cout<<csVar<<std::endl;
  // 	  m_weightSysts.push_back({ csVar, WeightVar / Props::MCEventWeight.get(m_eventInfo) });

  // 	}
  //     }
  //   }
  // }
  // std::cout<<std::endl;

  //Swtich to only get 2015 or 2016 data, controlled from config (Including MC samples)

  /// This is not used
  // if (m_isMC && m_splitData) {
  //   if (m_whichData == "2015" && Props::RandomRunNumber.get(eventInfo) > 296939) {
  //     //std::cout<<"event skipped"<<std::endl;
  //     return EL::StatusCode::SUCCESS;
  //   } else if (m_whichData == "2016" && Props::RandomRunNumber.get(eventInfo) < 296939 && Props::RandomRunNumber.get(eventInfo) >= 297730)
  //     return EL::StatusCode::SUCCESS;
  //   else if (m_whichData == "2017" && Props::RandomRunNumber.get(eventInfo) < 297730)
  //     return EL::StatusCode::SUCCESS;
  // }

  //Get Objects
  DBResult selectionResult = ((PreEvtSelection*)m_eventSelection)->result();
  std::vector<const xAOD::Electron*> electron = selectionResult.el;
  std::vector<const xAOD::Electron*> forwardelectron = selectionResult.fwdel;
  std::vector<const xAOD::Muon*> muon = selectionResult.mu;
  std::vector<const xAOD::TauJet*> tau = selectionResult.ta;
  const xAOD::MissingET* met = selectionResult.met;
  //const xAOD::MissingET* mpt = selectionResult.mpt;
  std::vector<const xAOD::Jet*> smallJets = selectionResult.smallJets;
  //std::vector<const xAOD::TrackParticle*> TrackPart = selectionResult.TrackPart;
  // std::vector< const xAOD::Photon* > Photons = selectionResult.Photons;
  // std::vector< const xAOD::TruthParticle* > truthParticles = selectionResult.truthParticles;

  // std::cout<<"Number of Track Particles is "<<TrackPart.size()<<std::endl;
  // std::vector<const xAOD::TrackParticle*> TrackPartGood;
  // for(unsigned int iTrack=0; iTrack<TrackPart.size(); iTrack++)
  //   {

  //     TLorentzVector TrackVec;
  //     TrackVec.SetPtEtaPhiM(Props::pt.get(TrackPart[iTrack]),Props::eta.get(TrackPart[iTrack]),Props::phi.get(TrackPart[iTrack]),Props::m.get(TrackPart[iTrack]));

  //     bool Overlap=false;

  //     for(unsigned int iJet=0; iJet< smallJets.size() && !Overlap; iJet++){
  // 	TLorentzVector jetVec = smallJets[iJet]->p4();
  // 	if (jetVec.DeltaR(TrackVec)<0.4) Overlap=true;
  //     }

  //     for(unsigned int ilep=0; ilep< electron.size() && !Overlap; ilep++){
  // 	TLorentzVector lepVec = electron[ilep]->p4();
  // 	if (lepVec.DeltaR(TrackVec)<0.2) Overlap=true;
  //     }

  //     for(unsigned int ilep=0; ilep< muon.size() && !Overlap; ilep++){
  // 	TLorentzVector lepVec = muon[ilep]->p4();
  // 	if (lepVec.DeltaR(TrackVec)<0.2) Overlap=true;
  //     }

  //     if(!Overlap)
  // 	TrackPartGood.push_back(TrackPart[iTrack]);

  //   }
  // std::cout<<"Number of Track Particles is Good "<<TrackPartGood.size()<<std::endl;
  // if(TrackPartGood.size()>0)     return EL::StatusCode::SUCCESS;

  // std::cout<<"Number of Photons "<<Photons.size()<<std::endl;
  // if( Photons.size()>0 ) return EL::StatusCode::SUCCESS;

  // Truth information

  unsigned int Ntruthtau = 0;

  // if ( m_isMC ) {
  //   for ( auto itpar : truthParticles ) {
  //     m_tree->truth_PdgId.push_back( itpar->pdgId() ); // is this needed?
  //     if ( abs( itpar->pdgId() ) == 15 ) Ntruthtau++;
  //   }
  // }

  if (m_isMC)
    m_tree->runNumber = eventInfo->mcChannelNumber();  //datasetid;
  else
    m_tree->runNumber = eventInfo->runNumber();  //datasetid;

  m_tree->eventNumber = eventInfo->eventNumber();

  ////////////////////////////////////////
  //              Jets                  //
  ////////////////////////////////////////

  std::vector<const xAOD::Jet*> goodJets;
  std::vector<const xAOD::Jet*> bJets;
  std::vector<const xAOD::Jet*> bJets77;
  TLorentzVector jetVec;

  //bool found_badjet = false;
  for (unsigned int iJet = 0; iJet < smallJets.size(); iJet++) {
    jetVec = smallJets[iJet]->p4();

//  	bool isNotBadJet = !EventSelection::passJetCleaning(smallJets.at(iJet), m_eventInfo);

    m_tree->AllJetLepOverlap += Props::nrMuonInJet.get(smallJets.at(iJet)) + Props::nrElectronInJet.get(smallJets.at(iJet));

    //Search for BadJets and if found remove the event.
    // TODO: fix magic numbers
    // TODO: just break if bad jet is found
    // if (jetVec.Pt() > 20.e3 && jetVec.Pt() < 60.e3 && !Props::goodJet.get(smallJets[iJet]) &&
    //     (fabs(jetVec.Eta()) > 2.4 || Props::PassJvtMedium.get(smallJets[iJet]))) {
    //   found_badjet = true;
    // }
    // if (jetVec.Pt() > 60.e3 && !Props::goodJet.get(smallJets[iJet])) {
    //   found_badjet = true;
    // }
    // if (found_badjet) {
    //   break;
    // }

    //if( !Props::passOR.get(smallJets[iJet]) ) continue;

    // //Jet Defenition
    // // PT>20 GeV and Eta<4.5 and IsGOODJet. If Eta<2.5 or PT<60 do Clean JVT
    // if (jetVec.Pt() > 20.e3 && fabs(jetVec.Eta()) < 4.5 && Props::goodJet.get(smallJets[iJet]))
    // // && (fabs( jetVec.Eta() )>2.4 || Props::PassJvtMedium.get(smallJets[iJet]) || jetVec.Pt()>60.e3) )
    // {
    //   //If jet is btagged count it.
    //   BTagProps::tagWeight.set(smallJets[iJet], Props::MV2c10.get(smallJets[iJet]));
    //   if (Props::MV2c10.get(smallJets[iJet]) > 0.11 && fabs(jetVec.Eta()) < 2.5) {
    //     BTagProps::isTagged.set(smallJets[iJet], 1);
    //     bJets.push_back(smallJets[iJet]);
    //   } else {
    //     BTagProps::isTagged.set(smallJets[iJet], 0);
    //   }

    //   if (Props::MV2c10.get(smallJets[iJet]) > 0.64 && fabs(jetVec.Eta()) < 2.5) {
    //     bJets77.push_back(smallJets[iJet]);
    //   }
    //   goodJets.push_back(smallJets[iJet]);
    // }

    // Jet should passJVT and good jet if signal jet defined in CxAODMaker
    if (Props::isSignalJet.get(smallJets[iJet])) {
      // btag 85%

      if (BTagProps::isTagged.get(smallJets[iJet])) bJets.push_back(smallJets[iJet]);

      // test
      // check tag weight consitency
      float tagWeight = BTagProps::tagWeight.get(smallJets[iJet]);
      // float tagWeight1 = m_bTagTool->getTaggerWeight(*smallJets[iJet]);
      // if (tagWeight != tagWeight1){
      //   std::cout<<"different b tag weight"<<std::endl;
      //   std::cout<< "BTagProps: "<< tagWeight << "; m_bTagTool: "<< tagWeight1<<std::endl;
      // }
      // btag 77%
      // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/BTaggingBenchmarksRelease21#DL1rnn_tagger_AN3
      if (tagWeight > 2.20 && fabs(jetVec.Eta()) < 2.5) {
        bJets77.push_back(smallJets[iJet]);
      }
      goodJets.push_back(smallJets[iJet]);
    }
  }
  //JVT Scale Factor
  if (m_isMC) {
    m_weight *= Props::JvtSF.get(eventInfo);
    m_tree->weightJVT *= Props::JvtSF.get(eventInfo);
  }
  //
  // float bWeight = computeBTagSFWeight(goodJets, "AntiKt4EMPFlowJets");
  // float bWeight = computeBTagSFWeight(goodJets, m_jetReader->getContainerName());
  // TO-DO: btag SF is not applied. It is due to the probably currently with old CDI file
  // Will update when release update to 21.2.90 or after
  // std::cout<<"get SF: "<< bWeight<< std::endl;

  float bWeight = 1.;
  m_weight *= bWeight;
  m_tree->weightBtag *= bWeight;

  // if(m_isMC && bJets.size()>0) m_weight*= computeBTagSFWeight(bJets,m_jetReader->getContainerName());
  // if(m_isMC && bJets.size()>0) m_tree->weightBtag*= computeBTagSFWeight(bJets,m_jetReader->getContainerName());

  //Pt Sorting Jets
  int jet_num = goodJets.size();
  std::sort(goodJets.begin(), goodJets.end(), compare_jet_pt);
  std::sort(bJets.begin(), bJets.end(), compare_jet_pt);

  // // bad jet veto
  // if (found_badjet) {  // there is a break above
  //   if (m_debug) Info("fill_1Lep()", "Failed bad jet veto");
  //   return EL::StatusCode::SUCCESS;
  // }

  ////////////////////////////////////////
  //            leptons                 //
  ////////////////////////////////////////
  std::vector<const xAOD::TauJet*> TauSel;

  std::vector<const xAOD::Muon*> muon_loose;
  std::vector<const xAOD::Muon*> muon_tight;
  std::vector<const xAOD::Muon*> MuonSel;
  std::vector<const xAOD::Electron*> ElectronSel;
  std::vector<const xAOD::Electron*> Electron_Z;
  std::vector<const xAOD::Electron*> Electron_W;
  std::vector<const xAOD::IParticle*> ele_loose;  // why are these vectors of IPArticle and not Electron?
  std::vector<const xAOD::IParticle*> ele_NoBL;

  ////////////////////////////////////////
  //              Taus                  //
  ////////////////////////////////////////

  unsigned tauAll_num = 0;
  unsigned tauTight_num = 0;
  unsigned tauMedium_num = 0;
  unsigned tauLoose_num = 0;

  // count number of loose, tight, medium taus
  for (unsigned int iTau = 0; iTau < tau.size(); iTau++) {
    //if(!Props::passOR.get(tau[iTau])) continue;
    tauAll_num++;
    TauSel.push_back(tau[iTau]);
    // TODO: this is akward
    if (!Props::isBDTLoose.get(tau[iTau])) {
      continue;
    } else {
      tauLoose_num++;
    }
    if (!Props::isBDTMedium.get(tau[iTau])) {
      continue;
    } else {
      tauMedium_num++;
    }
    if (!Props::isBDTTight.get(tau[iTau])) {
      continue;
    } else {
      tauTight_num++;
    }
  }

  ////////////////////////////////////////
  //              Muons                 //
  ////////////////////////////////////////

  float lepWeight = 1;
  unsigned muon_num = 0;
  unsigned muonLoose_num = 0;
  // unsigned muonTight_num = 0;
  int muonsize = 0;
  int muonsize_z = 0;
  int muonsize_w = 0;
  float vetoMuonPt = 0;
  float vetoMuonEta = 0;
  float vetoMuonPhi = 0;

  int muonCS = 0;
  int muonCS_z = 0;

  for (unsigned int iMuon = 0; iMuon < muon.size(); iMuon++) {
    bool isTight = false;
    bool isLoose = false;
    //Second on is isolation
    if (Props::isVetoMuon.get(muon[iMuon]) ){//&& muon[iMuon]->pt() > 4500) {
      muonsize++;
	  if(Props::isLooseMuon.get(muon[iMuon])) {
		  muonsize_z++;
      	  muonCS_z += muon[iMuon]->charge();
		  if(Props::isSignalMuon.get(muon[iMuon])) {
			  muonsize_w++;
		  }
	  }
    } else if (Props::isVetoMuon.get(muon[iMuon])) {
      if (vetoMuonPt == 0 || vetoMuonPt > muon[iMuon]->pt()) {
        vetoMuonPt = muon[iMuon]->pt();
        vetoMuonEta = muon[iMuon]->eta();
        vetoMuonPhi = muon[iMuon]->phi();
      }
    }

    //if((Props::isSignalMuon.get(muon[iMuon]) && Props::isGradientIso.get(muon[iMuon]))) isTight=true;
    if (Props::isSignalMuon.get(muon[iMuon]) && PLVTight(Props::PromptLeptonVetoMuon.get(muon[iMuon]), fabs(muon[iMuon]->pt()),
                                                         ParticleType::MUON, Props::ptvarcone30_TightTTVA_pt1000.get(muon[iMuon]))
        // &&Props::PromptLeptonVetoMuon.get(muon[iMuon]) < -0.8 &&
        // && Props::isFCLoose_FixedRadIso.get( muon[iMuon] )
    ) {
      isTight = true;
    } else if (Props::isLooseMuon.get(muon[iMuon])) {
      isLoose = true;
    }

    //if (isTight || isLoose) {
    if (Props::isLooseMuon.get(muon[iMuon])) {
      MuonSel.push_back(muon[iMuon]);
      ++muon_num;
      // Leptons.push_back(muon[iMuon]);
      // lCharge.push_back(muon[iMuon]->charge());
      WWWParticle tmpLep;

      // JL: testing
      if (m_isMC) {
        tmpLep.isPrompt = Props::isPrompt.get(muon[iMuon]);
        tmpLep.isFake = Props::isFake.get(muon[iMuon]);
        tmpLep.truthStatus = Props::truthStatus.get(muon[iMuon]);
      }

      tmpLep.p4 = muon[iMuon]->p4();
      tmpLep.isZ = Props::isLooseMuon.get(muon[iMuon]);
      tmpLep.isW = Props::isSignalMuon.get(muon[iMuon]);
      tmpLep.innerTrackPt = Props::innerTrackPt.get(muon[iMuon]);

      //used for isolation WP
      tmpLep.PtCone20 = Props::ptcone20.get(muon[iMuon]);
      tmpLep.TopoEtCone20 = Props::topoetcone20.get(muon[iMuon]);
      tmpLep.PtVarCone30_TightTTVA_pt1000 = Props::ptvarcone30_TightTTVA_pt1000.get(muon[iMuon]);
      //Not used
      tmpLep.PtVarCone20 = Props::ptvarcone20.get(muon[iMuon]);
      tmpLep.PtVarCone30 = Props::ptvarcone30.get(muon[iMuon]);
      tmpLep.PtVarCone40 = Props::ptvarcone40.get(muon[iMuon]);
      //Not implemented in Maker
      tmpLep.PtVarCone20_TightTTVA_pt1000 = -999;

      tmpLep.d0 = Props::d0sigBL.get(muon[iMuon]);
      tmpLep.PromptVar = Props::PromptLeptonVetoMuon.get(muon[iMuon]);
      tmpLep.Type = ParticleType::MUON;
      tmpLep.isLoose = isLoose;
      tmpLep.charge = muon[iMuon]->charge();

      if (isTight || isLoose) {
        if ((!m_isMC && eventInfo->runNumber() >= 296939) || (m_isMC && Props::RandomRunNumber.get(eventInfo) >= 296939)) //2026-18
          tmpLep.TrigMatch = Props::matchHLT_mu26_ivarmedium.get(muon[iMuon]) || Props::matchHLT_mu50.get(muon[iMuon]);
        else
          tmpLep.TrigMatch = Props::matchHLT_mu20_iloose_L1MU15.get(muon[iMuon]) || Props::matchHLT_mu50.get(muon[iMuon]);
      }
      m_tree->Leptons.push_back(tmpLep);

      muonCS += muon[iMuon]->charge();

      if (m_isMC) {
        if (isTight) {
          lepWeight *= Props::mediumEffSF.get(muon[iMuon]);
          m_tree->weightEffSF *= Props::mediumEffSF.get(muon[iMuon]);

          //lepWeight *= Props::fCLoose_FixedRadIso_PLVSF.get(muon[iMuon]);
          //m_tree->weightIso *= Props::fCLoose_FixedRadIso_PLVSF.get(muon[iMuon]);
          // lepWeight *= Props::fCLoose_FixedRadIsoSF.get(muon[iMuon]);
          // m_tree->weightIso *= Props::fCLoose_FixedRadIsoSF.get(muon[iMuon]);

          lepWeight *= Props::TTVAEffSF.get(muon[iMuon]);
          m_tree->weightTTVA *= Props::TTVAEffSF.get(muon[iMuon]);

        } else {
          lepWeight *= Props::mediumEffSF.get(muon[iMuon]);
          m_tree->weightEffSF *= Props::mediumEffSF.get(muon[iMuon]);
          lepWeight *= Props::TTVAEffSF.get(muon[iMuon]);
          m_tree->weightTTVA *= Props::TTVAEffSF.get(muon[iMuon]);
        }
      }

      if (isLoose) {
        ++muonLoose_num;
        muon_loose.push_back(muon[iMuon]);
      }
    }
  }

  m_tree->vetoMuonPt = vetoMuonPt;
  m_tree->vetoMuonEta = vetoMuonEta;
  m_tree->vetoMuonPhi = vetoMuonPhi;

  // bool MuTrig=false;
  // for( auto Lep: m_tree->Leptons)
  //   {
  //     if (Lep.Type!=ParticleType::MUON) continue;
  //     MuTrig = MuTrig || (Lep.TrigMatch);
  //   }

  // if(m_isMC && MuTrig)
  //   {
  //     double tmpMuonSF;
  //     m_muonTrigSF->getTriggerScaleFactor(*MuonSel2.asDataVector(),tmpMuonSF,"HLT_mu26_ivarmedium_OR_HLT_mu50");
  //     lepWeight*=tmpMuonSF;
  //     m_tree->weightTrig*=tmpMuonSF;
  //   }

  ////////////////////////////////////////
  //            Electrons               //
  ////////////////////////////////////////

  unsigned elec_num = 0;
  // unsigned elecTight_num = 0;
  unsigned elecLoose_num = 0;
  unsigned elecNoBL_num = 0;

  int elecCS = 0;
  int elecCS_z = 0;
  int electronsize = 0;
  int electronsize_z = 0;
  int electronsize_w = 0;
  float vetoElectronPt = 0;
  float vetoElectronEta = 0;
  float vetoElectronPhi = 0;

  // bool passAuthor = true;
  // bool passPhoton = false;

  for (unsigned int iEl = 0; iEl < electron.size(); iEl++) {
    //if( ! Props::passOR.get(electron[iEl]) )continue;
// for WZ VBS
    if (Props::isVetoElectron.get(electron[iEl])){// && electron[iEl]->pt() > 7e3) {
      electronsize++;
      if (Props::isLooseElectron.get(electron[iEl])) {
        electronsize_z++;
      	elecCS_z += electron[iEl]->charge();
		if (Props::isSignalElectron.get(electron[iEl])) {
			electronsize_w++;
		}
      }
    } else if (Props::isVetoElectron.get(electron[iEl])) {
      if (vetoElectronPt == 0 || vetoElectronPt > electron[iEl]->pt()) {
        vetoElectronPt = electron[iEl]->pt();
        vetoElectronEta = electron[iEl]->eta();
        vetoElectronPhi = electron[iEl]->phi();
      }
    }

    //Isolation Cut
    bool isTight = false;
    bool isLoose = false;
    bool isNoBL = false;
    //if(Props::isSignalElectron.get(electron[iEl]) && Props::isGradientIso.get(electron[iEl]) && Props::Author.get(electron[iEl]) == 1) isTight=true;
    //if((Props::isSignalElectron.get(electron[iEl]) && Props::PromptLeptonVeto.get(electron[iEl])<-0.7 && Props::ECIDS_accept.get(electron[iEl]) && Props::isFixedCutLooseIso.get(electron[iEl]) && Props::Author.get(electron[iEl]) == 1 )) isTight=true;
    if (Props::isSignalElectron.get(electron[iEl]) &&
        PLVTight(Props::PromptLeptonVetoEl.get(electron[iEl]), fabs(electron[iEl]->pt()), ParticleType::ELECTRON,
                 Props::ptvarcone20_TightTTVA_pt1000.get(electron[iEl])) &&
        //Props::ECIDSResult.get(electron[iEl]) > 0.7 &&
        Props::ECIDS.get(electron[iEl]) && Props::Author.get(electron[iEl]) == 1 && Props::addAmbiguity.get(electron[iEl]) <= 0
        //&& Props::isFixedCutLooseIso.get(electron[iEl])
    ) {
      isTight = true;
    } else if (Props::isLooseElectron.get(electron[iEl]) &&
               //PLVLoose(Props::PromptLeptonVetoEl.get(electron[iEl]), fabs(electron[iEl]->pt()), ParticleType::ELECTRON,
               // Props::ptvarcone30_TightTTVALooseCone_pt1000.get(electron[iEl])) &&
               // Props::ECIDS.get(electron[iEl]) &&
               Props::Author.get(electron[iEl]) == 1 && Props::addAmbiguity.get(electron[iEl]) <= 0) {
      isLoose = true;
    } else if (Props::isNoBLElectron.get(electron[iEl]) && !Props::isTightLH.get(electron[iEl]) &&
               PLVTight(Props::PromptLeptonVetoEl.get(electron[iEl]), fabs(electron[iEl]->pt()), ParticleType::ELECTRON,
                        Props::ptvarcone20_TightTTVA_pt1000.get(electron[iEl])) &&
               Props::ECIDS.get(electron[iEl])) {
      isNoBL = true;
    }

    //if (isTight || isLoose || isNoBL) {
    if (Props::isLooseElectron.get(electron[iEl])) {
      ElectronSel.push_back(electron[iEl]);
      //Count Electron number and push it back in Lepton object as well as m_tree.
      ++elec_num;
      // Leptons.push_back(electron[iEl]);
      // lCharge.push_back(electron[iEl]->charge());
      WWWParticle tmpLep;

      // JL: testing
      if (m_isMC) {
        tmpLep.isPrompt = Props::isPrompt.get(electron[iEl]);
        tmpLep.isFake = Props::isFake.get(electron[iEl]);
        tmpLep.truthStatus = Props::truthStatus.get(electron[iEl]);
      }

      tmpLep.p4 = electron[iEl]->p4();
      tmpLep.isZ = Props::isLooseElectron.get(electron[iEl]);
      tmpLep.isW = Props::isSignalElectron.get(electron[iEl]);
      tmpLep.innerTrackPt = Props::innerTrackPt.get(electron[iEl]);

      // Used for isolation WP
      tmpLep.TopoEtCone20 = Props::topoetcone20.get(electron[iEl]);
      tmpLep.PtVarCone20 = Props::ptvarcone20.get(electron[iEl]);
      tmpLep.PtVarCone20_TightTTVA_pt1000 = Props::ptvarcone20_TightTTVA_pt1000.get(electron[iEl]);

      // Not used
      tmpLep.PtVarCone40 = -999.;
      tmpLep.PtVarCone30_TightTTVA_pt1000 = -999.;

      // The following is not available anymore for electron
      tmpLep.PtVarCone30 = -999.;
      tmpLep.PtCone20 = -999.;

      tmpLep.d0 = Props::d0sigBL.get(electron[iEl]);
      tmpLep.PromptVar = Props::PromptLeptonVetoEl.get(electron[iEl]);
      tmpLep.ECIDSBDT = Props::ECIDSResult.get(electron[iEl]);
      tmpLep.Type = ParticleType::ELECTRON;
      tmpLep.charge = electron[iEl]->charge();
      tmpLep.isLoose = isLoose;
      tmpLep.isNoBL = isNoBL;
      tmpLep.Author = Props::Author.get(electron[iEl]);
      tmpLep.addAmbiguity = Props::addAmbiguity.get(electron[iEl]);
      //passAuthor = passAuthor && Props::Author.get(electron[iEl])==1;
      //passPhoton = passPhoton || Props::Author.get(electron[iEl])==4;

      //Trigger Match
      if (isTight || isLoose) {
        if ((!m_isMC && eventInfo->runNumber() >= 296939) || (m_isMC && Props::RandomRunNumber.get(eventInfo) >= 296939))
        //if ((!m_isMC && eventInfo->runNumber() >= 296939) || (m_isMC && Props::RandomRunNumber.get(eventInfo) >= 296939))
        tmpLep.TrigMatch = Props::matchHLT_e26_lhtight_nod0_ivarloose.get(electron[iEl]) ||
                           Props::matchHLT_e60_lhmedium_nod0.get(electron[iEl]) || Props::matchHLT_e140_lhloose_nod0.get(electron[iEl]);
        else
          tmpLep.TrigMatch = Props::matchHLT_e24_lhmedium_L1EM20VH.get(electron[iEl]) || Props::matchHLT_e24_lhmedium_L1EM18VH.get(electron[iEl]) || Props::matchHLT_e60_lhmedium.get(electron[iEl]) || Props::matchHLT_e120_lhloose.get(electron[iEl]);
      }

      elecCS += electron[iEl]->charge();
      m_tree->Leptons.push_back(tmpLep);

      if (m_isMC) {
        if (isTight) {
          lepWeight *= Props::effSFReco.get(electron[iEl]);
          m_tree->weightEffReco *= Props::effSFReco.get(electron[iEl]);

          lepWeight *= Props::effSFtightLH.get(electron[iEl]);
          m_tree->weightEffSF *= Props::effSFtightLH.get(electron[iEl]);

          //lepWeight*=Props::effSFIsoGradientTightLH.get(electron[iEl]);
          //m_tree->weightIso*=Props::effSFIsoGradientTightLH.get(electron[iEl]);

          // lepWeight *= Props::effSFIsoFixedCutLooseTightLH.get(electron[iEl]);
          // m_tree->weightIso *= Props::effSFIsoFixedCutLooseTightLH.get(electron[iEl]);

        } else if (isNoBL) {
          lepWeight *= Props::effSFReco.get(electron[iEl]);
          m_tree->weightEffReco *= Props::effSFReco.get(electron[iEl]);

          lepWeight *= Props::effSFtightLH.get(electron[iEl]);
          m_tree->weightEffSF *= Props::effSFtightLH.get(electron[iEl]);

          //lepWeight*=Props::effSFIsoGradientTightLH.get(electron[iEl]);
          //m_tree->weightIso*=Props::effSFIsoGradientTightLH.get(electron[iEl]);

          // lepWeight *= Props::effSFIsoFixedCutLooseTightLH.get(electron[iEl]);
          // m_tree->weightIso *= Props::effSFIsoFixedCutLooseTightLH.get(electron[iEl]);

        } else {
          lepWeight *= Props::effSFReco.get(electron[iEl]);
          m_tree->weightEffReco *= Props::effSFReco.get(electron[iEl]);

          lepWeight *= Props::effSFmediumLH.get(electron[iEl]);
          m_tree->weightEffSF *= Props::effSFmediumLH.get(electron[iEl]);

          // lepWeight*=Props::effSFIsoFixedCutLooseMediumLH.get(electron[iEl]);
          // m_tree->weightIso*=Props::effSFIsoFixedCutLooseMediumLH.get(electron[iEl]);
        }
      }

      if (isLoose) {
        ++elecLoose_num;
        ele_loose.push_back(electron[iEl]);
      } else if (isNoBL) {
        ++elecNoBL_num;
        ele_NoBL.push_back(electron[iEl]);
      }
    }
  }
  m_tree->vetoElectronPt = vetoElectronPt;
  m_tree->vetoElectronEta = vetoElectronEta;
  m_tree->vetoElectronPhi = vetoElectronPhi;

  //WZ Electron Truth match CF //???
  //TODO: fix magic numbers; what does this do?
  if (m_isMC && (eventInfo->mcChannelNumber() == 364253 || eventInfo->mcChannelNumber() == 364284 ||
                 eventInfo->mcChannelNumber() == 364250 || eventInfo->mcChannelNumber() == 364283)) {
    const xAOD::TruthParticleContainer* truthParticle = nullptr;
    m_event->retrieve(truthParticle, "TruthParticles___Nominal");
    for (auto* itpar : *truthParticle) {
      if (abs(itpar->pdgId()) == 11) {
        if (itpar->p4().Pt() < 15.e3) continue;

        for (auto Lep : m_tree->Leptons) {
          if (Lep.Type != ParticleType::ELECTRON) continue;

          if (Lep.p4.DeltaR(itpar->p4()) < 0.4 && Lep.p4.Pt() * 0.5 < itpar->p4().Pt()) {
            if ((itpar->pdgId() * Lep.charge) == (11)) {
              //std::cout<<"killling "<<itpar->pdgId()<<" "<<Lep.charge<<" "<<Lep.p4.DeltaR(itpar->p4())<<" "<<Lep.p4.Pt()<<" "<<itpar->p4().Pt()<<" "<<itpar->p4().Eta()<<" "<<itpar->p4().Phi()<<std::endl;
              return EL::StatusCode::SUCCESS;
            }
          }
        }
      }
    }
  }

  const float Mz = 91.1876e3;
  float Mll_forwardelectron = 0.0;
  float forwardElPt = 0.0;
  float forwardElEta = 0.0;
  for (unsigned int iEl1 = 0; iEl1 < forwardelectron.size(); iEl1++) {
    //std::cout << "electron_forward(): pt" << forwardelectron[iEl1]->pt() << std::endl;
    //std::cout << "electron_forward(): eta" << forwardelectron[iEl1]->eta() << std::endl;
    for (unsigned int iEl2 = 0; iEl2 < ElectronSel.size(); iEl2++) {
      // OS
      if (forwardelectron[iEl1]->charge() * ElectronSel[iEl2]->charge() > 0) continue;
      // MZ
      TLorentzVector vMee = forwardelectron[iEl1]->p4() + ElectronSel[iEl2]->p4();
      if (fabs(vMee.M() - Mz) < fabs(Mll_forwardelectron - Mz)) {
        Mll_forwardelectron = vMee.M();
        forwardElPt = forwardelectron[iEl1]->pt();
        forwardElEta = forwardelectron[iEl1]->eta();
      }
      // comment out for check without forward electron
      //if (fabs(vMee.M()- Mz) > 15e3) continue;
      //else electron.push_back(forwardelectron[iEl1]);
    }
  }
  m_tree->Mll_forwardelectron = Mll_forwardelectron;
  m_tree->forwardElEta = forwardElEta;
  m_tree->forwardElPt = forwardElPt;

  ////////////////////////////////////////
  //           Systematics              //
  ////////////////////////////////////////

  CP::SystematicSet sysSet;
  m_triggerTool->applySystematicVariation(sysSet);
  m_triggerTool->setEventInfo(m_eventInfo, m_randomRunNumber);
  m_triggerTool->setElectrons({ElectronSel});
  m_triggerTool->setMuons({MuonSel});
  m_triggerTool->setMET(met);

  bool passTrig = false;

  double triggerSF_nominal = 1;
  //std::cout<<"Get Trigger SF begin..."<<std::endl;
  if ((!m_isMC && eventInfo->runNumber() > 0) || (m_isMC && Props::RandomRunNumber.get(eventInfo) > 0)) {
    passTrig = m_triggerTool->getDecisionAndScaleFactor(triggerSF_nominal);  // TODO: look into what this function returns
  }
  //std::cout<<"Get Trigger SF end..."<<std::endl;
  m_tree->weightTrig = triggerSF_nominal;
  lepWeight *= triggerSF_nominal;

  //bool passTrigMatch = true;
  bool passTrigMatch = false;  // JL: changed from above
  for (auto Lep : m_tree->Leptons) {
    passTrigMatch = (passTrigMatch || Lep.TrigMatch);
  }

  m_config->getif<vector<string> >("triggerSystList", m_triggerSystList);

  if (m_isMC && (m_currentVar == "Nominal")) {
    for (size_t i = 0; i < m_triggerSystList.size(); i++) {
      if (muon_num == 0 || (m_triggerSystList.at(i).find("MUON_EFF_Trig") == std::string::npos)) {
        //std::cout<<"continue "<<m_triggerSystList.at(i)<<" muon num "<<muon_num<<" elec num "<<elec_num<<std::endl;
        m_weightSysts.push_back({m_triggerSystList.at(i), 1.0});
        continue;
      }

      if (elec_num == 0 && (m_triggerSystList.at(i).find("EL_EFF_Trig") != std::string::npos)) {
        m_weightSysts.push_back({m_triggerSystList.at(i), 1.0});
        continue;
      }

      double triggerSF;
      // get decision + weight
      CP::SystematicSet sysSet(m_triggerSystList.at(i));
      m_triggerTool->applySystematicVariation(sysSet);
      m_triggerTool->getDecisionAndScaleFactor(triggerSF);

      if (triggerSF_nominal > 0)
        m_weightSysts.push_back({m_triggerSystList.at(i), (float)(triggerSF / triggerSF_nominal)});
      else
        Error("pass1LepTrigger()", "Nominal trigger SF=0!, The systematics will not be generated.");
      //std::cout<<m_triggerSystList.at(i)<<" triggerSF"<<triggerSF<<" Nom "<<triggerSF_nominal<<std::endl;
    }
  }

//  if (m_tree->Leptons.size() < 2) {
//    return EL::StatusCode::SUCCESS;
//  }
  //Sort m_tree Leptons container
  if (m_tree->Leptons.size() > 1) {
    // std::cout << "Before Lep1 "<<m_tree->Leptons.at(0).p4.Pt()<<" Lep2 "<<m_tree->Leptons.at(1).p4.Pt()<<std::endl;
    std::sort(m_tree->Leptons.begin(), m_tree->Leptons.end(), compare_WWW_pt);
    // std::cout << "After Lep1 "<<m_tree->Leptons.at(0).p4.Pt()<<" Lep2 "<<m_tree->Leptons.at(1).p4.Pt()<<std::endl;
  }
  //Kill Event regarding the MET and Lepton size (Saves Space)

//  if ((muonLoose_num + elecLoose_num + elecNoBL_num) > 1) {
//    return EL::StatusCode::SUCCESS;
//  }


  bool passMET = false;
  if (m_tree->Leptons.size() > 0) {
  passMET =
      (met->met() > 55.e3 || (m_tree->Leptons.at(0).Type == ParticleType::MUON)); 
  		if (m_tree->Leptons.size() > 1) {
  passMET =
      (met->met() > 55.e3 || (m_tree->Leptons.at(0).Type == ParticleType::MUON || m_tree->Leptons.at(1).Type == ParticleType::MUON));
  		}
  }
  for (auto Lep : m_tree->Leptons) {
    m_tree->TotalQ += Lep.charge;
  }

  bool passSameSign = false;
  if (m_tree->Leptons.size() >= 2) {
    passSameSign = (m_tree->Leptons.at(0).charge * m_tree->Leptons.at(1).charge) == 1;
  }

  //MC Events that doesn't pass SameSign are killed (Saves Space) Data is needed for CF
  //if(m_tree->Leptons.size()==2 && m_isMC && !passSameSign) return EL::StatusCode::SUCCESS;
  //Fakes data events that doesn't pass the SameSign are killed (Saves Space)
  //if(m_tree->Leptons.size()==2 && (ele_loose.size()+muon_loose.size())>0 && !passSameSign) return EL::StatusCode::SUCCESS;

  //if(m_isMC && !passSameSign) return EL::StatusCode::SUCCESS;
  //Fakes data events that doesn't pass the SameSign are killed (Saves Space)
  //if((ele_loose.size()+muon_loose.size())>0 && !passSameSign) return EL::StatusCode::SUCCESS;

  // if(!passSameSign) return EL::StatusCode::SUCCESS;

  ////////////////////////////////////////
  //           Fake Factor              //
  ////////////////////////////////////////

  double ff_weight = -1.;
  if (ele_loose.size() == 1) {
    ff_weight =
        h_ff_el->GetBinContent(h_ff_el->FindBin(min(ele_loose.at(0)->pt() * 0.001, 149.99), min(fabs(ele_loose.at(0)->eta()), 2.469)));
    m_tree->LooseElPt = ele_loose.at(0)->p4().Pt();
    m_tree->LooseElE = ele_loose.at(0)->p4().E();
    m_tree->LooseElEta = ele_loose.at(0)->p4().Eta();
    m_tree->LooseElPhi = ele_loose.at(0)->p4().Phi();
  }
  if (muon_loose.size() == 1) {
    ff_weight =
        h_ff_mu->GetBinContent(h_ff_mu->FindBin(min(muon_loose.at(0)->pt() * 0.001, 149.99), min(fabs(muon_loose.at(0)->eta()), 2.499)));
    m_tree->LooseMuPt = muon_loose.at(0)->p4().Pt();
    m_tree->LooseMuE = muon_loose.at(0)->p4().E();
    m_tree->LooseMuEta = muon_loose.at(0)->p4().Eta();
    m_tree->LooseMuPhi = muon_loose.at(0)->p4().Phi();
  }

  ////////////////////////////////////////
  //        Charge Flip Rate            //
  ////////////////////////////////////////

  double cf_weight = 1.;
  double cf_StatSys = 1.;
  double cf_wBG = 1.;

  double cf_weight_1 = 0.;
  double cf_weight_2 = 0.;

  double cf_error_1 = 0.;
  double cf_error_2 = 0.;

  double cf_wBG_1 = 0.;
  double cf_wBG_2 = 0.;

  // TODO: remove magic numbers
  if (m_tree->Leptons.size() == 1) {
    if (m_tree->Leptons.at(0).Type == ParticleType::ELECTRON) {
      cf_weight_1 = h_CF_el->GetBinContent(
          h_CF_el->FindBin(min(fabs(m_tree->Leptons.at(0).p4.Eta()), 2.469), min(m_tree->Leptons.at(0).p4.Pt() * 0.001, 999.99)));
      cf_error_1 = cf_weight_1 + h_CF_el->GetBinError(h_CF_el->FindBin(min(fabs(m_tree->Leptons.at(0).p4.Eta()), 2.469),
                                                                       min(m_tree->Leptons.at(0).p4.Pt() * 0.001, 999.99)));
      cf_wBG_1 = h_CF_el_wBG->GetBinContent(
          h_CF_el_wBG->FindBin(min(fabs(m_tree->Leptons.at(0).p4.Eta()), 2.469), min(m_tree->Leptons.at(0).p4.Pt() * 0.001, 999.99)));
    }
  } else if (m_tree->Leptons.size() == 2) {
    if (m_tree->Leptons.at(0).Type == ParticleType::ELECTRON) {
      cf_weight_1 = h_CF_el->GetBinContent(
          h_CF_el->FindBin(min(fabs(m_tree->Leptons.at(0).p4.Eta()), 2.469), min(m_tree->Leptons.at(0).p4.Pt() * 0.001, 999.99)));
      cf_error_1 = cf_weight_1 + h_CF_el->GetBinError(h_CF_el->FindBin(min(fabs(m_tree->Leptons.at(0).p4.Eta()), 2.469),
                                                                       min(m_tree->Leptons.at(0).p4.Pt() * 0.001, 999.99)));
      cf_wBG_1 = h_CF_el_wBG->GetBinContent(
          h_CF_el_wBG->FindBin(min(fabs(m_tree->Leptons.at(0).p4.Eta()), 2.469), min(m_tree->Leptons.at(0).p4.Pt() * 0.001, 999.99)));
    }

    if (m_tree->Leptons.at(1).Type == ParticleType::ELECTRON) {
      cf_weight_2 = h_CF_el->GetBinContent(
          h_CF_el->FindBin(min(fabs(m_tree->Leptons.at(1).p4.Eta()), 2.469), min(m_tree->Leptons.at(1).p4.Pt() * 0.001, 999.99)));
      cf_error_2 = cf_weight_2 + h_CF_el->GetBinError(h_CF_el->FindBin(min(fabs(m_tree->Leptons.at(1).p4.Eta()), 2.469),
                                                                       min(m_tree->Leptons.at(1).p4.Pt() * 0.001, 999.99)));
      cf_wBG_2 = h_CF_el_wBG->GetBinContent(
          h_CF_el_wBG->FindBin(min(fabs(m_tree->Leptons.at(1).p4.Eta()), 2.469), min(m_tree->Leptons.at(1).p4.Pt() * 0.001, 999.99)));
    }
  } else if (m_tree->Leptons.size() == 3 && abs(m_tree->TotalQ) == 1) {
    for (auto Lep : m_tree->Leptons)
      if (Lep.Type == ParticleType::ELECTRON && Lep.charge == m_tree->TotalQ) {
        cf_weight_1 = h_CF_el->GetBinContent(h_CF_el->FindBin(min(fabs(Lep.p4.Eta()), 2.469), min(Lep.p4.Pt() * 0.001, 999.99)));
        cf_error_1 = cf_weight_1 + h_CF_el->GetBinError(h_CF_el->FindBin(min(fabs(Lep.p4.Eta()), 2.469), min(Lep.p4.Pt() * 0.001, 999.99)));
        cf_wBG_1 = h_CF_el_wBG->GetBinContent(h_CF_el_wBG->FindBin(min(fabs(Lep.p4.Eta()), 2.469), min(Lep.p4.Pt() * 0.001, 999.99)));
      }
  }

  // What exactly is going on here?
  cf_weight = 1 / (1 - cf_weight_1 - cf_weight_2 + (2 * cf_weight_1 * cf_weight_2)) - 1;
  cf_StatSys = 1 / (1 - cf_error_1 - cf_error_2 + (2 * cf_error_1 * cf_error_2)) - 1;
  cf_wBG = 1 / (1 - cf_wBG_1 - cf_wBG_2 + (2 * cf_wBG_1 * cf_wBG_2)) - 1;

  //Trigger and Trig Match (Match is mainly done inside the electron loop)

   //bool m_passHLT_e26_lhtight_nod0_ivarloose = Props::passHLT_e26_lhtight_nod0_ivarloose.get(eventInfo);
   //bool m_passHLT_e60_lhmedium_nod0 = Props::passHLT_e60_lhmedium_nod0.get(eventInfo);
   //bool m_passHLT_e140_lhloose_nod0 = Props::passHLT_e140_lhloose_nod0.get(eventInfo);

   //bool m_passHLT_e17_lhloose = Props::passHLT_e17_lhloose_L1EM15.get(eventInfo);

   //bool m_passHLT_2e17_lhvloose_nod0 =  Props::passHLT_2e17_lhvloose_nod0.get(eventInfo);
   //bool m_passHLT_2e24_lhvloose_nod0 = Props::passHLT_2e24_lhvloose_nod0.get(eventInfo);

   //bool m_passHLT_mu14 = Props::passHLT_mu14.get(eventInfo);
   //bool m_passHLT_mu22_mu8noL1 = Props::passHLT_mu22_mu8noL1.get(eventInfo);
   //bool m_passHLT_2mu14 = Props::passHLT_2mu14.get(eventInfo);
   //bool m_passHLT_mu24_ivarmedium = Props::passHLT_mu24_imedium.get(eventInfo);
   //bool m_passHLT_mu26_ivarmedium = Props::passHLT_mu26_ivarmedium.get(eventInfo);
   //bool m_passHLT_mu50 = Props::passHLT_mu50.get(eventInfo);

  //  if(elec_num>0)
  //    {

  //      passTrig = passTrig || m_passHLT_e140_lhloose_nod0 || m_passHLT_e26_lhtight_nod0_ivarloose ||  m_passHLT_e60_lhmedium_nod0;// || m_passHLT_2e17_lhvloose_nod0 || m_passHLT_2e24_lhvloose_nod0; //
  //      //passTrig = passTrig || m_passHLT_e26_lhtight_nod0_ivarloose; // || m_passHLT_2e17_lhvloose_nod0 || m_passHLT_2e24_lhvloose_nod0; //
  //    }

  //  if(muon_num>0)
  //    {
  //  	passTrig = passTrig || m_passHLT_mu26_ivarmedium || m_passHLT_mu50;
  //      //passTrig = passTrig || m_passHLT_mu26_ivarmedium;
  //    }

  //Wjj reconstruction from two prompt jets
  TLorentzVector vMjj;
  if (jet_num >= 2) {
    vMjj = goodJets.at(0)->p4() + goodJets.at(1)->p4();
  }

  // Pass Cuts
  bool pass3Leptonorigin = false; 
  bool pass1ZLepton = (electronsize_z + muonsize_z) > 0;
  bool pass2ZLepton = (electronsize_z + muonsize_z) > 1;
  bool pass3ZLepton = (electronsize_z + muonsize_z) > 2;
  bool pass1WLepton = (electronsize_w + muonsize_w) > 0;
  bool pass1Lepton = (electronsize + muonsize) > 0;
  bool pass2Lepton = (electronsize + muonsize) > 1;
  bool pass3Lepton = (electronsize + muonsize) > 2;
  bool pass4Lepton = (electronsize + muonsize) > 3;

  bool pass3LRegion = (muon_num + elec_num) == 3 && ((abs(elecCS) == 0 && abs(muonCS) == 1) || (abs(muonCS) == 0 && abs(elecCS) == 1));

  bool passNJets = jet_num >= 2;

  //bool passLeptonPt = (muon_num + elec_num) == 0; // this looks strange
  bool passLeptonPt = false;  // JL changed this from above

  for (auto Lep : m_tree->Leptons) {
    if (Lep.TrigMatch) {
      passLeptonPt = passLeptonPt || Lep.p4.Pt() > 27.e3;
    }
  }

  bool passJetPt = false;
  bool passJetEta = false;
  bool passMjj = false;
  bool passWSide = false;
  bool passDEtaJJ = false;
  if (jet_num >= 2) {
    passJetPt = goodJets.at(0)->p4().Pt() > 30.e3 && goodJets.at(1)->p4().Pt() > 20.e3;
    passJetEta = fabs(goodJets.at(0)->p4().Eta()) < 2.5 && fabs(goodJets.at(1)->p4().Eta()) < 2.5;
    passMjj = vMjj.M() < 120.e3 && vMjj.M() > 50.e3;
    passWSide = (vMjj.M() > 120.e3 || vMjj.M() < 50.e3) && vMjj.M() < 300.e3;
    passDEtaJJ = fabs(goodJets.at(0)->p4().Eta() - goodJets.at(1)->p4().Eta()) < 1.5;

  } else if (jet_num == 1) {
    passJetPt = goodJets.at(0)->p4().Pt() > 30.e3;
    passJetEta = fabs(goodJets.at(0)->p4().Eta()) < 2.5;
    passMjj = false;
    passWSide = false;
    passDEtaJJ = false;
  }

  //Prevent Z Reconstrution
  TLorentzVector vMll;
  // TLorentzVector vMll13;
  // TLorentzVector vMll23;
  TLorentzVector vMllee;
  TLorentzVector vMllmm;
  TLorentzVector vMlll;
  TLorentzVector vWWW;
  TLorentzVector vMET;
  //TLorentzVector vMPT;
  vMET.SetPxPyPzE(met->mpx(), met->mpy(), 0, met->met());
  //vMPT.SetPxPyPzE(mpt->mpx(),mpt->mpy(),0,mpt->met());
  if (m_tree->Leptons.size() >= 2) {
    vMll = (m_tree->Leptons.at(0).p4 + m_tree->Leptons.at(1).p4);
    vMlll = vMll;
    vWWW = vMET;

    m_tree->mtll = (vMll + vMET).Mt();
    m_tree->mtLep1 = (m_tree->Leptons.at(0).p4 + vMET).Mt();
    m_tree->mtLep2 = (m_tree->Leptons.at(1).p4 + vMET).Mt();
    m_tree->dPhill = m_tree->Leptons.at(0).p4.DeltaPhi(m_tree->Leptons.at(1).p4);
    m_tree->dEtall = fabs(m_tree->Leptons.at(0).p4.Eta() - m_tree->Leptons.at(1).p4.Eta());

    if (m_tree->Leptons.size() >= 3) {
      m_tree->mtLep3 = (m_tree->Leptons.at(2).p4 + vMET).Mt();

      vMlll += m_tree->Leptons.at(2).p4;
      m_tree->mtlll = (vMlll + vMET).Mt();
      m_tree->etalll = vMlll.Eta();
      m_tree->philll = vMlll.Phi();

      // vMll13 = m_tree->Leptons.at(0).p4 + m_tree->Leptons.at(2).p4;
      // m_tree->mll13 = vMll13.M();
      // m_tree->mtll13 = (vMll13+vMET).Mt();
      // m_tree->dRll13 = m_tree->Leptons.at(0).p4.DeltaR(m_tree->Leptons.at(2).p4);
      // m_tree->dPhill13 = m_tree->Leptons.at(0).p4.DeltaPhi(m_tree->Leptons.at(2).p4);

      // vMll23 = m_tree->Leptons.at(1).p4 + m_tree->Leptons.at(2).p4;
      // m_tree->mll23 = vMll23.M();
      // m_tree->mtll23 = (vMll23+vMET).Mt();
      // m_tree->dRll23 = m_tree->Leptons.at(1).p4.DeltaR(m_tree->Leptons.at(2).p4);
      // m_tree->dPhill23 = m_tree->Leptons.at(1).p4.DeltaPhi(m_tree->Leptons.at(2).p4);
    }

    if (m_tree->Leptons.size() == 2) {
      vWWW += (vMll + vMjj);
    } else if (m_tree->Leptons.size() == 3) {
      vWWW += vMlll;
    }
  }

  TLorentzVector MllSFOS;
  //Int_t chargeSF = 0;
  int m_ZCandidates = 0;
  //std::vector<WWWParticle> m_ZCandidates;
  float minZmass = 1000.;

 if( m_tree->Leptons.size()>0){
   for (int ilep = 0; ilep <(int) m_tree->Leptons.size();ilep++){
	   for (int jlep = ilep+1; jlep<(int) m_tree->Leptons.size();jlep++){
		   WWWParticle lep1 = m_tree->Leptons.at(ilep);
		   WWWParticle lep2 = m_tree->Leptons.at(jlep);
		   if(lep1.charge * lep2.charge >0) continue;
		   if(lep1.Type != lep2.Type ) continue;
 			   
//		   m_ZCandidates.push_back(lep1);
//		   m_ZCandidates.push_back(lep2);
			float dMass = fabs((lep1.p4 + lep2.p4).M() - Mz);
				if(m_ZCandidates == 0 || dMass < minZmass){
					minZmass = dMass;
		   			MllSFOS = (lep1.p4 + lep2.p4);
				}
			m_ZCandidates++;
		   //break;
	   }
	}
}

//  if (elec_num == 2) {
//	for (auto Lep : m_tree->Leptons) {
//	  if (Lep.Type != ParticleType::ELECTRON) continue;
//	  vMllee = vMllee + Lep.p4;
//	  chargeSF += Lep.charge;
//	}
//	if (chargeSF == 0) MllSFOS = vMllee.M();
// }else if (muon_num == 2) {
//	for (auto Lep : m_tree->Leptons) {
//	  if (Lep.Type != ParticleType::MUON) continue;
//	  vMllmm = vMllmm + Lep.p4;
//	  chargeSF += Lep.charge;
//	}
//	if (chargeSF == 0) MllSFOS = vMllmm.M();
// }
	
  m_tree->mllSFOS = MllSFOS.M();
  m_tree->pass1SFOS = m_ZCandidates>0;

  // TODO: fix magic numbers
  bool passMll = vMll.M() > 40.e3 && vMll.M() < 400.e3;
  bool passZVeto = false;
  if (m_tree->Leptons.size() > 1) {
  passZVeto = (m_tree->Leptons.at(0).Type == ParticleType::ELECTRON && m_tree->Leptons.at(1).Type == ParticleType::ELECTRON &&
                    (vMll.M() < 80.e3 || vMll.M() > 100.e3)) ||
                   !(m_tree->Leptons.at(0).Type == ParticleType::ELECTRON && m_tree->Leptons.at(1).Type == ParticleType::ELECTRON);
  }
  int Nbjets = bJets.size();
  bool passNBtag = Nbjets == 0;

  //Fill jets to the TTree's
  for (auto jet : goodJets) {
    WWWParticle tmpJet;
    tmpJet.p4 = jet->p4();
    tmpJet.Type = JET;
    m_tree->Jets.push_back(tmpJet);
  }

  if (TauSel.size() > 0) {
    m_tree->TauPt = TauSel.at(0)->p4().Pt();
    m_tree->TauE = TauSel.at(0)->p4().E();
    m_tree->TauEta = TauSel.at(0)->p4().Eta();
    m_tree->TauPhi = TauSel.at(0)->p4().Phi();
    m_tree->TaunTracks = Props::nTracks.get(TauSel.at(0));
  }

  //Fill TTree
  m_tree->met = met->met();
  m_tree->metSig = Props::metSig.get(met);
  m_tree->metSigRatio = Props::metSig.get(met) * 10 / met->met();
  m_tree->metPhi = met->phi();
  // m_tree->mpt=mpt->met();
  // m_tree->mptPhi=mpt->phi();

  m_tree->Nleptons = muon_num + elec_num;
  m_tree->Njets = jet_num;
  m_tree->Nbjets = Nbjets;
  m_tree->Nbjets77 = bJets77.size();
  m_tree->Nel = elec_num;
  m_tree->Nmu = muon_num;

  m_tree->NAllEl = electron.size();
  m_tree->NAllMu = muon.size();
  m_tree->NAllTa = tau.size();

  m_tree->Ntruthtau = Ntruthtau;

  m_tree->Nloosetau = tauLoose_num;
  m_tree->Nmediumtau = tauMedium_num;
  m_tree->Ntighttau = tauTight_num;

  m_tree->Mll = vMll.M();
  m_tree->Mllee = vMllee.M();
  m_tree->Mllmm = vMllmm.M();

  if (elec_num == 2) {
    m_tree->MllSF = vMllee.M();
  } else {
    m_tree->MllSF = vMllmm.M();
  }

  m_tree->Mlll = vMlll.M();
  m_tree->Mjj = vMjj.M();
  m_tree->MWWW = vWWW.M();

  m_tree->Ptll = vMll.Pt();
  m_tree->Ptjj = vMjj.Pt();
  m_tree->PtWWW = vWWW.Pt();

  if (jet_num > 1) {
    m_tree->DEtajj = fabs(goodJets.at(0)->p4().Eta() - goodJets.at(1)->p4().Eta());
  }

  m_tree->Etall = vMll.Eta();
  m_tree->Etajj = vMjj.Eta();
  m_tree->EtaWWW = vWWW.Eta();

  m_tree->Phill = vMll.Phi();
  m_tree->Phijj = vMjj.Phi();
  m_tree->PhiWWW = vWWW.Phi();

  m_tree->SumEta = 0;
  m_tree->SumPt = 0;

  m_tree->passTrig = passTrig;                    // m_triggerTool->getDecisionAndScaleFactor( triggerSF_nominal )
  m_tree->passTrigMatch = passTrigMatch;          // for each lepton ( passTrigMatch || Lep.TrigMatch )
  m_tree->passMET = passMET;                      // (met->met() > 55.e3 || (m_tree->Leptons.at(0).Type == ParticleType::MUON
                                                  // || m_tree->Leptons.at(1).Type == ParticleType::MUON));
  m_tree->passSameSign = passSameSign;            // ( m_tree->Leptons.at(0).charge * m_tree->Leptons.at(1).charge ) == 1
  m_tree->pass3Lepton = pass3Lepton;              // (electron.size() + muon.size()) > 2
  //m_tree->isNotBadJet = isNotBadJet;              // (electron.size() + muon.size()) > 2
  m_tree->pass1ZLepton = pass1ZLepton;              // (electron.size() + muon.size()) > 2
  m_tree->pass2ZLepton = pass2ZLepton;              // (electron.size() + muon.size()) > 2
  m_tree->pass1Lepton = pass1Lepton;              // (electron.size() + muon.size()) > 2
  m_tree->pass2Lepton = pass2Lepton;              // (electron.size() + muon.size()) > 2
  m_tree->pass3ZLepton = pass3ZLepton;              // (electron.size() + muon.size()) > 2
  m_tree->pass1WLepton = pass1WLepton;              // (electron.size() + muon.size()) > 2
  m_tree->pass3Leptonorigin = pass3Leptonorigin;  // electronsize_z + muon.size() > 2
  m_tree->pass4Lepton = pass4Lepton;              // (electron.size() + muon.size()) > 3
  m_tree->pass3LRegion = pass3LRegion;            // (muon_num + elec_num) == 3 && ((abs(elecCS) == 0 && abs(muonCS) == 1)
                                                  // || (abs(muonCS) == 0 && abs(elecCS) == 1))
  m_tree->passNJets = passNJets;                  // jet_num >= 2
  m_tree->passLeptonPt = passLeptonPt;            // passLeptonPt = passLeptonPt || Lep.p4.Pt() > 27.e3
  m_tree->passJetPt = passJetPt;                  // goodJets.at( 0 )->p4().Pt() > 30.e3 && goodJets.at( 1 )->p4().Pt() > 20.e3
  m_tree->passJetEta = passJetEta;                // fabs( goodJets.at(0)->p4().Eta() ) < 2.5 && fabs( goodJets.at( 1 )->p4().Eta() ) < 2.5
  m_tree->passMjj = passMjj;                      // vMjj.M() < 120.e3 && vMjj.M() > 50.e3
  m_tree->passWSide = passWSide;                  // ( vMjj.M() > 120.e3 || vMjj.M() < 50.e3 ) && vMjj.M() < 300.e3
  m_tree->passDEtaJJ = passDEtaJJ;                // fabs( goodJets.at(0)->p4().Eta() - goodJets.at(1)->p4().Eta() ) < 1.5
  m_tree->passMll = passMll;                      // vMll.M() > 40.e3 && vMll.M() < 400.e3
  m_tree->passZVeto = passZVeto;                  // (m_tree->Leptons.at(0).Type == ParticleType::ELECTRON
                                                  // && m_tree->Leptons.at(1).Type == ParticleType::ELECTRON && (vMll.M() < 80.e3
                                                  // || vMll.M() > 100.e3)) || !(m_tree->Leptons.at(0).Type == ParticleType::ELECTRON
                                                  // && m_tree->Leptons.at(1).Type == ParticleType::ELECTRON)
  m_tree->passNBtag = passNBtag;                  // Nbjets == 0
  m_tree->weight = m_weight * lepWeight;
  m_tree->QCDweight = ff_weight;
  m_tree->Nloose = muonLoose_num + elecLoose_num;
  m_tree->Nlooseel = elecLoose_num;
  m_tree->NNoBLel = elecNoBL_num;
  m_tree->Nloosemu = muonLoose_num;

  // m_tree->passAuthor = passAuthor;
  // m_tree->passPhoton = passPhoton;

  m_tree->CFweight = cf_weight;
  m_tree->CFStatSys = cf_StatSys;
  m_tree->CFwBG = cf_wBG;

  if (bJets.size() >= 1) {
    m_tree->bJet1E = bJets.at(0)->p4().E();
    m_tree->bJet1Pt = bJets.at(0)->p4().Pt();
    m_tree->bJet1M = bJets.at(0)->p4().M();
    m_tree->bJet1Eta = bJets.at(0)->p4().Eta();
    m_tree->bJet1Phi = bJets.at(0)->p4().Phi();
  }

  if (jet_num >= 1) {
    m_tree->Jet1E = goodJets.at(0)->p4().E();
    m_tree->Jet1Pt = goodJets.at(0)->p4().Pt();
    m_tree->Jet1Eta = goodJets.at(0)->p4().Eta();
    m_tree->Jet1Phi = goodJets.at(0)->p4().Phi();
    m_tree->Jet1NTrkPt500PV = Props::NumTrkPt500PV.get(goodJets.at(0));
    m_tree->Jet1NTrkPt1000PV = Props::NumTrkPt1000PV.get(goodJets.at(0));

    m_tree->Jet1nrMuon = Props::nrMuonInJet.get(goodJets.at(0));
    m_tree->Jet1nrEl = Props::nrElectronInJet.get(goodJets.at(0));

    m_tree->passJetLepOverlap = Props::nrMuonInJet.get(goodJets.at(0)) == 0 && Props::nrElectronInJet.get(goodJets.at(0)) == 0;
  }
  if (jet_num >= 2) {
    m_tree->Jet2E = goodJets.at(1)->p4().E();
    m_tree->Jet2Pt = goodJets.at(1)->p4().Pt();
    m_tree->Jet2Eta = goodJets.at(1)->p4().Eta();
    m_tree->Jet2Phi = goodJets.at(1)->p4().Phi();
    m_tree->SumEta = m_tree->Jet2Eta + m_tree->Jet1Eta;
    m_tree->SumPt = m_tree->Jet2Pt + m_tree->Jet1Pt;
    m_tree->DRjj = goodJets.at(0)->p4().DeltaR(goodJets.at(1)->p4());
    m_tree->Jet2NTrkPt500PV = Props::NumTrkPt500PV.get(goodJets.at(1));
    m_tree->Jet2NTrkPt1000PV = Props::NumTrkPt1000PV.get(goodJets.at(1));

    m_tree->Jet2nrMuon = Props::nrMuonInJet.get(goodJets.at(1));
    m_tree->Jet2nrEl = Props::nrElectronInJet.get(goodJets.at(1));

    m_tree->passJetLepOverlap =
        m_tree->passJetLepOverlap && Props::nrMuonInJet.get(goodJets.at(1)) == 0 && Props::nrElectronInJet.get(goodJets.at(1)) == 0;
  }

  if (m_tree->Leptons.size() >= 1) {
    // JL: testing
    if (m_isMC) {
      m_tree->Lep1isPrompt = m_tree->Leptons.at(0).isPrompt;
      m_tree->Lep1isFake = m_tree->Leptons.at(0).isFake;
      m_tree->Lep1truthStatus = m_tree->Leptons.at(0).truthStatus;
    }

    m_tree->Lep1E = m_tree->Leptons.at(0).p4.E();
    m_tree->Lep1Pt = m_tree->Leptons.at(0).p4.Pt();
    m_tree->Lep1Eta = m_tree->Leptons.at(0).p4.Eta();
    m_tree->Lep1Phi = m_tree->Leptons.at(0).p4.Phi();
    m_tree->Lep1innerTrackPt = m_tree->Leptons.at(0).innerTrackPt;

    m_tree->Lep1Q = m_tree->Leptons.at(0).charge;
    m_tree->Lep1Flav = m_tree->Leptons.at(0).Type;
    m_tree->Lep1isLoose = m_tree->Leptons.at(0).isLoose;
    m_tree->Lep1isNoBL = m_tree->Leptons.at(0).isNoBL;
    m_tree->Lep1Author = m_tree->Leptons.at(0).Author;

    m_tree->Lep1TopoEtCone20 = m_tree->Leptons.at(0).TopoEtCone20;
    m_tree->Lep1PtCone20 = m_tree->Leptons.at(0).PtCone20;
    m_tree->Lep1PtVarCone20 = m_tree->Leptons.at(0).PtVarCone20;
    m_tree->Lep1PtVarCone30 = m_tree->Leptons.at(0).PtVarCone30;
    m_tree->Lep1PtVarCone40 = m_tree->Leptons.at(0).PtVarCone40;
    m_tree->Lep1PtVarCone20_TightTTVA_pt1000 = m_tree->Leptons.at(0).PtVarCone20_TightTTVA_pt1000;
    m_tree->Lep1PtVarCone30_TightTTVA_pt1000 = m_tree->Leptons.at(0).PtVarCone30_TightTTVA_pt1000;

    m_tree->Lep1d0 = m_tree->Leptons.at(0).d0;

    m_tree->Lep1PromptWeight = m_tree->Leptons.at(0).PromptVar;
    m_tree->Lep1ECIDSBDT = m_tree->Leptons.at(0).ECIDSBDT;

    if (m_tree->Leptons.at(0).Type == ParticleType::ELECTRON) {
      if (m_tree->Leptons.at(0).PromptVar > m_tree->LepPLVEl) {
        m_tree->LepPLVEl = m_tree->Leptons.at(0).PromptVar;
      }
      if (m_tree->Leptons.at(0).ECIDSBDT < m_tree->LepECIDSBDT) {
        m_tree->LepECIDSBDT = m_tree->Leptons.at(0).ECIDSBDT;
      }
      if (m_tree->Leptons.at(0).addAmbiguity > m_tree->LepAddAmbiguity) {
        m_tree->LepAddAmbiguity = m_tree->Leptons.at(0).addAmbiguity;
      }
    } else if (m_tree->Leptons.at(0).Type == ParticleType::MUON) {
      if (m_tree->Leptons.at(0).PromptVar > m_tree->LepPLVMu) {
        m_tree->LepPLVMu = m_tree->Leptons.at(0).PromptVar;
      }
    }
  }
  if (m_tree->Leptons.size() >= 2) {
    // JL: testing
    if (m_isMC) {
      m_tree->Lep2isPrompt = m_tree->Leptons.at(1).isPrompt;
      m_tree->Lep2isFake = m_tree->Leptons.at(1).isFake;
      m_tree->Lep2truthStatus = m_tree->Leptons.at(1).truthStatus;
    }

    m_tree->Lep2E = m_tree->Leptons.at(1).p4.E();
    m_tree->Lep2Pt = m_tree->Leptons.at(1).p4.Pt();
    m_tree->Lep2Eta = m_tree->Leptons.at(1).p4.Eta();
    m_tree->Lep2Phi = m_tree->Leptons.at(1).p4.Phi();
    m_tree->Lep2innerTrackPt = m_tree->Leptons.at(1).innerTrackPt;

    m_tree->Lep2Q = m_tree->Leptons.at(1).charge;
    m_tree->Lep2Flav = m_tree->Leptons.at(1).Type;
    m_tree->Lep2isLoose = m_tree->Leptons.at(1).isLoose;
    m_tree->Lep2isNoBL = m_tree->Leptons.at(1).isNoBL;
    m_tree->Lep2Author = m_tree->Leptons.at(1).Author;

    m_tree->SumEta += m_tree->Lep2Eta + m_tree->Lep1Eta;
    m_tree->SumPt += m_tree->Lep2Pt + m_tree->Lep1Pt;
    m_tree->DRll = m_tree->Leptons.at(0).p4.DeltaR(m_tree->Leptons.at(1).p4);

    m_tree->Lep2d0 = m_tree->Leptons.at(1).d0;

    m_tree->Lep2TopoEtCone20 = m_tree->Leptons.at(1).TopoEtCone20;
    m_tree->Lep2PtCone20 = m_tree->Leptons.at(1).PtCone20;
    m_tree->Lep2PtVarCone20 = m_tree->Leptons.at(1).PtVarCone20;
    m_tree->Lep2PtVarCone30 = m_tree->Leptons.at(1).PtVarCone30;
    m_tree->Lep2PtVarCone40 = m_tree->Leptons.at(1).PtVarCone40;
    m_tree->Lep2PtVarCone20_TightTTVA_pt1000 = m_tree->Leptons.at(1).PtVarCone20_TightTTVA_pt1000;
    m_tree->Lep2PtVarCone30_TightTTVA_pt1000 = m_tree->Leptons.at(1).PtVarCone30_TightTTVA_pt1000;

    m_tree->Lep2PromptWeight = m_tree->Leptons.at(1).PromptVar;
    m_tree->Lep2ECIDSBDT = m_tree->Leptons.at(1).ECIDSBDT;
    if (m_tree->Leptons.at(1).Type == ParticleType::ELECTRON) {
      if (m_tree->Leptons.at(1).PromptVar > m_tree->LepPLVEl) {
        m_tree->LepPLVEl = m_tree->Leptons.at(1).PromptVar;
      }
      if (m_tree->Leptons.at(1).ECIDSBDT < m_tree->LepECIDSBDT) {
        m_tree->LepECIDSBDT = m_tree->Leptons.at(1).ECIDSBDT;
      }
      if (m_tree->Leptons.at(1).addAmbiguity > m_tree->LepAddAmbiguity) {
        m_tree->LepAddAmbiguity = m_tree->Leptons.at(1).addAmbiguity;
      }
    } else if (m_tree->Leptons.at(1).Type == ParticleType::MUON) {
      if (m_tree->Leptons.at(1).PromptVar > m_tree->LepPLVMu) {
        m_tree->LepPLVMu = m_tree->Leptons.at(1).PromptVar;
      }
    }
  }

  if (m_tree->Leptons.size() >= 3) {
    // JL: testing
    if (m_isMC) {
      m_tree->Lep3isPrompt = m_tree->Leptons.at(2).isPrompt;
      m_tree->Lep3isFake = m_tree->Leptons.at(2).isFake;
      m_tree->Lep3truthStatus = m_tree->Leptons.at(2).truthStatus;
    }

    m_tree->Lep3E = m_tree->Leptons.at(2).p4.E();
    m_tree->Lep3Pt = m_tree->Leptons.at(2).p4.Pt();
    m_tree->Lep3Eta = m_tree->Leptons.at(2).p4.Eta();
    m_tree->Lep3Phi = m_tree->Leptons.at(2).p4.Phi();
    m_tree->Lep3innerTrackPt = m_tree->Leptons.at(2).innerTrackPt;

    m_tree->Lep3Q = m_tree->Leptons.at(2).charge;
    m_tree->Lep3Flav = m_tree->Leptons.at(2).Type;
    m_tree->Lep3isLoose = m_tree->Leptons.at(2).isLoose;
    m_tree->Lep3isNoBL = m_tree->Leptons.at(2).isNoBL;
    m_tree->Lep3Author = m_tree->Leptons.at(2).Author;

    m_tree->pass3lTT = m_tree->Leptons.size() == 3 && elecCS * muonCS < 0 &&
                       ((abs(elecCS) == 2 && abs(muonCS) == 1) || (abs(elecCS) == 1 && abs(muonCS) == 2));

    m_tree->pass0SFOS = m_tree->Leptons.size() == 3 && ((abs(elecCS) == 2 && abs(muonCS) == 1) || (abs(elecCS) == 1 && abs(muonCS) == 2));
//    m_tree->pass1SFOS = (electronsize_z > 1 && abs(elecCS_z) == 0) || (muonsize_z > 1 && abs(muonCS_z) == 0) || (electronsize_z == 3 && abs(elecCS_z) == 1) || (muonsize_z == 3 && abs(muonCS_z) == 1);

    Int_t LepType = 0;
    if (elec_num == 2 && muon_num == 1) {
      LepType = ParticleType::MUON;
    } else if (muon_num == 2 && elec_num == 1) {
      LepType = ParticleType::ELECTRON;
    }

    for (unsigned int index = 0; index < m_tree->Leptons.size(); index++) {
      if (m_tree->Leptons.at(index).Type == LepType) {
        m_tree->LepOFE = m_tree->Leptons.at(index).p4.E();
        m_tree->LepOFPt = m_tree->Leptons.at(index).p4.Pt();
        m_tree->LepOFEta = m_tree->Leptons.at(index).p4.Eta();
        m_tree->LepOFPhi = m_tree->Leptons.at(index).p4.Phi();

        m_tree->LepOFQ = m_tree->Leptons.at(index).charge;
        m_tree->LepOFFlav = m_tree->Leptons.at(index).Type;
        m_tree->LepOFisLoose = m_tree->Leptons.at(index).isLoose;
        m_tree->LepOFisNoBL = m_tree->Leptons.at(index).isNoBL;

        break;
      }
    }
  }

  if (m_tree->Leptons.size() >= 1 && goodJets.size() >= 1) {
    m_tree->DRj1l1 = m_tree->Leptons.at(0).p4.DeltaR(goodJets.at(0)->p4());
  }
  if (m_tree->Leptons.size() >= 1 && goodJets.size() >= 2) {
    m_tree->DRj2l1 = m_tree->Leptons.at(0).p4.DeltaR(goodJets.at(1)->p4());
  }
  if (m_tree->Leptons.size() >= 2 && goodJets.size() >= 1) {
    m_tree->DRj1l2 = m_tree->Leptons.at(1).p4.DeltaR(goodJets.at(0)->p4());
  }
  if (m_tree->Leptons.size() >= 2 && goodJets.size() >= 2) {
    m_tree->DRj2l2 = m_tree->Leptons.at(1).p4.DeltaR(goodJets.at(1)->p4());
  }

  // MTree Pre-def regions

  // m_tree->passBasics=passTrig && passTrigMatch && passNJets  && passJetEta && passJetPt && passLeptonPt && passMll;

  // ##############################################################
  // ##############   NTOE ########################################
  // ##############################################################
  // The following cuts do not by themselves select a
  // particular signal region. For 3 lepton regions there
  // are two additional cuts that need to be applied:
  //    Nloose==0 ( Nloose==1 selects non-prompt region )
  //    NNoBLel==0 ( NNoBLel==1 selects photon conversion region )
  // For the two lepton regions, we also have to apply:
  //    passSameSign==True ( passSameSign==False selects charge flip region )
  //
  // Do not apply any cuts for passSameSign in 3 lepton regions
  // ##############################################################

  // Tight leptons meet all signal requirements
  m_tree->passWWWSR = passTrig                     // returned vale from trigger tool; not exactly sure what this is
                      && passTrigMatch             // Lep.trigmatch is true for one of the leptons; not sure eactly what this is
                      && passNJets                 // 2 or more jets
                      && passJetEta                // jets have the correct ets
                      && passJetPt                 // jets have the correct min pt
                      && passNBtag                 // no btaged jets
                      && !pass3Lepton              // less than 3 leptons (not really sure what status they have, though; need to check)
                      && elec_num + muon_num == 2  // makes sure that there are two leptons in the event but they don't have to be tight
                      && passLeptonPt              // leptons have at least min pt
                      && passMll                   // Mll > 40 and < 400
                      && passZVeto                 // iff ee, Mll not in Z window
                      && passMET                   // if ee, MET above specified value
                      && passMjj                   // Mjj within SR window
                      && passDEtaJJ;               // if 2 jets, Deta < certain value

  m_tree->passWWWSRorigin = passTrig && passTrigMatch && passNJets && passJetEta && passJetPt && passNBtag && !pass3Leptonorigin &&
                            elec_num + muon_num == 2 && passLeptonPt && passMll && passZVeto && passMET && passMjj && passDEtaJJ;

  m_tree->passSideBandCR = passTrig  // same as passWWWSR but different Mjj
                           && passTrigMatch && passNJets && passJetEta && passJetPt && passNBtag && !pass3Lepton &&
                           elec_num + muon_num == 2 && passLeptonPt && passMll && passZVeto && passMET &&
                           passWSide  // Mjj area lessthan 300 and not Mjj SR
                           && passDEtaJJ;

  m_tree->passSideBandCRorigin = passTrig && passTrigMatch && passNJets && passJetEta && passJetPt && passNBtag && !pass3Leptonorigin &&
                                 elec_num + muon_num == 2 && passLeptonPt && passMll && passZVeto && passMET && passWSide && passDEtaJJ;

  m_tree->passBtagCR = passTrig && passTrigMatch && passNJets && passJetEta && passJetPt && Nbjets == 1 && !pass3Lepton &&
                       elec_num + muon_num == 2 && passLeptonPt && passMll && passZVeto && passMET && (passMjj || passWSide) && passDEtaJJ;

  m_tree->passl2jetCR = passTrig && passTrigMatch && !passNJets && passJetEta && passJetPt && passNBtag && !pass3Lepton &&
                        elec_num + muon_num == 2 && passLeptonPt && passMll && passZVeto && passMET;

  m_tree->pass3lCR = passTrig && passTrigMatch && passNBtag && pass3LRegion && elec_num + muon_num == 3 && !pass4Lepton && passLeptonPt;

  m_tree->pass3lSR = passTrig && passTrigMatch && passNBtag && pass3Lepton && elec_num + muon_num == 3 && !pass4Lepton && passLeptonPt &&
                     abs(m_tree->TotalQ) == 1;
  //&& (elec_num!=2 || m_tree->Mllee>100.e3 || m_tree->Mllee<90.e3) ;

  m_tree->pass3lBtag = passTrig && passTrigMatch && m_tree->Nbjets >= 1 && pass3Lepton && elec_num + muon_num == 3 && !pass4Lepton &&
                       passLeptonPt && abs(m_tree->TotalQ) == 1;

  m_tree->pass3lVgamma = passTrig && passTrigMatch && passNBtag && pass3LRegion && elec_num + muon_num == 3 && !pass4Lepton &&
                         passLeptonPt && vMlll.M() < 100.e3 && vMlll.M() > 80.e3;

  m_tree->passZWindowCR = passTrig && passTrigMatch && passNJets && passJetEta && passJetPt && passNBtag && !pass3Lepton &&
                          elec_num + muon_num == 2 && passLeptonPt && passMll && !passZVeto && passMET && (passMjj || passWSide) &&
                          passDEtaJJ;

  m_tree->passCFCR = passTrig && passTrigMatch &&
                     !pass3Lepton
                     //&& elec_num + muon_num == 2 // redundant
                     && elec_num == 2 && muon_num == 0 && elecNoBL_num == 0 && elecLoose_num == 0  // allows 1 loose muon
                     && passLeptonPt;

  m_tree->Fill();

  return EL::StatusCode::SUCCESS;
}

void CxAODReader_WZ::FillHist(string /*CutName*/) {
  // m_histSvc -> BookFillHist(Form("_Mjj",CutName.c_str()),   20, 20, 320,  m_tree->Mjj, m_tree->weight);
  // m_histSvc -> BookFillHist(Form("_Mll",CutName.c_str()),   0, 20, 400,  m_tree->Mll, m_tree->weight);
  // Doesn't do anything
  return;
}

CxAODReader_WZ::CxAODReader_WZ() {}

//Sets Up Histogram Manager, Currently not actually used
// EL::StatusCode CxAODReader_WZ ::histInitialize() {
//   // histogram manager
//   m_histSvc = new HistSvc();
//   m_histNameSvc = new HistNameSvc();
//   m_histSvc->SetNameSvc(m_histNameSvc);
//   //m_histSvc -> SetWeightSysts(&m_weightSysts);

//   bool fillHists = true;
//   m_config->getif<bool>("writeHistograms", fillHists);
//   m_histSvc->SetFillHists(fillHists);

//   return EL::StatusCode::SUCCESS;
// }

// EL::StatusCode CxAODReader_WZ :: initializeVariations ()
// {

//   return EL::StatusCode::SUCCESS;
// }

// Here you do everything that needs to be done exactly once for every
// single file, e.g. collect a list of all lumi-blocks processed
EL::StatusCode CxAODReader_WZ::fileExecute() { return EL::StatusCode::SUCCESS; }

//Defines PreSelection tool that does pre selection on our objects.
EL::StatusCode CxAODReader_WZ::initializeSelection() {
  m_eventSelection = new PreEvtSelection(m_config);
  m_fillFunction = std::bind(&CxAODReader_WZ::fill_WZ, this);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode CxAODReader_WZ::initializeSumOfWeights() {
  Info("initializeSumOfWeights()", "Initialize sum of weights.");

  if (!m_isMC) {
    return EL::StatusCode::SUCCESS;
  }

  Info("initializeSumOfWeights()", "Location of sumOfWeightsFile: %s", m_sumOfWeightsFile.c_str());
  m_sumOfWeightsProvider = new sumOfWeightsProvider(gSystem->ExpandPathName(m_sumOfWeightsFile.c_str()));

  if (!m_sumOfWeightsProvider) {
    Error("initializeSumOfWeights()", "sumOfWeightsProvider not initialized!");
    return EL::StatusCode::FAILURE;
  }
  return EL::StatusCode::SUCCESS;
}  // initializeSumOfWeights

//Initializes tools, really usefull for adding non existing systematics
EL::StatusCode CxAODReader_WZ::initializeTools() {
  Info("initializeTools()", "Initialize tools.");
  // call the base
  AnalysisReader::initializeTools();

  m_EvtWeightVar_WWW = new EvtWeightVariations_WWW();

  // m_muonTrigSF = new CP::MuonTriggerScaleFactors("TrigSFClass");
  // TOOL_CHECK("AnalysisReader_DB::initializeSelection()",m_muonTrigSF->setProperty("MuonQuality", "Medium"));
  // TOOL_CHECK("AnalysisReader_DB::initializeSelection()",m_muonTrigSF->setProperty("EventInfoContName","EventInfo___Nominal"));
  // //TOOL_CHECK("AnalysisReader_DB::initializeSelection()",m_muonTrigSF->setProperty("CalibrationRelease", "180216_Moriond21"));
  // TOOL_CHECK("AnalysisReader_DB::initializeSelection()",m_muonTrigSF->initialize());

  std::string WorkingFolder = "";
  if (std::getenv("WorkDir_DIR") != NULL) {
    WorkingFolder = std::getenv("WorkDir_DIR");
  } else {
    WorkingFolder = std::getenv("ROOTCOREBIN");  // what is this?
  }

  m_triggerTool = new TriggerTool_WWW(*m_config);  // what is this?
  m_triggerTool->initialize();

  //     lumicalcFiles_el.push_back( WorkingFolder+"/data/FrameworkSub/GRL/ilumicalc_histograms_None_276262-284484.root" );
  //     lumicalcFiles_el.push_back( WorkingFolder+"/data/FrameworkSub/GRL/ilumicalc_histograms_None_297730-303560.root" );
  //     lumicalcFiles_mu.push_back( WorkingFolder+"/data/FrameworkSub/GRL/ilumicalc_histograms_None_276262-284484.root" );
  //     lumicalcFiles_mu.push_back( WorkingFolder+"/data/FrameworkSub/GRL/ilumicalc_histograms_None_297730-303560.root" );

  //     lumicalcFiles_mu.push_back( WorkingFolder+"/data/CxAODReader_WZ/GRL/ilumicalc_histograms_HLT_mu14_276262-284484_Data2015.root:HLT_mu14" );
  //     lumicalcFiles_mu.push_back( WorkingFolder+"/data/CxAODReader_WZ/GRL/ilumicalc_histograms_HLT_mu14_297730-311481_Data2016.root:HLT_mu14" );
  //     lumicalcFiles_el.push_back( WorkingFolder+"/data/CxAODReader_WZ/GRL/ilumicalc_histograms_HLT_e12_lhvloose_nod0_L1EM10VH_276262-284484_Data2015.root:HLT_e12_lhvloose_nod0_L1EM10VH" );
  //     lumicalcFiles_el.push_back( WorkingFolder+"/data/CxAODReader_WZ/GRL/ilumicalc_histograms_HLT_e12_lhvloose_nod0_L1EM10VH_297730-311481_Data2016.root:HLT_e12_lhvloose_nod0_L1EM10VH" );

  //    m_datareweight_el->setProperty("LumiCalcFiles",lumicalcFiles_el);
  //    //m_datareweight_el->setProperty("TrigDecisionTool","Trig::TrigDecisionTool/TrigDecisionTool");

  //    m_datareweight_mu->setProperty("LumiCalcFiles",lumicalcFiles_mu);
  //    //m_datareweight_mu->setProperty("TrigDecisionTool","Trig::TrigDecisionTool/TrigDecisionTool");

  //    m_datareweight_el->initialize();
  //    m_datareweight_mu->initialize();

  //   }

  ////////////////////////////////////////
  //         Fake Factor File           //
  ////////////////////////////////////////
  std::string tmpFile = WorkingFolder + "/data/CxAODReader_WZ/FakeFactor/ff_ssWWPAOD_2D_Nominal.root";
  file_fakefactor = new TFile(tmpFile.c_str());
  file_fakefactor->GetObject("h_mu_FF", h_ff_mu);
  file_fakefactor->GetObject("h_el_FF", h_ff_el);

  ////////////////////////////////////////
  //       Charge Flip Rate File        //
  ////////////////////////////////////////

  //std::string tmpFile2 = WorkingFolder+"/data/CxAODReader_WZ/ChargeFlipRate/rates_Mll_l0_ptVsEtaFine_bkgeeZjets.root";
  std::string tmpFile2 = WorkingFolder + "/data/CxAODReader_WZ/ChargeFlipRate/test.root";
  std::cout << "Reading CF From " << tmpFile2 << std::endl;
  file_CFrate = new TFile(tmpFile2.c_str());
  //file_CFrate->GetObject("SFCentral_RunNumber0_9999999_SS",h_CF_el);
  file_CFrate->GetObject("test_misid", h_CF_el);

  std::string tmpFile3 = WorkingFolder + "/data/CxAODReader_WZ/ChargeFlipRate/test_wBG.root";
  std::cout << "Reading CF From " << tmpFile3 << std::endl;
  file_CFrate_wBG = new TFile(tmpFile3.c_str());
  //file_CFrate->GetObject("SFCentral_RunNumber0_9999999_SS",h_CF_el);
  file_CFrate_wBG->GetObject("test_misid", h_CF_el_wBG);

  m_tree = new MVATree_WWW(true, true, 1, wk(), m_variations, &m_weightSysts, false);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode CxAODReader_WZ::finalize() { return EL::StatusCode::SUCCESS; }

/*EL::StatusCode CxAODReader_WZ::setupJob(EL::Job& job) {
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.

  if (m_debug) Info("setupJob()", "Setting up job.");

  job.useXAOD();

  // check if ConfigStore is set
  if (!m_config) {
    Error("setupJob()", "ConfigStore not setup! Remember to set it via setConfig()!");
    return EL::StatusCode::FAILURE;
  }

  bool writeMVATree = false;
  bool writeEasyTree = false;
  string m_eosPATH = "none";
  m_config->getif<std::string>("eosFolderUser", m_eosPATH);

  string m_localPATH = "none";
  m_config->getif<std::string>("localFolderUser", m_localPATH);

  string driver = "direct";
  m_config->getif<std::string>("driver", driver);

  EL::OutputStream out("MVATree");
  m_config->getif<bool>("writeMVATree", writeMVATree);
  m_config->getif<bool>("writeEasyTree", writeEasyTree);

  if (writeMVATree || writeEasyTree) {
    if (m_eosPATH != "none") {
      out.output(new SH::DiskOutputXRD("root://eosuser.cern.ch/" + m_eosPATH));
    } else if (m_localPATH != "none" && driver == "condor") {
      out.output(new SH::DiskOutputLocal(m_localPATH));
    }
    job.outputAdd(out);
  }

  bool writeOSTree = false;
  EL::OutputStream outOS("OSTree");
  m_config->getif<bool>("writeOSTree", writeOSTree);
  if (writeOSTree) {
    if (m_eosPATH != "none") {
      outOS.output(new SH::DiskOutputXRD("root://eosuser.cern.ch/" + m_eosPATH));
    } else if (m_localPATH != "none" && driver == "condor") {
      outOS.output(new SH::DiskOutputLocal(m_localPATH));
    }
    job.outputAdd(outOS);
  }
  // let's initialize the algorithm to use the xAODRootAccess package
  xAOD::Init("MyxAODAnalysis").ignore();  // call before opening first file

  return EL::StatusCode::SUCCESS;
}  // setupJob
*/
//  LocalWords:  fabs
