// VVlvJ includes
#include "CxAODReader_WZ/PreEvtSelection.h"
//#include "CxAODTools_lvqq/DBProperties.h"
#include "AthContainers/DataVector.h"
#include "AthLinks/ElementLink.h"
#include "CxAODTools/CommonProperties.h"

// Constructor
PreEvtSelection::PreEvtSelection(
    ConfigStore* config)  // Where is ConfigStore defined?
                          // : m_ptAsymCut(0.15), m_deltaYCut(1.2), m_YFiltCut(0.45), m_config(config), DBEvtSelection() {
    : DBEvtSelection() {
  m_config = config;
  SetParameters();
}

void PreEvtSelection::SetParameters() {  // what are these and where are they defined?
  m_config->getif<bool>("debug", m_debug);
  // m_config->getif< double >( "VVJJptAsymCut", m_ptAsymCut );
  // m_config->getif< double >( "VVJJdeltaYCut", m_deltaYCut );
  // m_config->getif< double >( "VVJJYFiltCut", m_YFiltCut );
  // m_config->getif< std::vector< std::string > >( "triggerListDB", m_TriggerList );
  // m_config->getif< std::string>( "METSoftTerm", m_softTerm );
}

bool PreEvtSelection::passSelection(SelectionContainers&, bool) {
  // Not used at the moment
  return true;
}

bool PreEvtSelection::passPreSelection(SelectionContainers& containers, bool) {
  // This function applies preselection to events
  const xAOD::EventInfo* evtinfo = containers.evtinfo;
  const xAOD::MissingET* met = containers.met;
  const xAOD::MissingET* mpt = containers.mettrack;
  const xAOD::ElectronContainer* electrons = containers.electrons;
  const xAOD::ElectronContainer* forwardelectrons = containers.forwardelectrons;
  //const xAOD::PhotonContainer* photons = containers.photons;
  const xAOD::MuonContainer* muons = containers.muons;
  const xAOD::TauJetContainer* taus = containers.taus;
  const xAOD::JetContainer* jets = containers.jets;
  // const xAOD::JetContainer* fatjets = containers.fatjets;
  // const xAOD::JetContainer* trackjets = containers.trackjets;
  //const xAOD::TrackParticleContainer* TrackPart = containers.TrackPart;
  // const xAOD::TruthParticleContainer* truthParticles = containers.truthParticles;
  // const xAOD::TruthVertexContainer* truthVertex = containers.truthVertex;

  clearResult();

  if (m_debug) Info("passPreSelection()", "In Pre Selection");
  m_cutFlow.count("Full");

  m_result.met = met;
  m_result.mpt = mpt;

  // for (unsigned int iPart = 0; iPart < TrackPart->size(); ++iPart) {
  //   const xAOD::TrackParticle * Part = TrackPart->at(iPart);
  //   m_result.TrackPart.push_back(Part);

  // }

  // for (unsigned int iPhot = 0; iPhot < photons->size(); ++iPhot) {
  //   const xAOD::Photon * Photon = photons->at(iPhot);
  //   m_result.Photons.push_back(Photon);
  // }

  // this method is called from the HSG5framework and determines whether this event should be kept or skipped.
  // Note: If any sys. variation flags the event to be kept, it will be kept. So the event selection (and OR)
  // needs to be performed again in subsequent analysis (if writting the CxAOD to disk)

  // return values:
  // 0 : didn't pass selection
  // 1 : passed selection

  // event cleaning
  bool passEventCleaning = true;
  bool isMC = Props::isMC.get(evtinfo);
  passEventCleaning &= Props::hasPV.get(evtinfo);
  if (!isMC) {
    passEventCleaning &= Props::passGRL.get(evtinfo);
    passEventCleaning &= Props::isCleanEvent.get(evtinfo);
  }

  // if (isMC) {
  //   for (unsigned int iPart = 0; iPart < truthParticles->size(); ++iPart) {
  //     const xAOD::TruthParticle* Part = truthParticles->at(iPart);
  //     m_result.truthParticles.push_back(Part);
  //   }
  // }

  if (!passEventCleaning) return false;
  if (m_debug) Info("passPreSelection()", "Finished Event Cleaning");
  m_cutFlow.count("EventCleaning");

  m_result.el.clear();
  m_result.fwdel.clear();
  m_result.mu.clear();
  m_result.ta.clear();

  _el.clear();
  _mu.clear();
  _ta.clear();
  _fwdel.clear();

  int lep_result = doWWWLeptonPreSelection(electrons, forwardelectrons, muons, taus, _el, _fwdel, _mu, _ta);
  //if( doDBLeptonPreSelection( electrons, muons, el1, el2, mu1, mu2 ) == 1 ){

  lep_result = _el.size() + _mu.size();

  if (lep_result >= 1) {
    if (m_debug) Info("passPreSelection()", "Passed Lepton PreSelection");
  } else if (lep_result <= 0) {
    if (m_debug) Info("passPreSelection()", "Failed Lepton Selection");
    //return false;
  }
  m_cutFlow.count("AtLeastOneLepton");

  m_result.el = _el;
  m_result.mu = _mu;
  m_result.ta = _ta;
  m_result.fwdel = _fwdel;

  // jets
  //  if( !passJetPreSelection( jets, fatjets, trackjets ) ){
  //  // very loose jet cut for first iteration
  //  // at least one fat jet or at least two akt4 jets
  //    Info("passPreSelection()", "Failed Jet Preselection");
  //    return false;
  //  }
  // if (!passJetPreSelection(jets, fatjets, trackjets)) {
  //   // currently don't do anything on jets but filling the jet containers in m_result
  // }
  // if (!passJetPreSelection(jets, jets, jets)) {
  //   // currently don't do anything on jets but filling the jet containers in m_result
  // }

  m_result.smallJets.clear();

  for (unsigned int iJet = 0; iJet < jets->size(); ++iJet) {
    const xAOD::Jet* jet = jets->at(iJet);
    // if( !Props::goodJet.exists(jet) ) continue;
    //if( Props::OROutputLabel.get(jet) ) continue;
    // if (!Props::passOR.get(jet)) {
    //   std::cout << "!!!! not pass OR Jet !!!" << std::endl;
    //   continue;
    // }
    if (!Props::isSignalJet.get(jet)) continue;
    m_result.smallJets.push_back(jet);
  }
  std::sort(m_result.smallJets.begin(), m_result.smallJets.end(), sort_pt);

  if (m_debug) Info("passPreSelection()", "Passed Jet Selection");
  m_cutFlow.count("JetPreRequirement");

  // trigger
  //if ( !passTriggerSelection(const xAOD::EventInfo* evtinfo) ) return false;
  if (m_debug) Info("passPreSelection()", "Passed Trigger Selection");
  m_cutFlow.count("Trigger");

  return true;
}

//EL::StatusCode PreEvtSelection::writeEventVariables(const xAOD::EventInfo* eventInfoIn, xAOD::EventInfo* eventInfoOut, bool isKinVar,
//                                                    bool isWeightVar, std::string sysName, int rdm_RunNumber,
//                                                    CP::MuonTriggerScaleFactors* trig_sfmuon) {
//  return EL::StatusCode::SUCCESS;
//}

// ----------- Selection Functions -------------//

//int PreEvtSelection::passTriggerSelection(const xAOD::EventInfo* evtinfo){
//  if(m_TriggerList.size()){
//    if( !(DBProps::passHLT_mu26_imedium.get(evtinfo) ||
//    DBProps::passHLT_mu50.get(evtinfo) ||
//    DBProps::passHLT_e28_tight_iloose.get(evtinfo) ||
//    DBProps::passHLT_e60_medium.get(evtinfo) ) ){
//      return 0;
//    }
//  }
//  return 1;
//}

// int PreEvtSelection::passJetPreSelection(const xAOD::JetContainer* akt4jets, const xAOD::JetContainer* /*fatjets*/,
//                                          const xAOD::JetContainer* /*trackjets*/) {
//   m_result.smallJets.clear();
//   // m_result.largeJets.clear();
//   // m_result.trackJets.clear();

//   int njets = 0;
//   for (unsigned int iJet = 0; iJet < akt4jets->size(); ++iJet) {
//     const xAOD::Jet* jet = akt4jets->at(iJet);
//     // if( !Props::goodJet.exists(jet) ) continue;
//     //if( Props::OROutputLabel.get(jet) ) continue;
//     if (!Props::passOR.get(jet)) {
//       std::cout << "!!!! not pass OR Jet !!!" << std::endl;
//       continue;
//     }
//     m_result.smallJets.push_back(jet);
//     njets++;
//   }
//   std::sort(m_result.smallJets.begin(), m_result.smallJets.end(), sort_pt);

//   // int nfatjets=0;
//   // for(unsigned int iFatjet=0; iFatjet < fatjets->size(); ++iFatjet ){
//   //   const xAOD::Jet* fatjet = fatjets->at(iFatjet);
//   //   //if( !Props::passPreSel.get(fatjet)) continue; //Props::passPreSel applies pt/eta cut
//   //   if( !Props::isFatJet.get(fatjet)) continue; //Props::isFatJet applies pt/eta cut
//   //   m_result.largeJets.push_back(fatjet);
//   //   nfatjets++;
//   // }
//   // std::sort(m_result.largeJets.begin(), m_result.largeJets.end(), sort_pt);

//   // if(trackjets){

//   //   for (unsigned int iJet(0) ; iJet < trackjets->size(); iJet++) {
//   //       //apply some selection?
//   //       const xAOD::Jet * jet = trackjets->at(iJet);
//   //           m_result.trackJets.push_back(jet);
//   //   }

//   // }
//   // if( njets < 2 ) return 0;
//   //if( nfatjets < 1 || njets < 1 ) return 0;
//   //if( nfatjets<1 ) return 0;

//   return 1;
// }

//int PreEvtSelection::passJetSelection(const xAOD::JetContainer* fatjets) { return 1; }
