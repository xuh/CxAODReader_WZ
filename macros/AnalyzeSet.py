import sys
import ROOT
import os
from math import sqrt
from array import array
from WWWPlot import *
from MTree import MTree
import collections

import DataSets # All Information about the datasets are stored here
import commands
import time
import errno

ReSubmit=True
Verbose=False 
Systs=False



ddVariations=["NonPromptBinning_E","NonPromptBtag77_E","NonPromptStat_E","NonPromptBinning_M","NonPromptBtag77_M","NonPromptStat_M","CFwBG","CFStatSys","VgammaEEE","VgammaStat","WvsZgamma"]

def MakeTable(EventCountDict, EventRawDict,sFolder, Region,Blind, Mode="2l2j", Tag=""):

    if Mode=="q0q1":
        NChan=2
    elif Mode=="q0q13l":
        NChan=2        
    else:
        NChan=4


    NominalOnly=False
    if len(EventCountDict.keys())==1 and "Nominal" in EventCountDict.keys():
        NominalOnly=True


    EventCount=EventCountDict["Nominal"]
    EventRaw=EventRawDict["Nominal"]

    EventSystUp={}
    EventSystVarUp={}
    EventSystDown={}
    EventSystVarDown={}

    for key in EventCount:
        EventSystUp[key] = [ 0.0 for Chan in range(0,NChan)]
        EventSystDown[key] = [ 0.0 for Chan in range(0,NChan)]


    SystNames = EventCountDict.keys()
    

    for Var in EventCountDict:
        if "Nominal" is Var: continue


        MClist =  EventCountDict[Var].keys()
        hasDown = ("__1up" in Var and Var.replace("__1up","__1down") in SystNames)
        if "__1down" in Var: continue
        if hasDown:
            Var = Var.replace("__1up","")

        EventSystVarUp[Var] = [ 0.0 for Chan in range(0,NChan)]
        EventSystVarDown[Var] = [ 0.0 for Chan in range(0,NChan)]

        for key in MClist:
            if (not Var in ddVariations) and (("Charge Flip" in str(key)) or ("Non Prompt" in str(key)) and ("Vgamma" in str(key))) :
                continue

            if "data" in str(key).lower():
                continue


            for Chan in range(0,NChan):
                if hasDown:
                    # print EventCountDict.keys()
                    # print Var,key,Chan
                    # print EventCountDict[Var+"__1up"]
                    # print EventCountDict[Var+"__1up"][key]
                    # print EventCountDict[Var+"__1up"][key][Chan]

                    EventSystUp[key][Chan] += (EventCountDict[Var+"__1up"][key][Chan]-EventCountDict["Nominal"][key][Chan])**2
                    EventSystVarUp[Var][Chan] += (EventCountDict[Var+"__1up"][key][Chan]-EventCountDict["Nominal"][key][Chan])**2
                    EventSystDown[key][Chan] += (EventCountDict[Var+"__1down"][key][Chan]-EventCountDict["Nominal"][key][Chan])**2
                    EventSystVarDown[Var][Chan] += (EventCountDict[Var+"__1down"][key][Chan]-EventCountDict["Nominal"][key][Chan])**2

                else:
                    EventSystUp[key][Chan] += (EventCountDict[Var][key][Chan]-EventCountDict["Nominal"][key][Chan])**2
                    EventSystVarUp[Var][Chan] += (EventCountDict[Var][key][Chan]-EventCountDict["Nominal"][key][Chan])**2
                    EventSystDown[key][Chan] += (EventCountDict[Var][key][Chan]-EventCountDict["Nominal"][key][Chan])**2
                    EventSystVarDown[Var][Chan] += (EventCountDict[Var][key][Chan]-EventCountDict["Nominal"][key][Chan])**2
            

    for key in EventCount:
        for Chan in range(0,NChan):
            EventSystUp[key][Chan]=sqrt(EventSystUp[key][Chan])
            EventSystDown[key][Chan]=sqrt(EventSystDown[key][Chan])

    for Var in EventSystVarUp:
        for Chan in range(0,NChan):
            EventSystVarUp[Var][Chan]=sqrt(EventSystVarUp[Var][Chan])
            EventSystVarDown[Var][Chan]=sqrt(EventSystVarDown[Var][Chan])
    
    try:
      os.makedirs(sFolder)
    except OSError as e:
      if e.errno != errno.EEXIST:
        raise  


    fTable = open(sFolder+Tag+Region.replace(" ","")+'.tex','w')

    LineData=""
    LineDataPrint=""
    if Mode=="q0q1"  or  Mode=="q0q13l":
        print >> fTable, "\\begin{tabular}{l|c c}"
    elif Mode=="3l"  or Mode=="2l2j" or Mode=="q0q1Split":
        print >> fTable, "\\begin{tabular}{l|c c c c}"
        
    print >> fTable, "\\hline\\hline"

    if Mode=="q0q1":
        print >> fTable, " & $q_0 q_1=-1$ & $q_0 q_1=1$ \\\\\\hline"
    elif Mode=="q0q1Split":
        print >> fTable, " & \\multicolumn{2}{ c | }{$ee\\mu$} & \\multicolumn{2}{ c | }{$\\mu\\mu e$} \\\\\\hline"
        print >> fTable, " & $q_0 q_1=-1$ & $q_0 q_1=1$  & $q_0 q_1=-1$ & $q_0 q_1=1$ \\\\\\hline"
    elif Mode=="2l2j":
        print >> fTable, " & $e^\\pm e^\\pm$ & $e^\\pm \mu^\\pm$ & $\mu^\\pm e^\\pm$ & $\mu^\\pm \mu^\\pm$ \\\\\\hline "
    elif Mode=="3l":
        print >> fTable, " & $e e e$ & $e e \mu$ & $e \mu \mu$ & $\mu \mu \mu$ \\\\\\hline"
    elif Mode=="q0q13l" :
        print >> fTable, " & $e e \mu$ & $\mu \mu e$ \\\\\\hline"


    # #Print the final yield and create latex tables
    TotalError=[0. for i in range(0,NChan)]
    TotalSysUp=[0. for i in range(0,NChan)]
    TotalSysDown=[0. for i in range(0,NChan)]
    TotalSum=[0. for i in range(0,NChan)]
    TotalDD=[0. for i in range(0,NChan)]

    for key in EventCount:

        if "data" in key.lower() and Blind: 
            continue



        line = key.replace("#gamma","$\\gamma$")
        linePrint = key
        for Chan in range(0,NChan):
            Error=0.0
            if EventRaw[key][Chan]>0:
                Error = sqrt(EventRaw[key][Chan])
            if "data" in key.lower():
                line = line.replace("#","\\") + " & $"+str(round(EventCount[key][Chan],2))+"$"
                linePrint = linePrint + " , " + str(round(EventCount[key][Chan],2)) 
            else:

                if not NominalOnly:
                    line = line.replace("#","\\") + " & $"+str(round(EventCount[key][Chan],2))+"\\pm"+str(round(Error,2))+"^{+"+str(round(EventSystUp[key][Chan],2))+"}_{-"+ str(round(EventSystDown[key][Chan],2)) +"}$"
                else:
                    line = line.replace("#","\\") + " & $"+str(round(EventCount[key][Chan],2))+"\\pm"+str(round(Error,2))+"$"
                
                linePrint = linePrint + " , " + str(round(EventCount[key][Chan],2)) 
                TotalError[Chan]+=(Error)**2
                TotalSysUp[Chan]+=EventSystUp[key][Chan]**2
                TotalSysDown[Chan]+=EventSystDown[key][Chan]**2
                TotalSum[Chan]+=EventCount[key][Chan]
                

        line += "\\\\"
        if "data" in key.lower():
            LineData=line
            LineDataPrint=linePrint
        else:
            print >> fTable, line
            print linePrint


    print >> fTable, "\\hline"
    tmpListWEr = []
    tmpList = []
    PercentSys=[0.0 for i in range(0,NChan)]
    for chan in range(0,NChan):
        TotalError[chan]=sqrt(TotalError[chan])
        TotalSysUp[chan]=sqrt(TotalSysUp[chan])
        TotalSysDown[chan]=sqrt(TotalSysDown[chan])
        if not NominalOnly:
            tmpListWEr.append("$"+str(round(TotalSum[chan],2))+"\\pm"+str(round(TotalError[chan],2))+"^{+"+str(round(TotalSysUp[chan],2))+"}_{-"+str(round(TotalSysDown[chan],2))+"}$")
        else:
            tmpListWEr.append("$"+str(round(TotalSum[chan],2))+"\\pm"+str(round(TotalError[chan],2))+"$")

        tmpList.append(str(round(TotalSum[chan],2)))
        
        if TotalSum[chan]>0:
            PercentSys[chan]=TotalSysUp[chan]/TotalSum[chan] if  TotalSysUp[chan]>TotalSysDown[chan] else TotalSysUp[chan]/TotalSum[chan]

    print >> fTable, "Total Sum &"+"&".join(tmpListWEr) +" \\\\"
    print "Total Sum",","," , ".join(tmpList)
    print >> fTable, LineData
    print LineDataPrint
    print >> fTable, "\\hline\\hline"
    print >> fTable, "\\end{tabular}"



    if not NominalOnly:
        fSyst = open(sFolder+Tag+Region.replace(" ","")+'Systematics.tex','w')

        LineData=""
        LineDataPrint=""

        if Mode=="q0q1":
            print >> fTable, "\\begin{tabular}{l|c c}"
        if Mode=="3l" or Mode=="q0q13l" or Mode=="2l2j":
            print >> fTable, "\\begin{tabular}{l|c c c c}"
        
        if Mode=="Non Prompt":
            print >> fTable, "\\begin{tabular}{l|c c c c}"



        print >> fTable, "\\hline\\hline"

        if Mode=="q0q1":
            print >> fTable, " & $q_0 q_1=-1$ & $q_0 q_1=1$ \\\\\\hline"
        if Mode=="2l2j":
            print >> fTable, " & $e^\\pm e^\\pm$ & $e^\\pm \mu^\\pm$ & $\mu^\\pm e^\\pm$ & $\mu^\\pm \mu^\\pm$ \\\\\\hline"
        if Mode=="3l" or Mode=="q0q13l":
            print >> fTable, " & $e e e$ & $e e \mu$ & $e \mu \mu$ & $\mu \mu \mu$ \\\\\\hline"

        if Mode=="Non Prompt":
            print >> fTable, " & $e e \mu$ & & $e \mu \mu$ &  \\\\\\hline"


        for Var in EventSystVarUp:
            line = Var 
            for Chan in range(0,NChan):
                if TotalSum[Chan]>0:
                    if not Var in ddVariations:
                        line +="& $ ^{+"+str(round(EventSystVarUp[Var][Chan]/TotalSum[Chan]*100,2)) +"\%} _{-"+str(round(EventSystVarDown[Var][Chan]/TotalSum[Chan]*100,2))+"\%}$"
                    elif "NonPrompt" in Var:
                        line +="& $ ^{+"+str(round(EventSystVarUp[Var][Chan]/(EventCount["Non Prompt"][Chan]+0.0001)*100,2)) +"\%} _{-"+str(round(EventSystVarDown[Var][Chan]/(EventCount["Non Prompt"][Chan]+0.0001)*100,2))+"\%}$"
                    elif "CF" in Var:
                        line +="& $ ^{+"+str(round(EventSystVarUp[Var][Chan]/(EventCount["Charge Flip"][Chan]+0.0001)*100,2)) +"\%} _{-"+str(round(EventSystVarDown[Var][Chan]/(EventCount["Charge Flip"][Chan]+0.0001)*100,2))+"\%}$"                  
                else:
                    line += "& $ 0  \% $"

            line += "\\\\"
            print >> fSyst, line


        print >> fSyst, "\\hline"
        print >> fSyst, "\\end{tabular}"




    
    return PercentSys


def ExpandData(Folder,SetName,SetNumbers):
    # if "data" in SetName.lower():
        DataNum=[]
        try:
            FileList = os.listdir(Folder)
            for SetNumber in SetNumbers:
                tmpDataNum = [sFile for sFile in FileList if str(SetNumber) in sFile]

                if len(tmpDataNum)==0:
                    print SetName,SetNumber,Folder,"cannot be found"
                else:
                    DataNum+=tmpDataNum
            return DataNum
        except:
            print "Failed in expading data"
    # else:
    #     return SetNumbers




def RunSet(Region="WWW SR",SetName="",SetNumber="",Folder="",SaveFolder="",Plots={},doQCDnCF=[True,True],MakeEffPlots=True,MakeTables=True,Verbose=True,Blind=False, EventPrint=False, Mode="2l2j", Variations=["Nominal"]):

    NChan=4
    if Mode=="q0q1":
        NChan=2
    elif Mode=="q0q13l":
        NChan=2        



    ROOT.gROOT.SetBatch()
    ROOT.gStyle.SetOptStat(0)
    ROOT.gROOT.ProcessLine( "gErrorIgnoreLevel = 2001;")
    # print "Starting Run"
    # print SetName,SetNumber,Folder,SaveFolder,Plots,doQCDnCF,MakeEffPlots,MakeTables,Verbose,Blind, EventPrint
    # print Variations

    ExtraHists=[]
    if doQCDnCF[0]:
        ExtraHists.append("Non Prompt")
    if doQCDnCF[1]:
        ExtraHists.append("Charge Flip")
    if doQCDnCF[1]:
        ExtraHists.append("V#gamma")

    EventCount=collections.OrderedDict()
    EventRaw=collections.OrderedDict()
    QCDEventCount=collections.OrderedDict()
    QCDEventRaw=collections.OrderedDict()
    NoBLEventCount=collections.OrderedDict()
    NoBLEventRaw=collections.OrderedDict()
    CFEventCount=collections.OrderedDict()
    CFEventRaw=collections.OrderedDict()


    sFolder = Folder.split("/")[Folder.split("/").index("data-MVATree")-1]
    
    RootFolder=SaveFolder
    RootFolder+=sFolder+"/"
    RootFolder+=Region.replace(" ","")+"/"
    RootFolder+=SetName.replace(" ","").replace("#","")+"/"
    #print "Save Folder",RootFolder
    try:
      os.makedirs(RootFolder)
    except OSError as e:
      if e.errno != errno.EEXIST:
        raise

    for Variation in Variations:

        for Name in ExtraHists+[SetName]:
            EventCount[Name]=[0.0 for i in range(0,NChan)];
            EventRaw[Name]=[0.0 for i in range(0,NChan)];
            QCDEventCount[Name]=[0.0 for i in range(0,NChan)];
            QCDEventRaw[Name]=[0.0 for i in range(0,NChan)];
            CFEventCount[Name]=[0.0 for i in range(0,NChan)];
            CFEventRaw[Name]=[0.0 for i in range(0,NChan)];
            NoBLEventCount[Name]=[0.0 for i in range(0,NChan)];
            NoBLEventRaw[Name]=[0.0 for i in range(0,NChan)];



        RootFile = ROOT.TFile(RootFolder+str(SetNumber)+Variation+".root","recreate")
        OutTxt = open(RootFolder+str(SetNumber)+Variation+".info","w")

        
        hEff=[]
        hEffQCD=[]
        hEffCF=[]
        hEffNoBL=[]
        if MakeEffPlots:
            for chan in range(0,NChan+1):
                strChan=str(chan)
                if chan==NChan:
                    strChan="Inc"

                hEff.append(ROOT.TH1F("cutflow_"+SetName.replace(" ","").replace("#","")+"_"+strChan,"cutflow "+SetName+strChan,40,0,40))
                if doQCDnCF[0]:
                    hEffQCD.append(ROOT.TH1F("cutflow_"+SetName.replace(" ","").replace("#","")+"_QCD_"+strChan,"cutflow "+SetName+" "+strChan,40,0,40))
                if doQCDnCF[1]:
                    hEffCF.append(ROOT.TH1F("cutflow_"+SetName.replace(" ","").replace("#","")+"_CF_"+strChan,"cutflow "+SetName+" "+strChan,40,0,40))
                if doQCDnCF[1]:
                    hEffNoBL.append(ROOT.TH1F("cutflow_"+SetName.replace(" ","").replace("#","")+"_NoBL_"+strChan,"cutflow "+SetName+" "+strChan,40,0,40))



        Histograms=collections.OrderedDict() #Histogram Array init
        HistogramsQCD=collections.OrderedDict() #Histogram Array init
        HistogramsCF =collections.OrderedDict() #Histogram Array init
        HistogramsNoBL =collections.OrderedDict() #Histogram Array init
        for plot in Plots.keys():
            Histograms[plot] = [collections.OrderedDict() for i in range(0,NChan+1)] 
            if doQCDnCF[0]:
                HistogramsQCD[plot] = [collections.OrderedDict() for i in range(0,NChan+1)] 
            if doQCDnCF[1]:
                HistogramsCF[plot] = [collections.OrderedDict() for i in range(0,NChan+1)] 
            if doQCDnCF[1]:
                HistogramsNoBL[plot] = [collections.OrderedDict() for i in range(0,NChan+1)] 


        for key in Histograms.keys():
            for chan in range(0,NChan+1):
                for tmpSetName in [SetName]+ExtraHists:

                    strChan=str(chan)
                    if chan==NChan:
                        strChan="Inc"
                
                    #print Plots[key]["RelBins"]
                    if "RelBins" in Plots[key].keys():
                        Histograms[key][chan][tmpSetName] = ROOT.TH1F(key+"_"+tmpSetName.replace(" ","").replace("#","")+"_"+strChan,key+" "+tmpSetName.replace(" ","").replace("#","")+" "+strChan,len(Plots[key]["RelBins"])-1,array('d',Plots[key]["RelBins"]))
                        Histograms[key][chan][tmpSetName].Sumw2()
                        if doQCDnCF[0]:
                            HistogramsQCD[key][chan][tmpSetName] = ROOT.TH1F("QCD_"+key+"_"+tmpSetName.replace(" ","").replace("#","")+"_"+strChan,"QCD_"+key+" "+tmpSetName.replace(" ","").replace("#","")+" "+strChan,len(Plots[key]["RelBins"])-1,array('d',Plots[key]["RelBins"]))
                            HistogramsQCD[key][chan][tmpSetName].Sumw2()
                        if doQCDnCF[1]:
                            HistogramsCF[key][chan][tmpSetName] = ROOT.TH1F("CF_"+key+"_"+tmpSetName.replace(" ","").replace("#","")+"_"+strChan,"CF_"+key+" "+tmpSetName.replace(" ","").replace("#","")+" "+strChan,len(Plots[key]["RelBins"])-1,array('d',Plots[key]["RelBins"]))
                            HistogramsCF[key][chan][tmpSetName].Sumw2()
                        if doQCDnCF[1]:
                            HistogramsNoBL[key][chan][tmpSetName] = ROOT.TH1F("NoBL_"+key+"_"+tmpSetName.replace(" ","").replace("#","")+"_"+strChan,"NoBL_"+key+" "+tmpSetName.replace(" ","").replace("#","")+" "+strChan,len(Plots[key]["RelBins"])-1,array('d',Plots[key]["RelBins"]))
                            HistogramsNoBL[key][chan][tmpSetName].Sumw2()

                    else:  
                        Histograms[key][chan][tmpSetName] = ROOT.TH1F(key+"_"+tmpSetName.replace(" ","").replace("#","")+"_"+strChan,key+" "+tmpSetName.replace(" ","").replace("#","")+" "+strChan,Plots[key]["NBins"],Plots[key]["xmin"],Plots[key]["xmax"])
                        Histograms[key][chan][tmpSetName].Sumw2()
                        if doQCDnCF[0]:
                            HistogramsQCD[key][chan][tmpSetName] = ROOT.TH1F("QCD_"+key+"_"+tmpSetName.replace(" ","").replace("#","")+"_"+strChan,"QCD_"+key+" "+tmpSetName.replace(" ","").replace("#","")+" "+strChan,Plots[key]["NBins"],Plots[key]["xmin"],Plots[key]["xmax"])
                            HistogramsQCD[key][chan][tmpSetName].Sumw2()
                        if doQCDnCF[1]:
                            HistogramsCF[key][chan][tmpSetName] = ROOT.TH1F("CF_"+key+"_"+tmpSetName.replace(" ","").replace("#","")+"_"+strChan,"CF_"+key+" "+tmpSetName.replace(" ","").replace("#","")+" "+strChan,Plots[key]["NBins"],Plots[key]["xmin"],Plots[key]["xmax"])
                            HistogramsCF[key][chan][tmpSetName].Sumw2()
                        if doQCDnCF[1]:
                            HistogramsNoBL[key][chan][tmpSetName] = ROOT.TH1F("NoBL_"+key+"_"+tmpSetName.replace(" ","").replace("#","")+"_"+strChan,"NoBL_"+key+" "+tmpSetName.replace(" ","").replace("#","")+" "+strChan,Plots[key]["NBins"],Plots[key]["xmin"],Plots[key]["xmax"])
                            HistogramsNoBL[key][chan][tmpSetName].Sumw2()



        FileList = os.listdir(Folder)
        if Verbose:
            print "Working on the MC Set",SetName,Folder


        #Locating the file
        fileName = [sFile for sFile in FileList if str(SetNumber) in sFile]


        #If multiples of the same MC number is found throw error and skip
        #For Data this is not valid
        if (not "data" in str(SetNumber)) and  len(fileName)==0:
            #print "!Error",SetName,SetNumber,"Cannot be found or has multiples."
            SaveFolder = SaveFolder.replace("Root/","condor_logs/")
            f= open(SaveFolder+str(SetNumber)+".NotFound","w+")
            print >> f, SetName,SetNumber,Folder
            return -1


                #Getting the Lumi correction, normally just returns 1, CxAOD reader handles this
        LumiWeight = 1 #1e3 #DataSets.GetLumiWeight(SetNumber)
                #Makesure Lumi Correction is correct
        # if LumiWeight < 0.:
        #     print "LumiWeight cannot be calculated for run",SetNumber
        #     exit

                #Loops over all the files for the given Data/MC sample
                #For MC len(fileName)=1,(only one file per MC sample) so this loop only works on data, for MC it doens't do much.
        for tmpFile in fileName: 

            #Gets the file and reads it and gets the TTree defined in MTree.py
            nRoot = Folder + "/"+tmpFile

            tmpVariation = "Nominal"
            if not Variation in ddVariations:
                tmpVariation = Variation
                if "data" in SetName.lower() and not Variation is "Nominal":
                    continue

            Tree = MTree(nRoot,tmpVariation)
            nentries = Tree.fTree.GetEntries()


            #Loops over every entry getting the cuts
            for TreeIndex in range(0,nentries):
                Tree.fTree.GetEntry(TreeIndex)


                IsQCD = Tree.IsQCD()
                #IsQCDTT = Tree.IsQCDTT()
                IsQCDTT = Tree.IsQCD()
                IsCF = Tree.IsCF()
                IsNoBL = Tree.IsNoBL()
                # if IsNoBL:
                #     print "No BL",IsNoBL
                if Mode=="3l":
                    IsCF= False
                elif "q0q1"==Mode or "q0q1Split"==Mode or Mode=="q0q13l":
                    IsCF= Tree.IsCF3Lep() 
                elif "Non Prompt"==Mode:
                    IsQCD = Tree.IsQCD()
                    IsQCDTT = Tree.IsQCDTT()
                    IsCF = False
                elif "Non Prompt2"==Mode:
                    IsQCD = Tree.IsQCD()
                    IsQCDTT = Tree.IsQCDTT()
                    IsCF = False
                elif "Non Prompt3"==Mode:
                    IsQCD = Tree.IsQCD()
                    IsQCDTT = Tree.IsQCD()
                    
                    
                


                if Blind and "data" in SetName.lower() and not IsQCD and not IsCF and not IsNoBL:
                    continue

                #MC weight is calculated
                weight=1.0
                if not "data" in SetName.lower():
                    weight= Tree.Branches["weight"][0]*LumiWeight

                # if "W#gamma" in SetName:
                #     weight*=1.95
                # if "Z#gamma" in SetName and Lepton3:
                #     weight*=1.95



                #Cutflows per regoin is defined, it's in MTree
                cuts = Tree.GetCuts(Region)

                #Runs over the given cutflow
                passRegion=True

                Flav1=Tree.Branches["Lep1Flav"][0]
                Flav2=Tree.Branches["Lep2Flav"][0]
                Flav3=Tree.Branches["Lep3Flav"][0]

                Nel=Tree.Branches["Nel"][0]
                Nmu=Tree.Branches["Nmu"][0]

                Nlel=Tree.Branches["Nlooseel"][0]
                Nlmu=Tree.Branches["Nloosemu"][0]


                Chan=-1            


                if Mode=="q0q1":
                    q0q1 = Tree.GetObject("q0q1")

                    if q0q1==-1:
                        Chan=0
                    elif q0q1==1:
                        Chan=1

                elif Mode=="q0q1Split":
                    q0q1 = Tree.GetObject("q0q1")

                    if Nel==2 and Nmu==1:
                        if q0q1==-1:
                            Chan=0
                        elif q0q1==1:
                            Chan=1
                    elif Nel==1 and Nmu==2:
                        if q0q1==-1:
                            Chan=2
                        elif q0q1==1:
                            Chan=3


                    
                elif Mode=="3l":
                        if Nel==3 and Nmu==0:
                            Chan=0
                        elif Nel==2 and Nmu==1:
                            Chan=1
                        elif Nel==1 and Nmu==2:
                            Chan=2
                        elif Nel==0 and Nmu==3:
                            Chan=3

                elif Mode=="q0q13l":
                        if Nel==2 and Nmu==1:
                            Chan=0
                        elif Nel==1 and Nmu==2:
                            Chan=1






                elif Mode=="2l2j":
                        if Flav1==1 and Flav2==1:
                            Chan=0
                        elif (Flav1==1 and Flav2==2) :
                            Chan=1
                        elif (Flav1==2 and Flav2==1):
                            Chan=2
                        elif Flav1==2 and Flav2==2:
                            Chan=3

                elif Mode=="Non Prompt":
                    if IsQCD:
                        FakeLep=[0,0]
                        if Tree.Branches["Lep1isLoose"][0]==True:
                            FakeLep=[Tree.Branches["Lep1Flav"][0],Tree.Branches["Lep1Q"][0]]
                        elif Tree.Branches["Lep2isLoose"][0]==True:
                            FakeLep=[Tree.Branches["Lep2Flav"][0],Tree.Branches["Lep2Q"][0]]
                        elif Tree.Branches["Lep3isLoose"][0]==True:
                            FakeLep=[Tree.Branches["Lep3Flav"][0],Tree.Branches["Lep3Q"][0]]

                        ElecQ=0
                        MuonQ=0
                        if Tree.Branches["Lep1Flav"][0]==1:
                            ElecQ+=Tree.Branches["Lep1Q"][0]
                        elif Tree.Branches["Lep1Flav"][0]==2:
                            MuonQ+=Tree.Branches["Lep1Q"][0]
                        if Tree.Branches["Lep2Flav"][0]==1:
                            ElecQ+=Tree.Branches["Lep2Q"][0]
                        elif Tree.Branches["Lep2Flav"][0]==2:
                            MuonQ+=Tree.Branches["Lep2Q"][0]
                        if Tree.Branches["Lep3Flav"][0]==1:
                            ElecQ+=Tree.Branches["Lep3Q"][0]
                        elif Tree.Branches["Lep3Flav"][0]==2:
                            MuonQ+=Tree.Branches["Lep3Q"][0]

                        if Nel==2 and Nmu==1 and Nlmu==1:
                            Chan=1
                        if Nel==1 and Nmu==2 and Nlel==1:
                            Chan=3
                        if Nel==2 and Nmu==1 and Nlel==1:
                            Chan=0
                        if Nel==1 and Nmu==2 and Nlmu==1:
                            Chan=2
                    else:
                        if Nel==2 and Nmu==1:
                            Chan=0
                        if Nel==1 and Nmu==2:
                            Chan=2

                elif Mode=="Non Prompt2":
                    if IsQCD:
                        FakeLep=[0,0]
                        if Tree.Branches["Lep1isLoose"][0]==True:
                            FakeLep=[Tree.Branches["Lep1Flav"][0],Tree.Branches["Lep1Q"][0]]
                        elif Tree.Branches["Lep2isLoose"][0]==True:
                            FakeLep=[Tree.Branches["Lep2Flav"][0],Tree.Branches["Lep2Q"][0]]
                        elif Tree.Branches["Lep3isLoose"][0]==True:
                            FakeLep=[Tree.Branches["Lep3Flav"][0],Tree.Branches["Lep3Q"][0]]

                        ElecQ=0
                        MuonQ=0
                        if Tree.Branches["Lep1Flav"][0]==1:
                            ElecQ+=Tree.Branches["Lep1Q"][0]
                        elif Tree.Branches["Lep1Flav"][0]==2:
                            MuonQ+=Tree.Branches["Lep1Q"][0]
                        if Tree.Branches["Lep2Flav"][0]==1:
                            ElecQ+=Tree.Branches["Lep2Q"][0]
                        elif Tree.Branches["Lep2Flav"][0]==2:
                            MuonQ+=Tree.Branches["Lep2Q"][0]
                        if Tree.Branches["Lep3Flav"][0]==1:
                            ElecQ+=Tree.Branches["Lep3Q"][0]
                        elif Tree.Branches["Lep3Flav"][0]==2:
                            MuonQ+=Tree.Branches["Lep3Q"][0]

                        if Nel==2 and Nmu==1 and Nlmu==1:
                            Chan=0
                        if Nel==1 and Nmu==2 and Nlel==1:
                            Chan=1
                        if Nel==2 and Nmu==1 and Nlel==1 and FakeLep[1]==-1*MuonQ :
                            Chan=2
                        if Nel==1 and Nmu==2 and Nlmu==1 and FakeLep[1]==-1*ElecQ:
                            Chan=3

                    else:
                        if Nel==2 and Nmu==1:
                            Chan=0
                        if Nel==1 and Nmu==2:
                            Chan=2


                elif Mode=="Non Prompt3":
                    if IsQCD:
                        if Nel==2 and Nmu==0 and Nlel==1:
                            Chan=0
                        if Nel==1 and Nmu==1 and Nlel==1:
                            Chan=1
                        if Nel==1 and Nmu==1 and Nlmu==1:
                            Chan=2
                        if Nel==0 and Nmu==2 and Nlmu==1:
                            Chan=3



                    else:
                        if Flav1==1 and Flav2==1:
                            Chan=0
                        elif (Flav1==1 and Flav2==2) :
                            Chan=1
                        elif (Flav1==2 and Flav2==1):
                            Chan=2
                        elif Flav1==2 and Flav2==2:
                            Chan=3



                            
               
#                if Chan==-1:
#					continue
                #Fills the efficiency plot
                passRegion=True
                for tmpKeys in cuts.keys():
                    passRegion = passRegion and cuts[tmpKeys]
                    if MakeEffPlots:
                        if True:
                        #if not IsQCD and not IsCF and not IsNoBL:
                            if MakeEffPlots and passRegion:
                                hEff[Chan].Fill(tmpKeys,1)#weight)
                                if Chan==-1:
			            		continue
                                hEff[-1].Fill(tmpKeys,1)#weight)
                        elif doQCDnCF[0] and IsQCD and not IsCF and not IsNoBL:
                            if MakeEffPlots and passRegion:
                                hEffQCD[Chan].Fill(tmpKeys,1)#weight)
                                hEffQCD[-1].Fill(tmpKeys,1)#weight)
                        elif doQCDnCF[1] and not IsQCD and IsCF and not IsNoBL:
                            if MakeEffPlots and passRegion:
                                hEffCF[Chan].Fill(tmpKeys,1)#weight)
                                hEffCF[-1].Fill(tmpKeys,1)#weight)
                        elif doQCDnCF[1] and not IsQCD and not IsCF and IsNoBL:
                            if MakeEffPlots and passRegion:
                                hEffNoBL[Chan].Fill(tmpKeys,1)#weight)
                                hEffNoBL[-1].Fill(tmpKeys,1)#weight)


                                
                if passRegion and MakeEffPlots:
                    for tmpKeys in ["weight",  "weightMC",  "weightJVT", "weightBtag", "weightEffSF", "weightEffReco","weightIso","weightTTVA","weightTrig"]:
                        #print tmpKeys, Tree.GetObject(tmpKeys)
                        if not IsQCD and not IsCF and not IsNoBL:
                            if MakeEffPlots and passRegion:
                                hEff[Chan].Fill(tmpKeys,Tree.GetObject(tmpKeys))
                                hEff[-1].Fill(tmpKeys,Tree.GetObject(tmpKeys))
                        elif doQCDnCF[0] and IsQCD and not IsCF and not IsNoBL:
                            if MakeEffPlots and passRegion:
                                hEffQCD[Chan].Fill(tmpKeys,Tree.GetObject(tmpKeys))
                                hEffQCD[-1].Fill(tmpKeys,Tree.GetObject(tmpKeys))
                        elif doQCDnCF[1] and not IsQCD and IsCF and not IsNoBL:
                            if MakeEffPlots and passRegion:
                                hEffCF[Chan].Fill(tmpKeys,Tree.GetObject(tmpKeys))
                                hEffCF[-1].Fill(tmpKeys,Tree.GetObject(tmpKeys))
                        elif doQCDnCF[1] and not IsQCD and not IsCF and IsNoBL:
                            if MakeEffPlots and passRegion:
                                hEffNoBL[Chan].Fill(tmpKeys,Tree.GetObject(tmpKeys))
                                hEffNoBL[-1].Fill(tmpKeys,Tree.GetObject(tmpKeys))

                #Kill the event if not in the "REGION"
                if not (passRegion): continue 

                #Couting and filling the signal regions
                if passRegion:                            

                    # if not "data" in SetName.lower() and abs(weight)>10:
                    #     print "Found Event with High Weight"
                    #     print SetName,tmpSetName,weight,weight
                    #     continue


                    tmpSetName=""
                    if not IsQCD and not IsCF and not IsNoBL and (not SetName in DataSets.QCDOnly):


                        tmpSetName = SetName

                    elif doQCDnCF[0] and IsQCDTT and not IsCF and not IsNoBL :


                        for key in Histograms.keys():
                            if Tree.GetObject(key)<=-99: continue



                            HistogramsQCD[key][Chan][SetName].Fill(Tree.GetObject(key)*Plots[key]["Correct"],weight)


                        QCDEventCount[SetName][Chan]+=weight;
                        QCDEventRaw[SetName][Chan]+=weight*weight;

                        if Variation is "NonPromptBinning_E":
                            weight*=Tree.QCDWeight("Binning_E")
                        elif Variation is "NonPromptBinning_M":
                            weight*=Tree.QCDWeight("Binning_M")
                        elif Variation is "NonPromptStat_E":
                            weight*=Tree.QCDWeight("Stat_E")
                        elif Variation is "NonPromptStat_M":
                            weight*=Tree.QCDWeight("Stat_M")
                        elif Variation is "NonPromptBtag77_E":
                            weight*=Tree.QCDWeight("Btag77_E")
                        elif Variation is "NonPromptBtag77_M":
                            weight*=Tree.QCDWeight("Btag77_M")
                        else:
                            weight*=Tree.QCDWeight("")



                        if not "data" in SetName.lower():
                            weight*=-1


                        tmpSetName = "Non Prompt"

                    elif doQCDnCF[1] and IsCF and not IsQCD and not IsNoBL:
                        #print "Event is CFlip"

                        if "q0q1"in Mode and not "data" in SetName.lower():
                            continue

                        if Tree.CFweight()==1: continue

                        for key in Histograms.keys():
                            if Tree.GetObject(key)<=-99: continue
                            HistogramsCF[key][Chan][SetName].Fill(Tree.GetObject(key)*Plots[key]["Correct"],weight)


                        CFEventCount[SetName][Chan]+=weight;
                        CFEventRaw[SetName][Chan]+=weight*weight;

                        if Variation is "CFwBG":
                            weight*=Tree.CFweight("CFwBG")
                        elif Variation is "CFStatSys":
                            weight*=Tree.CFweight("CFStatSys")
                        else:
                            weight*=Tree.CFweight("Nominal")

                        tmpSetName = "Charge Flip"





                        if not "data" in SetName.lower():
                            weight*=-1



                    elif doQCDnCF[0] and IsCF and IsQCDTT and not IsNoBL:

                        if "q0q1" in Mode and not "data" in SetName.lower():
                            continue


                        if Tree.CFweight()==1: continue

                        if Variation is "CFwBG":
                            weight*=Tree.CFweight(Variation)
                        elif Variation is "CFStatSys":
                            weight*=Tree.CFweight(Variation)
                        else:
                            weight*=Tree.CFweight("Nominal")


                        if not "data" in SetName.lower():
                            weight*=-1

                        QCDEventCount["Charge Flip"][Chan]+=weight;
                        QCDEventRaw["Charge Flip"][Chan]+=weight*weight;


                        if Variation is "NonPromptBinning_E":
                            weight*=Tree.QCDWeight("Binning_E")
                        elif Variation is "NonPromptBinning_M":
                            weight*=Tree.QCDWeight("Binning_M")
                        elif Variation is "NonPromptStat_E":
                            weight*=Tree.QCDWeight("Stat_E")
                        elif Variation is "NonPromptStat_M":
                            weight*=Tree.QCDWeight("Stat_M")
                        elif Variation is "NonPromptBtag77_E":
                            weight*=Tree.QCDWeight("Btag77_E")
                        elif Variation is "NonPromptBtag77_M":
                            weight*=Tree.QCDWeight("Btag77_M")
                        else:
                            weight*=Tree.QCDWeight("")

                        weight*=-1

                        tmpSetName = "Non Prompt"



                    elif doQCDnCF[0] and not IsQCD and not IsCF and IsNoBL :


                        for key in Histograms.keys():
                            if Tree.GetObject(key)<=-99: continue
                            HistogramsNoBL[key][Chan][SetName].Fill(Tree.GetObject(key)*Plots[key]["Correct"],weight)

                        NoBLEventCount[SetName][Chan]+=weight;
                        NoBLEventRaw[SetName][Chan]+=weight*weight;



                        if Variation is "VgammaEEE":
                            weight*=Tree.NoBLWeight("VgammaEEE")
                        elif Variation is "VgammaStat":
                            weight*=Tree.NoBLWeight("VgammaStat")
                        elif Variation is "WvsZgamma":
                            weight*=Tree.NoBLWeight("WvsZgamma")
                        else:
                            weight*=Tree.NoBLWeight("")





                        if not "data" in SetName.lower():
                            weight*=-1


                        tmpSetName = "V#gamma"



                    if tmpSetName=="": continue
                    #if tmpSetName=="Non Prompt": continue

                    if not "data" in SetName.lower() and abs(weight)>10:
                        print "Found Event with High Weight"
                        print SetName,tmpSetName,weight,weight
                        continue





                    EventCount[tmpSetName][Chan]+=weight;
                    EventRaw[tmpSetName][Chan]+=weight*weight;


                    if EventPrint:
                        if tmpSetName=="WZ" and math.fabs(weight)>0.5:
                            print Tree.Branches["runNumber"][0], Tree.Branches["eventNumber"][0],Folder,weight

                    for key in Histograms.keys():
                        if Tree.GetObject(key)<=-99: continue
                        Histograms[key][Chan][tmpSetName].Fill(Tree.GetObject(key)*Plots[key]["Correct"],weight) #Filling Channel histogram
                        Histograms[key][-1][tmpSetName].Fill(Tree.GetObject(key)*Plots[key]["Correct"],weight) #Filling Inclusive Plots

            #Debug verbose stream per sample
            if Verbose:
                print "SetNumber:",SetNumber
                print "LumiWeight:",LumiWeight
                print 


        for chan in range(0,NChan+1):
            if MakeEffPlots:
                hEff[chan].Write() 
                if doQCDnCF[0]:
                    hEffQCD[chan].Write() 
                    hEffQCD[chan].Delete() 
                if doQCDnCF[1]:
                    hEffCF[chan].Write() 
                    hEffCF[chan].Delete()  
                if doQCDnCF[1]:
                    hEffNoBL[chan].Write() 
                    hEffNoBL[chan].Delete() 


            for key in Histograms.keys():
                for tmpSetName in [SetName]+ExtraHists:
                    Histograms[key][chan][tmpSetName].Write() 
                    if doQCDnCF[0]:
                        HistogramsQCD[key][chan][tmpSetName].Write() 
                        HistogramsQCD[key][chan][tmpSetName].Delete() 
                    if doQCDnCF[1]:
                        HistogramsCF[key][chan][tmpSetName].Write() 
                        HistogramsCF[key][chan][tmpSetName].Delete() 
                    if doQCDnCF[1]:
                        HistogramsNoBL[key][chan][tmpSetName].Write() 
                        HistogramsNoBL[key][chan][tmpSetName].Delete() 

        for Name in ExtraHists+[SetName]:
            EventCount[Name]=[str(Val) for Val in EventCount[Name]]
            print >> OutTxt, "EventCount",",",Name,",",",".join(EventCount[Name])

        for Name in ExtraHists+[SetName]:
            EventRaw[Name]=[str(Val) for Val in EventRaw[Name]]        
            print >> OutTxt, "EventRaw",",",Name,",",",".join(EventRaw[Name])

        for Name in ExtraHists+[SetName]:
            QCDEventCount[Name]=[str(Val) for Val in QCDEventCount[Name]]
            print >> OutTxt, "QCDEventCount",",",Name,",",",".join(QCDEventCount[Name])

        for Name in ExtraHists+[SetName]:
            QCDEventRaw[Name]=[str(Val) for Val in QCDEventRaw[Name]]
            print >> OutTxt, "QCDEventRaw",",",Name,",",",".join(QCDEventRaw[Name])

        for Name in ExtraHists+[SetName]:
            CFEventCount[Name]=[str(Val) for Val in CFEventCount[Name]]
            print >> OutTxt, "CFEventCount",",",Name,",",",".join(CFEventCount[Name])

        for Name in ExtraHists+[SetName]:
            CFEventRaw[Name]=[str(Val) for Val in CFEventRaw[Name]]
            print >> OutTxt, "CFEventRaw",",",Name,",",",".join(CFEventRaw[Name])



        for Name in ExtraHists+[SetName]:
            NoBLEventCount[Name]=[str(Val) for Val in NoBLEventCount[Name]]
            print >> OutTxt, "NoBLEventCount",",",Name,",",",".join(NoBLEventCount[Name])

        for Name in ExtraHists+[SetName]:
            NoBLEventRaw[Name]=[str(Val) for Val in NoBLEventRaw[Name]]
            print >> OutTxt, "NoBLEventRaw",",",Name,",",",".join(NoBLEventRaw[Name])


        OutTxt.close()
        RootFile.Write()
        RootFile.Close()
        
    



def CondorAdd(Region="WWW SR",SetName="",SetNumber="",Folder="",Plots={},doQCDnCF=[True,True],MakeEffPlots=True,MakeTables=True,Verbose=True,Blind=False, EventPrint=False,Mode="2l2j",count=0,Variations=["Nominal"]):

    Analysis_dir = os.getcwd()
    SaveFolder = os.getcwd()+"/Root/"

    if not os.path.exists("submit_c"):
        f=open("submit_c","a")
        f.write("Executable = "+Analysis_dir+"/condor_s.sh \n"+"""
Universe = vanilla
Notification = Error\n"""
                )
    else:
        f=open("submit_c","a")
        
    out_t="should_transfer_files = NO\n"
    out_t+="Output ="+Analysis_dir+"/condor_logs/temp.o\n"
    out_t+="Error ="+Analysis_dir+"/condor_logs/temp.e\n"
    out_t+="Log ="+Analysis_dir+"/condor_logs/temp.l\n"
    out_t+="Arguments = \"'"+Region+"' '"+SetName+"' '"+str(SetNumber)+"' '"+Folder+"' '"+SaveFolder+"' '{"
    for key,items in Plots.iteritems():
        out_t+="\"\""+str(key)+"\"\":{"
        for key2,items2 in items.iteritems():
            out_t+="\"\""+key2+"\"\":"
            if type(items2) is str:
                out_t+="\"\""+items2+"\"\","
            else:
                out_t+=str(items2)+","
        out_t+="},"

    SystVar="["
    for key in Variations:
        SystVar+="\"\""+key+"\"\","
    SystVar+="]"
    
    out_t+="}' '"+str(doQCDnCF[0])+"' '"+str(doQCDnCF[1])+"' '"+str(MakeEffPlots)+"' '"+str(MakeTables)+"' '"+str(Verbose)+"' '"+str(Blind)+"' '"+str(EventPrint)+"' '"+str(Mode)+"' '"+str(SystVar)+"'\" \nQueue\n\n"
    
 
    out_t=out_t.replace("temp.",str(count)+".")
    count+=1    
    f.write(out_t)
    f.close()
    











def WWWAnalyzeCondor(Region="WWW SR",Plots={},doQCDnCF=[True,True],MakeEffPlots=True,MakeTables=True,Verbose=True,Blind=False, EventPrint=False, useCondor = True, Mode="2l2j", SystVariation=[]):


    if not "Nominal" in SystVariation:
        SystVariation.append("Nominal")

    print "We will run with variations", " ".join(SystVariation)


    NChan=4
    if Mode=="q0q1":
        NChan=2
    elif Mode=="q0q13l":
        NChan=2        

    #Mendetory Root Settings
    #ROOT.gROOT.Macro('$ROOTCOREDIR/scripts/load_packages.C')
    ROOT.gROOT.SetBatch()
    ROOT.gStyle.SetOptStat(0)
    ROOT.gROOT.ProcessLine( "gErrorIgnoreLevel = 2001;")

    #Opening Work Files
    Folders = DataSets.DataFolder

    #Setting up output files
    PrintFile = None
    if EventPrint:
        PrintFile = open("Print.txt","w")

    PlotFolder="Plots/"
    try:
        os.makedirs(PlotFolder)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    RootFolder="Root/"
    try:
        os.makedirs(RootFolder)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    try:
        os.makedirs("condor_logs")
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    #Setting up data deriven histograms
    ExtraHists=[]
    if doQCDnCF[0]:
        ExtraHists.append("Non Prompt")
    if doQCDnCF[1]:
        ExtraHists.append("Charge Flip")
    if doQCDnCF[1]:
        ExtraHists.append("V#gamma")


    #Running Jobs
    CondorCount=0
    if useCondor:
        os.system("rm -f submit_c")
        os.system("rm -f condor_logs/*")




    SaveFolder = os.getcwd()+"/Root/"

    DataSet = DataSets.ReturnDataSet(Mode)#DataSets.FullMCSet.items()
    #Looping over SetNames,SetNumbers and Variations to Setup the Condor file Or Just directly Run them.

    for SetName,SetNumbers in DataSet.items():
        for Folder in Folders:
            if Verbose:
                print "Working on the MC Set",SetName,Folder

            tmpSetNumbers = ExpandData(Folder,SetName,SetNumbers)
            



            for SetNumber in tmpSetNumbers:
                if useCondor:
                    CondorAdd(Region,SetName,SetNumber,Folder,Plots,doQCDnCF,MakeEffPlots,MakeTables,Verbose,Blind,EventPrint,Mode,CondorCount,SystVariation)
                    CondorCount+=1
                else:
                    RunSet(Region,SetName,SetNumber,Folder,SaveFolder,Plots,doQCDnCF,MakeEffPlots,MakeTables,Verbose,Blind,EventPrint,Mode,SystVariation)

    #If Runinng on Condor Submiting the condor jobs and waiting them till they finnish
    if useCondor and ReSubmit:
        #current_jobs=float(commands.getoutput("condor_q $USER | tail -n1 ").split()[0])
        print "Submiting Jobs to condor"
        os.system("condor_submit submit_c")
        time.sleep(10)
        while True:
            try:
                jobID=commands.getoutput("condor_q $USER | tail -n3 ").split()[0].split(".")[0]
                jobs=float(commands.getoutput("condor_q "+jobID+" | tail -n1 ").split()[0])
            except:
                continue
            else:
                break

        while jobs > 0:
            try:
                time.sleep(120)
                jobs=float(commands.getoutput("condor_q "+jobID+" | tail -n1 ").split()[0])
                print "Jobs Left",jobs
            except:
                continue 



    #Start reading the runs after the runs are complete
    OutFile = ROOT.TFile(RootFolder+Region.replace(" ","")+".root","recreate")

    PlotsDict=collections.OrderedDict()
    PlotsDictQCD=collections.OrderedDict()
    PlotsDictCF=collections.OrderedDict()
    PlotsDictNoBL=collections.OrderedDict()
    
    EventCountDict=collections.OrderedDict()
    EventRawDict=collections.OrderedDict()
    QCDEventCountDict=collections.OrderedDict()
    QCDEventRawDict=collections.OrderedDict()
    CFEventCountDict=collections.OrderedDict()
    CFEventRawDict=collections.OrderedDict()
    NoBLEventCountDict=collections.OrderedDict()
    NoBLEventRawDict=collections.OrderedDict()

    for Variation in SystVariation:
        
        print "Working in Variation",Variation
        OutFile.mkdir(Variation)
        OutFile.cd(Variation)
        # Directory = ROOT.TDirectory(Variation,Variation)
        # Directory.cd()

        hEff={} #Efficiency plots init
        hEffQCD={} #Efficiency plots init
        hEffCF={} #Efficiency plots init
        hEffNoBL={} #Efficiency plots init
        Histograms={} #Histogram Array init
        HistogramsQCD={} #Histogram Array init
        HistogramsCF={} #Histogram Array init
        HistogramsNoBL={} #Histogram Array init

        for plot in Plots.keys():
            Histograms[plot] = [collections.OrderedDict() for i in range(0,NChan+1)]
            if doQCDnCF[0]:
                HistogramsQCD[plot] = [collections.OrderedDict() for i in range(0,NChan+1)]
            if doQCDnCF[1]:
                HistogramsCF[plot] = [collections.OrderedDict() for i in range(0,NChan+1)]
            if doQCDnCF[1]:
                HistogramsNoBL[plot] = [collections.OrderedDict() for i in range(0,NChan+1)]

        for SetName in DataSet.keys()+ExtraHists:
            if MakeEffPlots:
                    hEff[SetName]=[]
                    hEffQCD[SetName]=[]
                    hEffCF[SetName]=[]
                    hEffNoBL[SetName]=[]
                    for chan in range(0,NChan+1):

                        strChan=str(chan)
                        if chan==NChan:
                            strChan="Inc"

                        hEff[SetName].append(ROOT.TH1F("cutflow_"+SetName.replace(" ","").replace("#","")+"_"+strChan,"cutflow "+SetName+strChan,40,0,40))
                        if doQCDnCF[0]:
                            hEffQCD[SetName].append(ROOT.TH1F("cutflow_"+SetName.replace(" ","").replace("#","")+"_QCD_"+strChan,"cutflow "+SetName+strChan,40,0,40))
                        if doQCDnCF[1]:
                            hEffCF[SetName].append(ROOT.TH1F("cutflow_"+SetName.replace(" ","").replace("#","")+"_CF_"+strChan,"cutflow "+SetName+strChan,40,0,40))
                        if doQCDnCF[1]:
                            hEffNoBL[SetName].append(ROOT.TH1F("cutflow_"+SetName.replace(" ","").replace("#","")+"_NoBL_"+strChan,"cutflow "+SetName+strChan,40,0,40))


            for key in Histograms.keys():
                for chan in range(0,NChan+1):

                    strChan=str(chan)
                    if chan==NChan:
                        strChan="Inc"








                    if "RelBins" in Plots[key].keys():
                        Histograms[key][chan][SetName] = ROOT.TH1F(key+"_"+SetName.replace(" ","").replace("#","")+"_"+strChan,key+" "+SetName.replace(" ","").replace("#","")+" "+strChan,len(Plots[key]["RelBins"])-1,array('d',Plots[key]["RelBins"]))
                        Histograms[key][chan][SetName].Sumw2()
                        if doQCDnCF[0]:
                            HistogramsQCD[key][chan][SetName] = ROOT.TH1F("QCD_"+key+"_"+SetName.replace(" ","").replace("#","")+"_"+strChan,"QCD_"+key+" "+SetName.replace(" ","").replace("#","")+" "+strChan,len(Plots[key]["RelBins"])-1,array('d',Plots[key]["RelBins"]))
                            HistogramsQCD[key][chan][SetName].Sumw2()
                        if doQCDnCF[1]:
                            HistogramsCF[key][chan][SetName] = ROOT.TH1F("CF_"+key+"_"+SetName.replace(" ","").replace("#","")+"_"+strChan,"CF_"+key+" "+SetName.replace(" ","").replace("#","")+" "+strChan,len(Plots[key]["RelBins"])-1,array('d',Plots[key]["RelBins"]))
                            HistogramsCF[key][chan][SetName].Sumw2()
                        if doQCDnCF[1]:
                            HistogramsNoBL[key][chan][SetName] = ROOT.TH1F("NoBL_"+key+"_"+SetName.replace(" ","").replace("#","")+"_"+strChan,"NoBL_"+key+" "+SetName.replace(" ","").replace("#","")+" "+strChan,len(Plots[key]["RelBins"])-1,array('d',Plots[key]["RelBins"]))
                            HistogramsNoBL[key][chan][SetName].Sumw2()



                    else:
                        Histograms[key][chan][SetName] = ROOT.TH1F(key+"_"+SetName.replace(" ","").replace("#","")+"_"+strChan,key+" "+SetName.replace(" ","").replace("#","")+" "+strChan,Plots[key]["NBins"],Plots[key]["xmin"],Plots[key]["xmax"])
                        Histograms[key][chan][SetName].Sumw2()
                        if doQCDnCF[0]:
                            HistogramsQCD[key][chan][SetName] = ROOT.TH1F("QCD_"+key+"_"+SetName.replace(" ","").replace("#","")+"_"+strChan,"QCD_"+key+" "+SetName.replace(" ","").replace("#","")+" "+strChan,Plots[key]["NBins"],Plots[key]["xmin"],Plots[key]["xmax"])
                            HistogramsQCD[key][chan][SetName].Sumw2()
                        if doQCDnCF[1]:
                            HistogramsCF[key][chan][SetName] = ROOT.TH1F("CF_"+key+"_"+SetName.replace(" ","").replace("#","")+"_"+strChan,"CF_"+key+" "+SetName.replace(" ","").replace("#","")+" "+strChan,Plots[key]["NBins"],Plots[key]["xmin"],Plots[key]["xmax"])
                            HistogramsCF[key][chan][SetName].Sumw2()
                        if doQCDnCF[1]:
                            HistogramsNoBL[key][chan][SetName] = ROOT.TH1F("NoBL_"+key+"_"+SetName.replace(" ","").replace("#","")+"_"+strChan,"NoBL_"+key+" "+SetName.replace(" ","").replace("#","")+" "+strChan,Plots[key]["NBins"],Plots[key]["xmin"],Plots[key]["xmax"])
                            HistogramsNoBL[key][chan][SetName].Sumw2()



        EventCount={}
        EventRaw={}

        QCDEventCount={}
        QCDEventRaw={}

        CFEventCount={}
        CFEventRaw={}

        NoBLEventCount={}
        NoBLEventRaw={}




        for SetName in DataSet.keys()+ExtraHists:
            EventCount[SetName]=[0.0 for i in range(0,NChan)];
            EventRaw[SetName]=[0.0 for i in range(0,NChan)];
            QCDEventCount[SetName]=[0.0 for i in range(0,NChan)];
            QCDEventRaw[SetName]=[0.0 for i in range(0,NChan)];
            CFEventCount[SetName]=[0.0 for i in range(0,NChan)];
            CFEventRaw[SetName]=[0.0 for i in range(0,NChan)];
            NoBLEventCount[SetName]=[0.0 for i in range(0,NChan)];
            NoBLEventRaw[SetName]=[0.0 for i in range(0,NChan)];


        for SetName,SetNumbers in DataSet.items():

            if not Variation=="Nominal" and (not Variation in ddVariations) and (("Charge Flip" in str(SetName)) or ("Vgamma" in str(SetName)) or ("Non Prompt" in str(SetName)) or ("data" in str(SetName).lower())) : continue




            for Folder in Folders:

                if ("data17" in SetName.lower() and "submitDir-A" in Folder): continue
                if ("data18" in SetName.lower() and "submitDir-A" in Folder): continue
                if ("data15" in SetName.lower() and "submitDir-D" in Folder): continue
                if ("data16" in SetName.lower() and "submitDir-D" in Folder): continue
                if ("data18" in SetName.lower() and "submitDir-D" in Folder): continue
                if ("data15" in SetName.lower() and "submitDir-E" in Folder): continue
                if ("data16" in SetName.lower() and "submitDir-E" in Folder): continue
                if ("data17" in SetName.lower() and "submitDir-E" in Folder): continue
                

                tmpSetNumbers = ExpandData(Folder,SetName,SetNumbers)
    
                for SetNumber in tmpSetNumbers:

                    sFile = "Root/"+Folder.split("/")[Folder.split("/").index("data-MVATree")-1]+"/"+Region.replace(" ","")+"/"+str(SetName).replace(" ","").replace("#","")+"/"+str(SetNumber)+str(Variation)+".root"
                    try:
                        tmpRoot = None
                        if len(Histograms.keys())>0:
                            tmpRoot = ROOT.TFile(sFile,"open")
                    except:
                        if Verbose:
                            print "Cannot find the file", sFile
                        continue



                    txtFile = "Root/"+Folder.split("/")[Folder.split("/").index("data-MVATree")-1]+"/"+Region.replace(" ","")+"/"+str(SetName).replace(" ","").replace("#","")+"/"+str(SetNumber)+str(Variation)+".info" 
                    try:
                        with open(txtFile) as f:
                            for line in f:
                                Val=[0.0 for i in range(0,NChan)]
                                tmpSplit = line.replace("\n","").split(",")
                                (key, Name) = tmpSplit[0:2]
                                Val = tmpSplit[2:]
                                if "QCDEventCount" in key:
                                    for chan in range(0,NChan):
                                        QCDEventCount[Name.strip()][chan]+=float(Val[chan])
                                elif "QCDEventRaw" in key:
                                    for chan in range(0,NChan):
                                        QCDEventRaw[Name.strip()][chan]+=float(Val[chan])
                                elif "CFEventCount" in key:
                                    for chan in range(0,NChan):
                                        CFEventCount[Name.strip()][chan]+=float(Val[chan])
                                elif "CFEventRaw" in key:
                                    for chan in range(0,NChan):
                                        CFEventRaw[Name.strip()][chan]+=float(Val[chan])
                                elif "NoBLEventCount" in key:
                                    for chan in range(0,NChan):
                                        NoBLEventCount[Name.strip()][chan]+=float(Val[chan])
                                elif "NoBLEventRaw" in key:
                                    for chan in range(0,NChan):
                                        NoBLEventRaw[Name.strip()][chan]+=float(Val[chan])
                                elif "EventCount" in key:
                                    for chan in range(0,NChan):
                                        EventCount[Name.strip()][chan]+=float(Val[chan])
                                elif "EventRaw" in key:
                                    for chan in range(0,NChan):
                                        EventRaw[Name.strip()][chan]+=float(Val[chan])
                            f.close()
                    except:
                        if Verbose:
                            print "Cannot read the file",txtFile


                    if not "TFile" in str(type(tmpRoot)) and len(Histograms.keys())>0:
                        if Verbose:
                            print "Cannot find the file", sFile
                        continue


                    for chan in range(0,NChan+1):
                        strChan=str(chan)
                        if chan==NChan:
                            strChan="Inc"

                        for key in Histograms.keys():
                            try:

                                #print tmpRoot.Get(key+"_"+SetName.replace(" ","").replace("#","")+"_"+strChan)
                                Histograms[key][chan][SetName].Add(tmpRoot.Get(key+"_"+SetName.replace(" ","").replace("#","")+"_"+strChan))
                                if not Systs:
                                    HistogramsQCD[key][chan][SetName].Add(tmpRoot.Get("QCD_"+key+"_"+SetName.replace(" ","").replace("#","")+"_"+strChan))
                                    HistogramsCF[key][chan][SetName].Add(tmpRoot.Get("CF_"+key+"_"+SetName.replace(" ","").replace("#","")+"_"+strChan))
                                    HistogramsNoBL[key][chan][SetName].Add(tmpRoot.Get("NoBL_"+key+"_"+SetName.replace(" ","").replace("#","")+"_"+strChan))
                                if doQCDnCF[0]:
                                    Histograms[key][chan]["Non Prompt"].Add(tmpRoot.Get(key+"_NonPrompt_"+strChan),1)
                                    if not Systs:
                                        HistogramsQCD[key][chan]["Non Prompt"].Add(tmpRoot.Get("QCD_"+key+"_NonPrompt_"+strChan),1)
                                        HistogramsCF[key][chan]["Non Prompt"].Add(tmpRoot.Get("CF_"+key+"_NonPrompt_"+strChan),1)
                                if doQCDnCF[1]:
                                    Histograms[key][chan]["Charge Flip"].Add(tmpRoot.Get(key+"_ChargeFlip_"+strChan),1)
                                    if not Systs:
                                        HistogramsQCD[key][chan]["Charge Flip"].Add(tmpRoot.Get("QCD_"+key+"_ChargeFlip_"+strChan),1)
                                        HistogramsCF[key][chan]["Charge Flip"].Add(tmpRoot.Get("CF_"+key+"_ChargeFlip_"+strChan),1)                        
                                if doQCDnCF[1]:
                                    Histograms[key][chan]["V#gamma"].Add(tmpRoot.Get(key+"_Vgamma_"+strChan),1)
                                    if not Systs:
                                        HistogramsQCD[key][chan]["V#gamma"].Add(tmpRoot.Get("QCD_"+key+"_Vgamma_"+strChan),1)
                                        HistogramsCF[key][chan]["V#gamma"].Add(tmpRoot.Get("CF_"+key+"_Vgamma_"+strChan),1)                        


                            except:
                                if Verbose:
                                    print "Problem fetching the histograms for",Folder,SetName,SetNumber,key,chan
                                    print key+"_"+SetName.replace(" ","").replace("#","")+"_"+strChan

                        if MakeEffPlots:
                            try:
                                hEff[SetName][chan].Add(tmpRoot.Get("cutflow_"+SetName.replace(" ","").replace("#","")+"_"+strChan))        
                                if not Systs:
                                    hEffQCD[SetName][chan].Add(tmpRoot.Get("cutflow_"+SetName.replace(" ","").replace("#","")+"_QCD_"+strChan))        
                                    hEffCF[SetName][chan].Add(tmpRoot.Get("cutflow_"+SetName.replace(" ","").replace("#","")+"_CF_"+strChan))        
                                    hEffNoBL[SetName][chan].Add(tmpRoot.Get("cutflow_"+SetName.replace(" ","").replace("#","")+"_NoBL_"+strChan))        
                            except:
                                if Verbose:
                                    print "Problem fetching the eff histograms for",Folder,SetName,SetNumber,strChan
                    if len(Histograms.keys())>0:
                        tmpRoot.Close()
                        

        #Saving Histograms
        OutFile.cd(Variation)
        for key in Histograms.keys():
            for chan in range(0,NChan+1):
                for SetName in DataSet.keys()+ExtraHists:
                    Histograms[key][chan][SetName].Write() 
                    if doQCDnCF[0]:
                      HistogramsQCD[key][chan][SetName].Write() 
                    if doQCDnCF[1]:
                      HistogramsCF[key][chan][SetName].Write() 
                      HistogramsNoBL[key][chan][SetName].Write() 

                   
                    if not Variation is "Nominal":
                        Histograms[key][chan][SetName].Delete()
                        if doQCDnCF[0]:
                          HistogramsQCD[key][chan][SetName].Delete() 
                        if doQCDnCF[1]:
                          HistogramsCF[key][chan][SetName].Delete() 
                          HistogramsNoBL[key][chan][SetName].Delete() 
                        

        if MakeEffPlots:
            for chan in range(0,NChan+1):
                for SetName in DataSet.keys()+ExtraHists:
                    hEff[SetName][chan].Write()
                    if doQCDnCF[0]:
                      hEffQCD[SetName][chan].Write()
                    if doQCDnCF[1]:
                      hEffCF[SetName][chan].Write()
                      hEffNoBL[SetName][chan].Write()

                    if not Variation is "Nominal":
                        hEff[SetName][chan].Delete()
                        if doQCDnCF[0]:
                          hEffQCD[SetName][chan].Delete()
                        if doQCDnCF[1]:
                          hEffCF[SetName][chan].Delete()
                          hEffNoBL[SetName][chan].Delete()


        if Variation is "Nominal":
            PlotsDict[Variation]=Histograms
            PlotsDictQCD[Variation]=HistogramsQCD
            PlotsDictCF[Variation]=HistogramsCF
            PlotsDictNoBL[Variation]=HistogramsNoBL

        

        EventCountDict[Variation]=EventCount
        EventRawDict[Variation]=EventRaw
        QCDEventCountDict[Variation]=QCDEventCount
        QCDEventRawDict[Variation]=QCDEventRaw
        CFEventCountDict[Variation]=CFEventCount
        CFEventRawDict[Variation]=CFEventRaw
        NoBLEventCountDict[Variation]=NoBLEventCount
        NoBLEventRawDict[Variation]=NoBLEventRaw

        






    OveralSyst = [0.0 for i in range(0,NChan)]
    #Prints the Final Numbers
    #Makes Latex Tables and Print Output


    print
    print "Yield numbers for region:",Region

    if MakeTables:
        OveralSyst = MakeTable(EventCountDict,EventRawDict,"Tables/",Region,Blind,Mode)

    if MakeTables:
        print "QCD"
        MakeTable(QCDEventCountDict,QCDEventRawDict,"Tables/",Region,False,Mode,Tag="QCD_")


    if MakeTables:
        print "CF"
        MakeTable(CFEventCountDict,CFEventRawDict,"Tables/",Region,False, Mode,Tag="CF_")


    if MakeTables:
        print "NoBL" 
        MakeTable(NoBLEventCountDict,NoBLEventRawDict,"Tables/",Region,False, Mode,Tag="NoBL_")

    

    TotalSyst=0.
    for ItemSyst in OveralSyst:
        TotalSyst=ItemSyst*ItemSyst
    OveralSyst.append(sqrt(TotalSyst))

    print "Overall Systematics",OveralSyst

    #Make Plots 
    tmpFolder=""
    tmpFilderQCD=""
    if len(PlotsDict["Nominal"].keys())>0:
        tmpFolder = PlotFolder+Region.replace(" ","")+"/"
        try:
            os.makedirs(tmpFolder)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

        if doQCDnCF[0]:
            tmpFolderQCD = PlotFolder+Region.replace(" ","")+"_QCD/"
            try:
                os.makedirs(tmpFolderQCD)
            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise


        if doQCDnCF[1]:
            tmpFolderCF = PlotFolder+Region.replace(" ","")+"_CF/"
            try:
                os.makedirs(tmpFolderCF)
            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise


        if doQCDnCF[1]:
            tmpFolderNoBL = PlotFolder+Region.replace(" ","")+"_NoBL/"
            try:
                os.makedirs(tmpFolderNoBL)
            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise


                
    OutFile.cd("Nominal")
    #Save Canvases per channel
    for key in PlotsDict["Nominal"].keys():
        for chan in range(0,NChan+1):
            #Select histograms to use
            Histos=collections.OrderedDict()
            for Hist in DataSets.HistogramOrder:
                if Hist in  DataSets.QCDOnly: continue
                if not Hist in PlotsDict["Nominal"][key][chan].keys(): continue
                Histos[Hist]=PlotsDict["Nominal"][key][chan][Hist]
            if not Systs:
                if doQCDnCF[0]:
                    HistosQCD=PlotsDictQCD["Nominal"][key][chan]
                if doQCDnCF[1]:
                    HistosCF=PlotsDictCF["Nominal"][key][chan]
                if doQCDnCF[1]:
                    HistosNoBL=PlotsDictNoBL["Nominal"][key][chan]

            #Setting up Canvas Settings
            #YScale
            YScale = Plots[key]["YScale"] if "YScale" in Plots[key].keys() else 1.8
            #XAxis Name (Exp:  M_{jj} [GeV])
            XAxis= Plots[key]["XAxis"] if "XAxis" in Plots[key].keys() else ""
            # Systematic Error (Percentage)
            Syst_Error=0
            #Extra Text in Legend

            tChan= ""
            labels=["ee","e#mu","#mue","#mu#mu"]

            if Mode=="q0q1":
                labels=["q0q1=-1","q0q1=1"]
            elif Mode=="q0q1Split":
                labels=["ee#mu q0q1=-1","ee#mu q0q1=1","#mu#mue q0q1=-1","#mu#mue q0q1=1"]
            elif Mode=="3l":
                labels=["eee","ee#mu","#mu#mue","#mu#mu#mu"]
            elif Mode=="q0q13l":
                labels=["ee#mu","#mu#mue"]
            elif Mode=="2l2j":
                labels=["ee","e#mu","#mue","#mu#mu"]
            elif Mode=="Non Prompt":
                labels=["ee#mu","ee#mu","#mu#mue","#mu#mue"]


            labels.append("Inclusive")

            tChan = labels[chan]

            strChan=str(chan)
            if chan==NChan:
                strChan="Inc"

            ExtraText = [Region+" "+tChan]
            #Make Y axis in Log Scale
            Logy= Plots[key]["Logy"] if "Logy" in Plots[key].keys() else False
            #Set custom XRange
            XRange=Plots[key]["XRange"] if "XRange" in Plots[key].keys() else []
            # Changes the way ticks are done (Use with NonGeV)
            Multip=Plots[key]["Multip"] if "Multip" in Plots[key].keys() else False
            # Changes Y Title to Entries instead of Entries/GeV (For Lepton Number etc)
            NonGeV=Plots[key]["NonGeV"] if "NonGeV" in Plots[key].keys() else False
            # Overlaid plot like signal, used for aQGC's
            OverlayPlot=[]
            OverlayName=[]
            # Plot Overflow
            OverFlow=Plots[key]["OverFlow"] if "OverFlow" in Plots[key].keys() else True

            Canvas = WWWPlot(Histos,YScale,XAxis,OveralSyst[chan],ExtraText,Logy,XRange,Blind,Multip,NonGeV,OverlayPlot,OverlayName,OverFlow)
            Canvas.Write(key.replace(" ","").replace("#","")+strChan)
            Canvas.SaveAs(tmpFolder+key.replace(" ","").replace("#","")+strChan+".png")
            Canvas.SaveAs(tmpFolder+key.replace(" ","").replace("#","")+strChan+".pdf")
            Canvas.Close()
            del Canvas

            if not Systs:
                if doQCDnCF[0]:
                    Canvas = WWWPlot(HistosQCD,YScale,XAxis,Syst_Error,ExtraText,Logy,XRange,False,Multip,NonGeV,OverlayPlot,OverlayName,OverFlow)
                    Canvas.Write("QCD_"+key.replace(" ","").replace("#","")+strChan)
                    Canvas.SaveAs(tmpFolderQCD+key.replace(" ","").replace("#","")+strChan+".png")
                    Canvas.SaveAs(tmpFolderQCD+key.replace(" ","").replace("#","")+strChan+".pdf")
                    Canvas.Close()
                    del Canvas

                if doQCDnCF[1]:
                    Canvas = WWWPlot(HistosCF,YScale,XAxis,Syst_Error,ExtraText,Logy,XRange,False,Multip,NonGeV,OverlayPlot,OverlayName,OverFlow)
                    Canvas.Write("CF_"+key.replace(" ","").replace("#","")+strChan)
                    Canvas.SaveAs(tmpFolderCF+key.replace(" ","").replace("#","")+strChan+".png")
                    Canvas.SaveAs(tmpFolderCF+key.replace(" ","").replace("#","")+strChan+".pdf")
                    Canvas.Close()
                    del Canvas

                if doQCDnCF[1]:
                    Canvas = WWWPlot(HistosNoBL,YScale,XAxis,Syst_Error,ExtraText,Logy,XRange,False,Multip,NonGeV,OverlayPlot,OverlayName,OverFlow)
                    Canvas.Write("NoBL_"+key.replace(" ","").replace("#","")+strChan)
                    Canvas.SaveAs(tmpFolderNoBL+key.replace(" ","").replace("#","")+strChan+".png")
                    Canvas.SaveAs(tmpFolderNoBL+key.replace(" ","").replace("#","")+strChan+".pdf")
                    Canvas.Close()
                    del Canvas





    OutFile.Close()



if __name__ == "__main__":


    MakeEffPlots=True
    MakeTables=True
    Verbose=False
    Blind=False
    doQCDnCF=[True,True]
    Plots={}
    Plots["Mjj"]={"NBins":20,"xmin":20,"xmax":320,"Correct":1e-3,"XAxis":"M_{jj}[GeV]"}
    Condor=True
    Variations = ["Nominal"]#,"QCDBinning","Btag77","QCDStat"]
    #RunSet("Side Band CR","WWW",364243,Folder,Plots,doQCDnCF,MakeEffPlots,MakeTables,Verbose,Blind)
    WWWAnalyzeCondor("Side Band CR",Plots,doQCDnCF,MakeEffPlots,MakeTables,Verbose,Blind,False,Condor,False,Variations)
