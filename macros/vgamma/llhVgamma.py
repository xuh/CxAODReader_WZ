
'''
 TUAN M. NGUYEN (PhD Student; Supervisor: Jean-Francois Arguin)
 Universite de Montreal
 tuan.nguyen.manh@cern.ch
 
 Electron charge flip
'''



#######################################################
# Minuit
#######################################################
def logLlhPair(parameters):
  nE0SFOS=dIIIe0SFOS# - nIII0SFOS
  nM0SFOS=dIIIm0SFOS# - nIII0SFOS



  VgammaE,VgammaM = parameters[0],parameters[1]

  expE0SFOS =  VgammaE*(dIIAeT0SFOS-nIIAeT0SFOS) + nIIIe0SFOS #+ FzM * (dIIAmZ0SFOS - nIIAmZ0SFOS)
  expM0SFOS = VgammaM*(dIIAmT0SFOS-nIIAmT0SFOS) + nIIIm0SFOS #+ FzE * (dIIAeZ0SFOS - nIIAeZ0SFOS) +
  

  try:
    logLlh =0
    logLlh += nE0SFOS * log(expE0SFOS) - expE0SFOS - lgamma(nE0SFOS) 
    logLlh += nM0SFOS * log(expM0SFOS) - expM0SFOS - lgamma(nM0SFOS) 




  except ValueError:
    logLlh = 0.0
  return logLlh

def negativeLog(n,parameters):
  
  negLog = -logLlhPair(parameters)
  return negLog

# Every parameter of fcn is an array
def fcn(numParameters, derivatives, function, parameters, internalFlag):
  function[0] = negativeLog(numParameters, parameters)

def ratesAndErrors(numParameters, minuit):
  rates = np.zeros(numParameters)
  rateErrors = np.zeros(numParameters)
  rate, rateError = Double(0), Double(0)
  for i in xrange(numParameters):
    minuit.GetParameter(i, rate, rateError)
    rates[i] = float(rate)
    rateErrors[i] = float(rateError)
  return rates, rateErrors

def runMinuit(numParameters):
  minuit = TMinuit(numParameters)
  minuit.SetPrintLevel(0) 
  
  minuit.SetFCN(fcn)
  arglist = np.zeros(numParameters) + 0.01
  print numParameters
  print arglist
  internalFlag, arglist[0] = Long(0), 0.5
  minuit.mnexcm("SET ERR", arglist, 1, internalFlag)
  
  initialValues = np.zeros(numParameters) + 0.001
  steps = np.zeros(numParameters) + 0.0001
  
  for i in xrange(numParameters):
    name = "epsilon%s" % i
    minuit.mnparm(i, name, initialValues[i], steps[i], 0, 1, internalFlag)
  
  # arglist[0] = 2
  # minuit.mnexcm("SET STR", arglist, 1, internalFlag)
  
  #arglist[0], arglist[1] = 10000, 0.1
  minuit.mnexcm("SIMPLEX", arglist, 1, internalFlag)
  minuit.mnexcm("MIGRAD", arglist, 1, internalFlag)
  
  print "FIT STATUS is " +str(minuit.GetStatus())
  return ratesAndErrors(numParameters, minuit)


#######################################################
# Run
#######################################################
from ROOT import TMinuit, Long, Double

from math import log, lgamma
import numpy as np

import ROOT
#import rutil
import time

start_time = time.time()
##############################

#etas = [0.0,2.5]
#pts  = [20.,40.,80.,120,1000.]
#pts  = [20.,40.,70.,1000]#,1000.]
#pts  = [20.,40.,70.,1000]#,1000.]
#pts  = [20.,2000.]#,80.,2000]#,1000.]
#pts  = [20.,40.,80.,2000]#,1000.]
#pts  = [20.,40.,2000]#,1000.]
#pts  = [20.,2000]#,1000.]
#pts  = [20.,40.,100.,2000]#,1000.]


##############################
numParameters = 2
##############################

def doIt(name, outFile):
  rates,errors = runMinuit(numParameters)
  
  # print nIII0SFOS, nIII1SFOSHigh,nIII1SFOSLow,nIIIZW
  # print dIII0SFOS, dIII1SFOSHigh,dIII1SFOSLow,dIIIZW
  # print nIIAT0SFOS, nIIAZ1SFOSHigh,nIIAT1SFOSHigh, nIIAZ1SFOSLow,nIIAT1SFOSLow,nIIAZZW,nIIATZW
  # print dIIAT0SFOS, dIIAZ1SFOSHigh,dIIAT1SFOSHigh, dIIAZ1SFOSLow,dIIAT1SFOSLow,dIIAZZW,dIIATZW


  
  ratesN, errorsN = '%s_rates.txt' % name, '%s_errors.txt' % name ##### Can also be without ".txt" at the not to save as txt
  with open(ratesN, 'wb') as ratesF,open(errorsN, 'wb') as errorsF:
    np.savetxt(ratesF, rates)   ##### if no ".txt" is given above, here use just save()
    np.savetxt(errorsF, errors) ##### if no ".txt" is given above, here use just save()
    with open(outFile, 'a') as txtOut:
      txtOut.write('%s\n%s\n' % (ratesN, errorsN))
  #reLabels = rutil.ratesErrorsBins(rates, errors, etas, pts)
  #rutil.printRatesErrorsBins(name, reLabels)
  
##### This part was added for TH2 plotting
  #bName = rutil.beautifyName(name)
  #rutil.writeToFile('%s.root' % name, rutil.hist2D(rates, errors, etas, pts, '%s_misid' % name))
#####

  
##############################
# This stuff below cannot be put into a function, 
# because Minuit needs to see global variables n and nss!!!

if __name__ == '__main__':
  import sys 

  nIIIe0SFOS = np.zeros(1)
  nIIAeT0SFOS = np.zeros(1)
  nIIAeZ0SFOS = np.zeros(1)

  nIIIm0SFOS = np.zeros(1)
  nIIAmZ0SFOS = np.zeros(1)
  nIIAmT0SFOS = np.zeros(1)

  nIIIe1SFOSLow = np.zeros(1)
  nIIIm1SFOSLow = np.zeros(1)

  nIIAmZ1SFOSLow = np.zeros(1)
  nIIAeZ1SFOSLow = np.zeros(1)
  nIIAeT1SFOSLow = np.zeros(1)
  nIIAmT1SFOSLow = np.zeros(1)

  nIIIe1SFOSHigh = np.zeros(1)
  nIIIm1SFOSHigh = np.zeros(1)

  nIIAmZ1SFOSHigh = np.zeros(1)
  nIIAeZ1SFOSHigh = np.zeros(1)
  nIIAeT1SFOSHigh = np.zeros(1)
  nIIAmT1SFOSHigh = np.zeros(1)



  nIIIeZW = np.zeros(1)
  nIIImZW = np.zeros(1)

  nIIAmZZW = np.zeros(1)
  nIIAeZZW = np.zeros(1)
  nIIAeTZW = np.zeros(1)
  nIIAmTZW = np.zeros(1)

 ###
  dIIIe0SFOS = np.zeros(1)
  dIIAeT0SFOS = np.zeros(1)
  dIIAeZ0SFOS = np.zeros(1)

  dIIIm0SFOS = np.zeros(1)
  dIIAmT0SFOS = np.zeros(1)
  dIIAmZ0SFOS = np.zeros(1)

  dIIIe1SFOSLow = np.zeros(1)
  dIIIm1SFOSLow = np.zeros(1)

  dIIAmZ1SFOSLow = np.zeros(1)
  dIIAeZ1SFOSLow = np.zeros(1)
  dIIAeT1SFOSLow = np.zeros(1)
  dIIAmT1SFOSLow = np.zeros(1)


  dIIIe1SFOSHigh = np.zeros(1)
  dIIIm1SFOSHigh = np.zeros(1)

  dIIAmZ1SFOSHigh = np.zeros(1)
  dIIAeZ1SFOSHigh = np.zeros(1)
  dIIAeT1SFOSHigh = np.zeros(1)
  dIIAmT1SFOSHigh = np.zeros(1)


  dIIIeZW = np.zeros(1)
  dIIImZW = np.zeros(1)

  dIIAmZZW = np.zeros(1)
  dIIAeZZW = np.zeros(1)
  dIIAeTZW = np.zeros(1)
  dIIAmTZW = np.zeros(1)




  f0SFOS = ROOT.TFile("WZVgammaCR.root","open")

  mcSets=["ttZ","WWW","tZ","ssWW","VVV","ttW","ZZ","WZ","NonPrompt","Data"]
  #mcSets=["ttZ","WWW","tZ","ssWW","VVV","ttW","ZZ","SingleTop","WZ","Fakes","Data"]
  #mcSets=["ttZ","WWW","tZ","ssWW","VVV","ttW","ZZ","SingleTop","WZ","Data"]
  
  for SET in mcSets:

    if not "data" in SET.lower():
      print SET
      plot = "Mjj"
      #plot = "Mlll"

      nBin = f0SFOS.Get("Nominal/"+plot+"_"+SET+"_0").GetNbinsX()


      nIIIe0SFOS += f0SFOS.Get("Nominal/"+plot+"_"+SET+"_0").Integral(0,nBin+1) 
      nIIIm0SFOS += f0SFOS.Get("Nominal/"+plot+"_"+SET+"_2").Integral(0,nBin+1) 

      nIIAeT0SFOS += f0SFOS.Get("Nominal/NoBL_"+plot+"_"+SET+"_0").Integral(0,nBin+1) 
      nIIAmT0SFOS += f0SFOS.Get("Nominal/NoBL_"+plot+"_"+SET+"_2").Integral(0,nBin+1) 



    else:
      dIIIe0SFOS += f0SFOS.Get("Nominal/"+plot+"_"+SET+"_0").Integral(0,nBin+1) 
      dIIIm0SFOS += f0SFOS.Get("Nominal/"+plot+"_"+SET+"_2").Integral(0,nBin+1) 

      dIIAeT0SFOS += f0SFOS.Get("Nominal/NoBL_"+plot+"_"+SET+"_0").Integral(0,nBin+1) 
      dIIAmT0SFOS += f0SFOS.Get("Nominal/NoBL_"+plot+"_"+SET+"_2").Integral(0,nBin+1) 
      





  doIt("Fakes", "Fakes.txt")






print("--- %s TIME ---" % (time.time() - start_time))





#  LocalWords:  SFOS
