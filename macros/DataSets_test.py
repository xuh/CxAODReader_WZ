import os
import collections

MCONLY = False
#MCONLY = True
DataFolder=["/atlas/data19/xuwenhao/WWWFramework_master_VHbb_1/run/submitDir-A-noambi/data-MVATree/", "/atlas/data19/xuwenhao/WWWFramework_master_VHbb_1/run/submitDir-D-noambi/data-MVATree/", "/atlas/data19/xuwenhao/WWWFramework_master_VHbb_1/run/submitDir-E-noambi/data-MVATree/"]

# list of dataset
# WH
WH2l2j=[341422,341424,341428,341430,341432,341436]
WH3l=[341421,341423,341427,341429,341431,341435]
WH = WH2l2j + WH3l
# WWW
WWW2l2j=range(364336,364340)+ WH2l2j
WWW3l=[364242] + WH3l


# diboson
WZ = [364253, 364284]
WZtest = [364253] 
ZZ = [364250, 364283] # + [361073]
ssWW = [364286, 366088]
TriBoson = [364243, 364249]  #range(364243,364250)
# top
ttZ = [410218,410219,410220]
ttW = [410155]
tZ  = [410560]


##############################
### MC for Data Driven Bkg ###
##############################

##############
### Vgamma ###
##############
Wgamma = range(364521,364536)  # ?? + range(363514,363520)

#Zgamma = range(364500,364515)  # ?? + range(363510,363513) 
ZgammaLO = range(366140,366155)
Zgamma = ZgammaLO

##################
### Non Prompt ###
##################
ttbar = [410470]

Wt = [410646,410647]
STop = [410644,410645,410658,410659] + Wt

Wjets=range(364156,364198)
WjetsCVetoBVeto = [364156,364159,364162,364165,364170,364173,364176,364179,364184,364187,364190,364193]
WjetsCFilterBVeto = [364157,364160,364163,364166,364171,364174,364177,364180,364185,364188,364191,364194]
WjetsBFilter =[364158,364161,364164,364167,364172,364175,364178,364181,364186,364189,364192,364195]

###################
### Charge Flip ###
###################
Zjets =  range(364100,364142)
ZjetsCVetoBVeto = [364100,364103,364106,364109,364114,364117,364120,364123,364128,364131,364134,364137]
ZjetsCFilterBVeto=[364101,364104,364107,364110,364115,364118,364121,364124,364129,364132,364135,364138]
ZjetsBFilter=[364102,364105,364108,364111,364116,364119,364122,364125,364130,364133,364136,364139]




############################################
WWW2l2j = range(364336,364340)
WH2l2j = [341422,341424,341428,341430,341432,341436]
WWW2l2j = WH2l2j + WWW2l2j

WH3l=[341421,341423,341427,341429,341431,341435]
WWW3l=[364242] + WH3l


ZZ= [364283, 364250] 
#ZZtest = [361073, 363356] 

WZ= [364253, 364284]  
#WZtest = [363358, 363489]

WW = [364286, 366088] # same-sign
# WWtest = [364254, 361077, 364285] + [363359 , 363360]
# OSWW? + nonprompt

Wgamma= range(364521,364536)
# Wgammatest = range(363514,363520) ?

Zgamma= range(364500, 364515)
#ZgammaLO = range(366140,366155)
# Zgammatest = range(363510,363513) ?

TriBoson = [364243, 364249]
# TriBosontest = [ 364244, 364246, 364248]

ttW = [410155]

ttZ = [410218,410219,410220]

tZ  = [410560]

Wt = [410646,410647]

ttbar = [410470]

STop = [410644,410645,410658,410659]

Zjets =  range(364100,364142)
Wjets=range(364156,364198)

############################################
# 2leptons channel
# Nominal Samples (in Truth Level): 
# have two same-sign leptons
# has three or more leptons


# charge flip samples
# have two os leptons



# photon conversion samples



# non-prompt samples 
#has 1 lepton 1 jet samples




# 3leptons channel
# nominal samples
# have 0SFOS three leptons
# have four leptons


# chargeflip samples 
# 1SFOS(ee) leptons



# photon conversion samples
# 2leptons + gamma

# non-prompt samples:
# 2 leptons + jet



#This is the MC set used by the Analysis Tool

def ReturnDataSet(Mode="2l2j"):

    FullMCSet = collections.OrderedDict()

    FullMCSet["Data"] = ['364337']
    FullMCSet["WZ"] = WZtest
    
    # FullMCSet["Data"] = ["data15","data16","data17","data18"]
    # FullMCSet["WZ"]=WZ
    # FullMCSet["ZZ"]=ZZ
    # FullMCSet["ttW"]=ttW
    # FullMCSet["ttZ"]=ttZ
    # FullMCSet["tZ"]=tZ
    # FullMCSet["VVV"]=TriBoson
    # FullMCSet["ssWW"]=ssWW
    
    if "q0q1" in Mode:

        FullMCSet["WWW"]=WWW3l

    else:
        #FullMCSet["SingleTop"]=STop
        FullMCSet["WWW"]=WWW2l2j+WH3l

    if MCONLY:
      #FullMCSet["Data"] = ssWW
      FullMCSet["V#gamma"] = Wgamma + Zgamma

      FullMCSet["Zjets"] = Zjets

      FullMCSet["ttbar"] = ttbar
      FullMCSet["SingleTop"] = STop
      FullMCSet["Wjets"] = Wjets


    return FullMCSet


HistogramOrder=["Data","WZ","Zjets","Wjets","WjetsCVetoBVeto","WjetsCFilterBVeto","WjetsBFilter","ZjetsCVetoBVeto","ZjetsCFilterBVeto","ZjetsBFilter","Non Prompt","Charge Flip","Z#gamma","W#gamma","V#gamma","ttbar","ttV","ttW","ttZ","tZ","ZZ","VVV","SingleTop","Others","ssWW","WH","WWW","WWW3l","WWW2l2j"]

#HistogramOrder=["Data", "WZ", "Non Prompt","Charge Flip", "V#gamma","ttW","ttZ","tZ","ZZ","VVV","SingleTop","ssWW","WWW"]


QCDOnly = collections.OrderedDict()
