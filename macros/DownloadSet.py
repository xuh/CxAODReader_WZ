import os
import sys
import ROOT
import subprocess


def GetPBook():
    subprocess.call(["pbook","-c","sync()"])
    proc = subprocess.Popen(["pbook","-c","show()"],stdout=subprocess.PIPE)
    tmp = proc.stdout.read()

    
    pBookDict={}

    name=""
    status=""
    for line in tmp.split('\n'):
        if "params" in line:
            for arg in line.split(" "):
                if "--outDS=" in arg:
                    name = arg.replace("--outDS=","") 
                    break
                
        if "taskStatus" in line:
            status=line.replace("taskStatus : ","")
        if "======" in line:
            if name!="" and status!="":
                pBookDict[name]=status
            name=""
            status=""

    # for key,item in pBookDict.iteritems():
    #     print key,item
    
    return pBookDict


ROOT.gROOT.Macro('$ROOTCOREDIR/scripts/load_packages.C')

ROOTCORE = os.environ["ROOTCOREBIN"]
if len(sys.argv)!= 2:
    exit

WORKDIR=sys.argv[1]

print "Working on:",WORKDIR

OutputDir = WORKDIR+"/output-MVATree/"
#OutputDir = WORKDIR+"/output-hist/"
DownloadDir ="Download/"
FileList = os.listdir(OutputDir)

pBookDict = GetPBook()


subprocess.call(["mkdir","-p",DownloadDir+WORKDIR])

fFailed = open(DownloadDir+WORKDIR+'/Failed.list','w')
fFin = open(DownloadDir+WORKDIR+'/Finished.list','w')
fHFin = open(DownloadDir+WORKDIR+'/HalfFinished.list','w')
fnFound = open(DownloadDir+WORKDIR+'/NotFound.list','w')



for File in FileList:
    rFile = ROOT.TFile(OutputDir+File)
    sample = rFile.Get("sample")
    print "nc_grid:",sample.getMetaString("nc_grid")
    
    try:
               
        status = pBookDict[sample.getMetaString("nc_grid").replace("_MVATree.root/","")]
        if "done" in status or "finished" in status:
            print "Match Found"
            subprocess.call(["rucio","get","--dir="+DownloadDir+WORKDIR,sample.getMetaString("nc_grid")])
            fFin.write(sample.getMetaString("sample_name")+"\n")
            if "finished" in status:
                fHFin.write(sample.getMetaString("sample_name")+"\n")

        else:
            print "Run failed status is", status
            fFailed.write(sample.getMetaString("sample_name")+" "+status+"\n")
    except:
        print "Run cannot be found"
        fnFound.write(sample.getMetaString("sample_name")+"\n")
