from AnalyzeSet import WWWAnalyzeCondor
import DataSets

#Name the TTree branches you want to plot


Plots={}
Plots["Mjj"]={"NBins":30,"xmin":0,"xmax":300,"Correct":1e-3,"XAxis":"M_{jj}[GeV]"}
#Plots["MjjSS"]={"NBins":20,"xmin":300,"xmax":1000,"Correct":1e-3,"XAxis":"M_{jj}[GeV]"}
#Plots["Mjj"]={"NBins":300,"xmin":20,"xmax":320,"Correct":1e-3,"XAxis":"M_{jj}[GeV]"}
#Plots["Mjj"]={"RelBins":[300,400,500,700,1000,2000],"Correct":1e-3,"XAxis":"M_{jj}[GeV]"}

Plots["Nleptons"]={"NBins":4,"xmin":0,"xmax":4,"Correct":1,"XAxis":"Nleptons","Multip":True}
Plots["Njets"]={"NBins":8,"xmin":0,"xmax":8,"Correct":1,"XAxis":"Njets","Multip":True}
Plots["Nbjets"]={"NBins":4,"xmin":0,"xmax":4,"Correct":1,"XAxis":"Nbjets","Multip":True}
Plots["Mll"]={"NBins":20,"xmin":0,"xmax":400,"Correct":1e-3,"XAxis":"M_{ll}[GeV]"}
Plots["Mll_forwardelectron"]={"NBins":16,"xmin":51,"xmax":131,"Correct":1e-3,"XAxis":"M_{llForwardEl}[GeV]", "OverFlow": False}
Plots["met"]={"NBins":20,"xmin":0,"xmax":400,"Correct":1e-3,"XAxis":"E_{T}^{miss}[GeV]"}

Plots["vetoElectronPt"]={"NBins":20,"xmin":0,"xmax":10,"Correct":1e-3,"XAxis":"p_{T}(veto ele)[GeV]"}
Plots["vetoMuonPt"]={"NBins":20,"xmin":0,"xmax":10,"Correct":1e-3,"XAxis":"p_{T}(veto #mu)[GeV]"}

Plots["Lep1Pt"]={"NBins":20,"xmin":0,"xmax":400,"Correct":1e-3,"XAxis":"p_{T}(l_{1})[GeV]"}
Plots["Lep2Pt"]={"NBins":20,"xmin":0,"xmax":200,"Correct":1e-3,"XAxis":"p_{T}(l_{2})[GeV]"}
Plots["Lep3Pt"]={"NBins":20,"xmin":0,"xmax":100,"Correct":1e-3,"XAxis":"p_{T}(l_{3})[GeV]"}
Plots["Lep1Eta"]={"NBins":30,"xmin":-3.2,"xmax":3.2,"Correct":1,"XAxis":"#eta(l_{1})[GeV]","YScale":3}
Plots["Lep2Eta"]={"NBins":30,"xmin":-3.2,"xmax":3.2,"Correct":1,"XAxis":"#eta(l_{2})[GeV]","YScale":3}
Plots["Lep3Eta"]={"NBins":30,"xmin":-3.2,"xmax":3.2,"Correct":1,"XAxis":"#eta(l_{3})[GeV]","YScale":3}

Plots["LepAddAmbiguity"]={"NBins":7,"xmin":-3.5,"xmax":3.5,"Correct":1,"XAxis":"AddAmbiguity"}
Plots["q0q1"]={"NBins":7,"xmin":-3.5,"xmax":3.5,"Correct":1,"XAxis":"q0q1"}
Plots["forwardElPt"]={"NBins":25,"xmin":0,"xmax":100,"Correct":1e-3,"XAxis":"p_{T}(fwdEl)[GeV]"}
Plots["forwardElEta"]={"NBins":40,"xmin":-4,"xmax":4,"Correct":1,"XAxis":"#eta(fwdEl)[GeV]","YScale":3}
Plots["Jet1Pt"]={"NBins":20,"xmin":0,"xmax":400,"Correct":1e-3,"XAxis":"p_{T}(j_{1})[GeV]"}
Plots["Jet2Pt"]={"NBins":20,"xmin":0,"xmax":400,"Correct":1e-3,"XAxis":"p_{T}(j_{2})[GeV]"}
Plots["Jet1Eta"]={"NBins":30,"xmin":-3.2,"xmax":3.2,"Correct":1,"XAxis":"#eta(j_{1})[GeV]","YScale":3}
Plots["Jet2Eta"]={"NBins":30,"xmin":-3.2,"xmax":3.2,"Correct":1,"XAxis":"#eta(j_{2})[GeV]","YScale":3}
Plots["DRjj"]={"NBins":30,"xmin":0,"xmax":4,"Correct":1,"XAxis":"#Delta R(jj)"}


# Plots["SumEta"]={"NBins":13,"xmin":-5,"xmax":7,"Correct":1,"XAxis":"#sum_{i} #eta_{i}"}
# Plots["SumLEta"]={"NBins":13,"xmin":-5,"xmax":7,"Correct":1,"XAxis":"#sum^{#ell}_{i} #eta_{i}"}
# Plots["SumPt"]={"NBins":20,"xmin":0,"xmax":800,"Correct":1e-3,"XAxis":"#sum_{i} p_{T}^{i}[GeV]"}

Plots["DRll"]={"NBins":30,"xmin":0,"xmax":4,"Correct":1,"XAxis":"#Delta R(ll)"}
Plots["DEtajj"]={"NBins":30,"xmin":0,"xmax":1.5,"Correct":1,"XAxis":"#Delta #eta(jj)"}



#Plots["Mlll"]={"NBins":10,"xmin":0,"xmax":400,"Correct":1e-3,"XAxis":"M_{lll}[GeV]"}
Plots["Mlll"]={"NBins":25,"xmin":0,"xmax":400,"Correct":1e-3,"XAxis":"M_{lll}[GeV]"}
# Plots["LepOFPt"]={"NBins":20,"xmin":0,"xmax":400,"Correct":1e-3,"XAxis":"p_{T}(l_{OF})[GeV]"}
# Plots["LepOFEta"]={"NBins":10,"xmin":-3.2,"xmax":3.2,"Correct":1,"XAxis":"#eta(l_{OF})[GeV]","YScale":3}



# Plots["d0"]={"NBins":40,"xmin":-10,"xmax":10,"Correct":1,"XAxis":"d0"}
# Plots["Mll2"]={"NBins":20,"xmin":60,"xmax":120,"Correct":1e-3,"XAxis":"M_{ll}[GeV]"}
# Plots["MllOS"]={"NBins":40,"xmin":0,"xmax":200,"Correct":1e-3,"XAxis":"M_{ll}^{OS}[GeV]"}
# Plots["LepEl"]={"NBins":20,"xmin":0,"xmax":100,"Correct":1e-3,"XAxis":"p_{T}(e)[GeV]"}
# Plots["MllMET"]={"NBins":20,"xmin":0,"xmax":400,"Correct":1e-3,"XAxis":"M_{ll}[GeV]"}
# Plots["PtllMET"]={"NBins":20,"xmin":0,"xmax":400,"Correct":1e-3,"XAxis":"P_T^{ll}[GeV]"}
# Plots["MT2Lep"]={"NBins":20,"xmin":0,"xmax":200,"Correct":1e-3,"XAxis":"M_{T}[GeV]"}
# Plots["El1Pt"]={"NBins":29,"xmin":10,"xmax":300,"Correct":1e-3,"XAxis":"p_{T}(e_{1})[GeV]"}
# Plots["Mu1Pt"]={"NBins":29,"xmin":10,"xmax":300,"Correct":1e-3,"XAxis":"p_{T}(#mu_{1})[GeV]"}


# Plots["weight"]={"NBins":300,"xmin":-1.5,"xmax":1.5,"Correct":1,"XAxis":"weight"}
# Plots["weightMC"]={"NBins":300,"xmin":-1.5,"xmax":1.5,"Correct":1,"XAxis":"weight"}
# Plots["weightJVT"]={"NBins":300,"xmin":-1.5,"xmax":1.5,"Correct":1,"XAxis":"weight"}
# Plots["weightBtag"]={"NBins":300,"xmin":-1.5,"xmax":1.5,"Correct":1,"XAxis":"weight"}
# Plots["weightEffSF"]={"NBins":300,"xmin":-1.5,"xmax":1.5,"Correct":1,"XAxis":"weight"}
# Plots["weightEffReco"]={"NBins":300,"xmin":-1.5,"xmax":1.5,"Correct":1,"XAxis":"weight"}
# Plots["weightIso"]={"NBins":300,"xmin":-1.5,"xmax":1.5,"Correct":1,"XAxis":"weight"}
# Plots["weightTTVA"]={"NBins":300,"xmin":-1.5,"xmax":1.5,"Correct":1,"XAxis":"weight"}
# Plots["weightTrig"]={"NBins":300,"xmin":-1.5,"xmax":1.5,"Correct":1,"XAxis":"weight"}

#Plots["OFLep"]={"NBins":20,"xmin":0,"xmax":100,"Correct":1e-3,"XAxis":"p_{T}(l_{OF})[GeV]"}
# 
#
# Plots["MT"]={"NBins":20,"xmin":0,"xmax":200,"Correct":1e-3,"XAxis":"M^{W}_{T}[GeV]"}

# Plots["Jet1E"]={"NBins":20,"xmin":0,"xmax":400,"Correct":1e-3,"XAxis":"E(j_{1})[GeV]"}
# Plots["Jet2E"]={"NBins":20,"xmin":0,"xmax":400,"Correct":1e-3,"XAxis":"E(j_{2})[GeV]"}
# Plots["Lep1E"]={"NBins":20,"xmin":0,"xmax":400,"Correct":1e-3,"XAxis":"E(l_{1})[GeV]"}
# Plots["Lep2E"]={"NBins":20,"xmin":0,"xmax":400,"Correct":1e-3,"XAxis":"E(l_{2})[GeV]"}
# Plots["MWWW"]={"NBins":10,"xmin":200,"xmax":700,"Correct":1e-3,"XAxis":"M_{WWW}[GeV]"}
# Plots["MWWW-NoMet"]={"NBins":10,"xmin":200,"xmax":700,"Correct":1e-3,"XAxis":"M_{WWW} (No #E_{T}^{miss})[GeV]"}
# Plots["Nlooseel"]={"NBins":4,"xmin":0,"xmax":4,"Correct":1,"XAxis":"NLooseEl"}
# Plots["Nloosemu"]={"NBins":4,"xmin":0,"xmax":4,"Correct":1,"XAxis":"NLoosMu"}
# Plots["Nloose"]={"NBins":4,"xmin":0,"xmax":4,"Correct":1,"XAxis":"NLooseleptons"}
#Plots["Lep1PromptWeight"]={"NBins":60,"xmin":-1.5,"xmax":1.5,"Correct":1,"XAxis":"PLV_{l1}"}
#Plots["Lep2PromptWeight"]={"NBins":60,"xmin":-1.5,"xmax":1.5,"Correct":1,"XAxis":"PLV_{l2}"}
#Plots["LepPLVEl"]={"NBins":60,"xmin":-1.5,"xmax":1.5,"Correct":1,"XAxis":"PLV_{e}"}
#Plots["LepPLVMu"]={"NBins":60,"xmin":-1.5,"xmax":1.5,"Correct":1,"XAxis":"PLV_{#mu}"}
#Plots["Lep1ECIDSBDT"]={"NBins":60,"xmin":-1.5,"xmax":1.5,"Correct":1,"XAxis":"ECIDS_{l1}"}
#Plots["Lep2ECIDSBDT"]={"NBins":60,"xmin":-1.5,"xmax":1.5,"Correct":1,"XAxis":"ECIDS_{l2}"}
#Plots["LepECIDSBDT"]={"NBins":60,"xmin":-1.5,"xmax":1.5,"Correct":1,"XAxis":"ECIDS"}
#Plots["Lep1PtRatio"]={"NBins":20,"xmin":0,"xmax":2,"Correct":1,"XAxis":"track p_{T}/p_{T}(l_{1})[GeV]"}
#Plots["Lep2PtRatio"]={"NBins":20,"xmin":0,"xmax":2,"Correct":1,"XAxis":"track p_{T}//p_{T}(l_{2})[GeV]"}
#Plots["Lep1EndCapPt"]={"NBins":40,"xmin":20,"xmax":160,"Correct":1e-3,"XAxis":"endcap p_{T}(l_{1})[GeV]"}
#Plots["Lep2EndCapPt"]={"NBins":20,"xmin":20,"xmax":100,"Correct":1e-3,"XAxis":"endcap p_{T}(l_{2})[GeV]"}


#Plots["Lep1PromptWeight"]={"NBins":40,"xmin":-1.5,"xmax":1.5,"Correct":1,"XAxis":"PromptWeight_{l1}","Logy":True,"YScale":100}
#Plots["Lep2PromptWeight"]={"NBins":40,"xmin":-1.5,"xmax":1.5,"Correct":1,"XAxis":"PromptWeight_{l2}","Logy":True,"YScale":100}



Variations = ["Nominal"] #,"NonPromptBinning_E","NonPromptBtag77_E","NonPromptStat_E","NonPromptBinning_M","NonPromptBtag77_M","NonPromptStat_M","CFwBG","CFStatSys","VgammaEEE","VgammaStat","WvsZgamma","MUON_EFF_RECO_STAT__1down","MUON_EFF_RECO_STAT__1up","MUON_EFF_RECO_SYS__1down","MUON_EFF_RECO_SYS__1up","MUON_EFF_RECO_STAT_LOWPT__1down","MUON_EFF_RECO_STAT_LOWPT__1up","MUON_EFF_RECO_SYS_LOWPT__1down","MUON_EFF_RECO_SYS_LOWPT__1up","MUON_EFF_ISO_STAT__1down","MUON_EFF_ISO_STAT__1up","MUON_EFF_ISO_SYS__1down","MUON_EFF_ISO_SYS__1up","EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down","EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up","EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down","EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up","EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down","EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up","EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down","EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up","MUON_EFF_TTVA_STAT__1down","MUON_EFF_TTVA_STAT__1up","MUON_EFF_TTVA_SYS__1down","MUON_EFF_TTVA_SYS__1up","JET_JvtEfficiency__1down","JET_JvtEfficiency__1up","PRW_DATASF__1down","PRW_DATASF__1up","MET_SoftTrk_ResoPara__1up","MET_SoftTrk_ResoPerp__1up","MET_SoftTrk_Scale__1down","MET_SoftTrk_Scale__1up","MUON_SCALE__1down","MUON_SCALE__1up","MUON_ID__1down","MUON_ID__1up","MUON_MS__1down","MUON_MS__1up","MUON_SAGITTA_RHO__1down","MUON_SAGITTA_RHO__1up","MUON_SAGITTA_RESBIAS__1down","MUON_SAGITTA_RESBIAS__1up","EG_RESOLUTION_ALL__1down","EG_RESOLUTION_ALL__1up","EG_SCALE_ALL__1down","EG_SCALE_ALL__1up","JET_JER_SINGLE_NP__1up","JET_31NP_JET_BJES_Response__1down","JET_31NP_JET_BJES_Response__1up","JET_31NP_JET_EffectiveNP_Detector1__1down","JET_31NP_JET_EffectiveNP_Detector1__1up","JET_31NP_JET_EffectiveNP_Detector2__1down","JET_31NP_JET_EffectiveNP_Detector2__1up","JET_31NP_JET_RelativeNonClosure_MC16__1down","JET_31NP_JET_RelativeNonClosure_MC16__1up","JET_31NP_JET_EffectiveNP_Mixed1__1down","JET_31NP_JET_EffectiveNP_Mixed1__1up","JET_31NP_JET_EffectiveNP_Mixed2__1down","JET_31NP_JET_EffectiveNP_Mixed2__1up","JET_31NP_JET_EffectiveNP_Mixed3__1down","JET_31NP_JET_EffectiveNP_Mixed3__1up","JET_31NP_JET_EffectiveNP_Modelling1__1down","JET_31NP_JET_EffectiveNP_Modelling1__1up","JET_31NP_JET_EffectiveNP_Modelling2__1down","JET_31NP_JET_EffectiveNP_Modelling2__1up","JET_31NP_JET_EffectiveNP_Modelling3__1down","JET_31NP_JET_EffectiveNP_Modelling3__1up","JET_31NP_JET_EffectiveNP_Modelling4__1down","JET_31NP_JET_EffectiveNP_Modelling4__1up","JET_31NP_JET_EffectiveNP_Statistical1__1down","JET_31NP_JET_EffectiveNP_Statistical1__1up","JET_31NP_JET_EffectiveNP_Statistical2__1down","JET_31NP_JET_EffectiveNP_Statistical2__1up","JET_31NP_JET_EffectiveNP_Statistical3__1down","JET_31NP_JET_EffectiveNP_Statistical3__1up","JET_31NP_JET_EffectiveNP_Statistical4__1down","JET_31NP_JET_EffectiveNP_Statistical4__1up","JET_31NP_JET_EffectiveNP_Statistical5__1down","JET_31NP_JET_EffectiveNP_Statistical5__1up","JET_31NP_JET_EffectiveNP_Statistical6__1down","JET_31NP_JET_EffectiveNP_Statistical6__1up","JET_31NP_JET_EtaIntercalibration_Modelling__1down","JET_31NP_JET_EtaIntercalibration_Modelling__1up","JET_31NP_JET_EtaIntercalibration_NonClosure_highE__1down","JET_31NP_JET_EtaIntercalibration_NonClosure_highE__1up","JET_31NP_JET_EtaIntercalibration_NonClosure_negEta__1down","JET_31NP_JET_EtaIntercalibration_NonClosure_negEta__1up","JET_31NP_JET_EtaIntercalibration_NonClosure_posEta__1down","JET_31NP_JET_EtaIntercalibration_NonClosure_posEta__1up","JET_31NP_JET_EtaIntercalibration_TotalStat__1down","JET_31NP_JET_EtaIntercalibration_TotalStat__1up","JET_31NP_JET_Flavor_Composition__1down","JET_31NP_JET_Flavor_Composition__1up","JET_31NP_JET_Flavor_Response__1down","JET_31NP_JET_Flavor_Response__1up","JET_31NP_JET_Pileup_OffsetMu__1down","JET_31NP_JET_Pileup_OffsetMu__1up","JET_31NP_JET_Pileup_OffsetNPV__1down","JET_31NP_JET_Pileup_OffsetNPV__1up","JET_31NP_JET_Pileup_PtTerm__1down","JET_31NP_JET_Pileup_PtTerm__1up","JET_31NP_JET_Pileup_RhoTopology__1down","JET_31NP_JET_Pileup_RhoTopology__1up","JET_31NP_JET_PunchThrough_MC16__1down","JET_31NP_JET_PunchThrough_MC16__1up","JET_31NP_JET_SingleParticle_HighPt__1down","JET_31NP_JET_SingleParticle_HighPt__1up","MET_JetTrk_Scale__1down","MET_JetTrk_Scale__1up","FT_EFF_Eigen_B_0_AntiKt4EMTopoJets__1down","FT_EFF_Eigen_B_0_AntiKt4EMTopoJets__1up","FT_EFF_Eigen_B_1_AntiKt4EMTopoJets__1down","FT_EFF_Eigen_B_1_AntiKt4EMTopoJets__1up","FT_EFF_Eigen_B_2_AntiKt4EMTopoJets__1down","FT_EFF_Eigen_B_2_AntiKt4EMTopoJets__1up","FT_EFF_Eigen_B_3_AntiKt4EMTopoJets__1down","FT_EFF_Eigen_B_3_AntiKt4EMTopoJets__1up","FT_EFF_Eigen_B_4_AntiKt4EMTopoJets__1down","FT_EFF_Eigen_B_4_AntiKt4EMTopoJets__1up","FT_EFF_Eigen_B_5_AntiKt4EMTopoJets__1down","FT_EFF_Eigen_B_5_AntiKt4EMTopoJets__1up","FT_EFF_Eigen_B_6_AntiKt4EMTopoJets__1down","FT_EFF_Eigen_B_6_AntiKt4EMTopoJets__1up","FT_EFF_Eigen_B_7_AntiKt4EMTopoJets__1down","FT_EFF_Eigen_B_7_AntiKt4EMTopoJets__1up","FT_EFF_Eigen_B_8_AntiKt4EMTopoJets__1down","FT_EFF_Eigen_B_8_AntiKt4EMTopoJets__1up","FT_EFF_Eigen_C_0_AntiKt4EMTopoJets__1down","FT_EFF_Eigen_C_0_AntiKt4EMTopoJets__1up","FT_EFF_Eigen_C_1_AntiKt4EMTopoJets__1down","FT_EFF_Eigen_C_1_AntiKt4EMTopoJets__1up","FT_EFF_Eigen_C_2_AntiKt4EMTopoJets__1down","FT_EFF_Eigen_C_2_AntiKt4EMTopoJets__1up","FT_EFF_Eigen_Light_0_AntiKt4EMTopoJets__1down","FT_EFF_Eigen_Light_0_AntiKt4EMTopoJets__1up","FT_EFF_Eigen_Light_10_AntiKt4EMTopoJets__1down","FT_EFF_Eigen_Light_10_AntiKt4EMTopoJets__1up","FT_EFF_Eigen_Light_1_AntiKt4EMTopoJets__1down","FT_EFF_Eigen_Light_1_AntiKt4EMTopoJets__1up","FT_EFF_Eigen_Light_2_AntiKt4EMTopoJets__1down","FT_EFF_Eigen_Light_2_AntiKt4EMTopoJets__1up","FT_EFF_Eigen_Light_3_AntiKt4EMTopoJets__1down","FT_EFF_Eigen_Light_3_AntiKt4EMTopoJets__1up","FT_EFF_Eigen_Light_4_AntiKt4EMTopoJets__1down","FT_EFF_Eigen_Light_4_AntiKt4EMTopoJets__1up","FT_EFF_Eigen_Light_5_AntiKt4EMTopoJets__1down","FT_EFF_Eigen_Light_5_AntiKt4EMTopoJets__1up","FT_EFF_Eigen_Light_6_AntiKt4EMTopoJets__1down","FT_EFF_Eigen_Light_6_AntiKt4EMTopoJets__1up","FT_EFF_Eigen_Light_7_AntiKt4EMTopoJets__1down","FT_EFF_Eigen_Light_7_AntiKt4EMTopoJets__1up","FT_EFF_Eigen_Light_8_AntiKt4EMTopoJets__1down","FT_EFF_Eigen_Light_8_AntiKt4EMTopoJets__1up","FT_EFF_Eigen_Light_9_AntiKt4EMTopoJets__1down","FT_EFF_Eigen_Light_9_AntiKt4EMTopoJets__1up","FT_EFF_extrapolation_AntiKt4EMTopoJets__1down","FT_EFF_extrapolation_AntiKt4EMTopoJets__1up","FT_EFF_extrapolation_from_charm_AntiKt4EMTopoJets__1down","FT_EFF_extrapolation_from_charm_AntiKt4EMTopoJets__1up","MUON_EFF_TrigSystUncertainty__1up","MUON_EFF_TrigStatUncertainty__1up","MUON_EFF_TrigSystUncertainty__1down","MUON_EFF_TrigStatUncertainty__1down"]

MakeEffPlots=True
MakeTables=True
Verbose=True
Blind=False
EventPrint=True
useCondor=False
Mode="2l2j"
doQCDnCF=[False,False] #First for QCD, second for CF
#doQCDnCF=[True,True] #First for QCD, second for CF

####################
# Test WZ VBS   #
####################
WWWAnalyzeCondor("WZ VBS",Plots,doQCDnCF,MakeEffPlots,MakeTables,Verbose,Blind,EventPrint,useCondor,"3l",Variations)
#Plots["Mlll"]={"NBins":50,"xmin":110,"xmax":610,"Correct":1e-3,"XAxis":"M_{lll}[GeV]"}
#WWWAnalyzeCondor("WZ CR",Plots,[False,False],MakeEffPlots,MakeTables,Verbose,True,EventPrint,useCondor,"3l",Variations)
#WWWAnalyzeCondor("MjjFull",Plots,[False,False],MakeEffPlots,MakeTables,Verbose,True,EventPrint,useCondor,Mode,Variations)

#WWWAnalyzeCondor("WZ CRwoFwdEl",Plots,[False,False],MakeEffPlots,MakeTables,Verbose,True,EventPrint,useCondor,"3l",Variations)
#WWWAnalyzeCondor("MjjFullwoFwdEl",Plots,[False,False],MakeEffPlots,MakeTables,Verbose,True,EventPrint,useCondor,Mode,Variations)
#WWWAnalyzeCondor("MjjFullwithFwdEl",Plots,[False,False],MakeEffPlots,MakeTables,Verbose,True,EventPrint,useCondor,Mode,Variations)
####################
# Control Regions  #
####################
#WWWAnalyzeCondor("BTag CR",Plots,doQCDnCF,MakeEffPlots,MakeTables,Verbose,Blind,EventPrint,useCondor,Mode,Variations)
#WWWAnalyzeCondor("Side Band CR",Plots,doQCDnCF,MakeEffPlots,MakeTables,Verbose,Blind,EventPrint,useCondor,Mode,Variations)
# Plots["Mlll"]={"NBins":50,"xmin":110,"xmax":610,"Correct":1e-3,"XAxis":"M_{lll}[GeV]"}
#WWWAnalyzeCondor("WZ CR",Plots,doQCDnCF,MakeEffPlots,MakeTables,Verbose,Blind,EventPrint,useCondor,"3l",Variations)
# WWWAnalyzeCondor("WZ2j CR",Plots,doQCDnCF,MakeEffPlots,MakeTables,Verbose,Blind,EventPrint,useCondor,"3l",Variations)
#Plots["Mll"]={"NBins":20,"xmin":70,"xmax":110,"Correct":1e-3,"XAxis":"M_{ll}[GeV]"}
#WWWAnalyzeCondor("ZWindow CR",Plots,doQCDnCF,MakeEffPlots,MakeTables,Verbose,Blind,EventPrint,useCondor,Mode,Variations)
#Plots["Mlll"]={"NBins":20,"xmin":70,"xmax":110,"Correct":1e-3,"XAxis":"M_{lll}[GeV]"}
#Plots["Mll"]={"NBins":10,"xmin":0,"xmax":100,"Correct":1e-3,"XAxis":"M_{ll}[GeV]"}
#WWWAnalyzeCondor("WZ Vgamma CR",Plots,doQCDnCF,MakeEffPlots,MakeTables,Verbose,Blind,EventPrint,useCondor,"3l",Variations)

####################
### Vgamma Rate  ###
####################
#WWWAnalyzeCondor("WZ Vgamma CR",Plots,doQCDnCF,MakeEffPlots,MakeTables,Verbose,Blind,EventPrint,useCondor,"3l",Variations)


####################
###  Fake Factor ###
####################
#WWWAnalyzeCondor("BTag CR",Plots,doQCDnCF,MakeEffPlots,MakeTables,Verbose,Blind,EventPrint,useCondor,"Non Prompt3",Variations)
# WWWAnalyzeCondor("BTag77 CR",Plots,doQCDnCF,MakeEffPlots,MakeTables,Verbose,Blind,EventPrint,useCondor,"Non Prompt3",Variations)
#WWWAnalyzeCondor("3l 1B 0SFOS",Plots,doQCDnCF,MakeEffPlots,MakeTables,Verbose,Blind,EventPrint,useCondor,"Non Prompt",Variations)
# WWWAnalyzeCondor("3l 1B77 0SFOS",Plots,doQCDnCF,MakeEffPlots,MakeTables,Verbose,Blind,EventPrint,useCondor,"Non Prompt",Variations)

####################
## Signal Region  ##
####################
# 2l2j channel
#WWWAnalyzeCondor("MjjFullold",Plots,doQCDnCF,MakeEffPlots,MakeTables,Verbose,True,EventPrint,useCondor,Mode,Variations)
#WWWAnalyzeCondor("MjjFull",Plots,doQCDnCF,MakeEffPlots,MakeTables,Verbose,True,EventPrint,useCondor,Mode,Variations)
#WWWAnalyzeCondor("MjjFull Lep1EndCap",Plots,[False,False],MakeEffPlots,MakeTables,Verbose,True,EventPrint,useCondor,Mode,Variations)
#WWWAnalyzeCondor("MjjFull Lep2EndCap",Plots,[False,False],MakeEffPlots,MakeTables,Verbose,True,EventPrint,useCondor,Mode,Variations)
#WWWAnalyzeCondor("WWW SR",Plots,doQCDnCF,MakeEffPlots,MakeTables,Verbose,True,EventPrint,useCondor,Mode,Variations)
# 3l channel
#WWWAnalyzeCondor("Less2Jets",Plots,doQCDnCF,MakeEffPlots,MakeTables,Verbose,Blind,EventPrint,useCondor,Mode,Variations)
# Plots["q0q1"]={"NBins":2,"xmin":-2,"xmax":2,"Correct":1,"XAxis":"q_{0}*q_{1}","Multip":True}
# WWWAnalyzeCondor("3l SR",Plots,doQCDnCF,MakeEffPlots,MakeTables,Verbose,True,EventPrint,useCondor,"q0q1",Variations)
#WWWAnalyzeCondor("3l SR",Plots,doQCDnCF,MakeEffPlots,MakeTables,Verbose,True,EventPrint,useCondor,"q0q1Split",Variations)
#WWWAnalyzeCondor("3l SR",Plots,doQCDnCF,MakeEffPlots,MakeTables,Verbose,True,EventPrint,useCondor,"q0q13l",Variations)

# for DataSet in DataSets.FullMCSet.keys():
#     if "data" in DataSet.lower(): continue
#     CreateVectors("WWW SR",DataSet)
    
