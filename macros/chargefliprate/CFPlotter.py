import ROOT
from array import array

ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetOptStat(0)

InputPlot = ROOT.TFile("test.root","open")
mHist = InputPlot.Get("test_misid")


etas = [0.0, 0.6, 1.1, 1.52, 1.7, 2.3, 2.5]
pts  = [20.,27.,60.,90.,130.,1000.]
ptsTMP  = [20.,27.,60.,90.,130.,200]

colors = [ROOT.kRed,ROOT.kMagenta,ROOT.kBlue,ROOT.kCyan,ROOT.kGreen,ROOT.kYellow+3,ROOT.kBlack]


hPT = [ ROOT.TH1F("HistPT"+str(index),"HistPt"+str(index),len(pts)-1,array('f',ptsTMP)) for index in range(0,len(etas)-1)]
hPT
tPT = [ "" for index in range(0,len(etas)-1) ]
hETA =[ ROOT.TH1F("HistEta"+str(index),"HistEta"+str(index),len(etas)-1,array('f',etas)) for index in range(0,len(ptsTMP)-1)]
tETA = [ "" for index in range(0,len(pts)-1) ]

for i in range(0,len(etas)-1):
    for j in range(0,len(pts)-1):

        hPT[i].SetBinContent(j+1,mHist.GetBinContent(i+1,j+1))
        hPT[i].SetBinError(j+1,mHist.GetBinError(i+1,j+1))

        print mHist.GetBinContent(i+1,j+1),mHist.GetBinError(i+1,j+1)


        tPT[i] = str(etas[i])+" < | #eta | < "+str(etas[i+1])

        
        hETA[j].SetBinContent(i+1,mHist.GetBinContent(i+1,j+1))
        hETA[j].SetBinError(i+1,mHist.GetBinError(i+1,j+1))

        tETA[j] = str(pts[j])+" GeV < | pT | < "+str(pts[j+1])+" GeV"
        


Canvas = ROOT.TCanvas("c1","canvas",600,600)
mylegend = ROOT.TLegend(0.1, 0.6, 0.4, 0.88)
mylegend.SetFillColor(0)
mylegend.SetBorderSize(0)

lLumi = ROOT.TLatex()
lLumi.SetNDC(1);
lLumi.SetTextFont(42);


hPT[0].SetTitle("")
hPT[0].GetXaxis().SetTitle("p_{T}")
hPT[0].GetYaxis().SetTitleOffset(1.5)
hPT[0].GetYaxis().SetTitle("#epsilon_{miss-ID}")

hPT[0].SetMinimum(0.00001)
hPT[0].SetMaximum(0.1)


Canvas.SetLogy()

Canvas.Draw()

for i in range(0,len(etas)-1):
    hPT[i].SetLineColor(colors[i])
    hPT[i].SetMarkerColor(colors[i])
    hPT[i].SetMarkerStyle(20)
    hPT[i].Draw("SAME")

    mylegend.AddEntry(hPT[i],tPT[i])

lLumi.DrawLatex(0.52,0.12,"#font[72]{ATLAS} Internal")
mylegend.Draw()
Canvas.RedrawAxis()



Canvas.SaveAs("PT-CF.png")






Canvas = ROOT.TCanvas("c2","canvas",600,600)
mylegend = ROOT.TLegend(0.1, 0.6, 0.4, 0.88)
mylegend.SetFillColor(0)
mylegend.SetBorderSize(0)

lLumi = ROOT.TLatex()
lLumi.SetNDC(1);
lLumi.SetTextFont(42);


hETA[0].SetTitle("")
hETA[0].GetXaxis().SetTitle("#eta")
hETA[0].GetYaxis().SetTitleOffset(1.5)
hETA[0].GetYaxis().SetTitle("#epsilon_{miss-ID}")

hETA[0].SetMinimum(0.00001)
hETA[0].SetMaximum(0.1)


Canvas.SetLogy()

Canvas.Draw()

for i in range(0,len(pts)-1):
    hETA[i].SetLineColor(colors[i])
    hETA[i].SetMarkerColor(colors[i])
    hETA[i].SetMarkerStyle(20)
    hETA[i].Draw("SAME")

    mylegend.AddEntry(hETA[i],tETA[i])

lLumi.DrawLatex(0.52,0.12,"#font[72]{ATLAS} Internal")
mylegend.Draw()
Canvas.RedrawAxis()



Canvas.SaveAs("ETA-CF.png")

