'''
 TUAN M. NGUYEN (PhD Student; Supervisor: Jean-Francois Arguin)
 Universite de Montreal
 tuan.nguyen.manh@cern.ch
 
 Electron charge flip
'''



#######################################################
# Minuit
#######################################################
def logLlhPair(i, j, parameters):
  nIJ = n[i,j]
  nssIJ = nss[i,j]
  epsI, epsJ = parameters[i], parameters[j]
  expectedNss = nIJ * ((1-epsI)*epsJ + (1-epsJ)*epsI)
  try:
    #logLlh = nssIJ * log(expectedNss) - expectedNss
    logLlh = nssIJ * log(expectedNss) - expectedNss - lgamma(nssIJ)
  except ValueError:
    logLlh = 0.0
  return logLlh

def negativeLog(numParameters, parameters):
  negLog = 0.0
  i = 0
  while i < numParameters[0]:
    j = 0
    while j < numParameters[0]:
      negLog -= logLlhPair(i, j, parameters)
      j += 1
    i += 1
  return negLog

# Every parameter of fcn is an array
def fcn(numParameters, derivatives, function, parameters, internalFlag):
  function[0] = negativeLog(numParameters, parameters)

def ratesAndErrors(numParameters, minuit):
  rates = np.zeros(numParameters)
  rateErrors = np.zeros(numParameters)
  rate, rateError = Double(0), Double(0)
  for i in xrange(numParameters):
    minuit.GetParameter(i, rate, rateError)
    rates[i] = float(rate)
    rateErrors[i] = float(rateError)
  return rates, rateErrors

def runMinuit(numParameters):
  minuit = TMinuit(numParameters)
  minuit.SetPrintLevel(0) 
  
  minuit.SetFCN(fcn)
  arglist = np.zeros(numParameters) + 0.01
  internalFlag, arglist[0] = Long(0), 0.5
  minuit.mnexcm("SET ERR", arglist, 1, internalFlag)
  
  initialValues = np.zeros(numParameters) + 0.01
  steps = np.zeros(numParameters) + 0.001
  
  for i in xrange(numParameters):
    name = "epsilon%s" % i
    minuit.mnparm(i, name, initialValues[i], steps[i], 0, 1, internalFlag)
  
  # arglist[0] = 2
  # minuit.mnexcm("SET STR", arglist, 1, internalFlag)
  
  arglist[0], arglist[1] = 10000, 0.1
  minuit.mnexcm("SIMPLEX", arglist, 1, internalFlag)
  minuit.mnexcm("MIGRAD", arglist, 1, internalFlag)
  
  print "FIT STATUS is " +str(minuit.GetStatus())
  return ratesAndErrors(numParameters, minuit)


#######################################################
# Run
#######################################################
from ROOT import TMinuit, Long, Double

from math import log, lgamma
import numpy as np

import ROOT
import rutil
import time

start_time = time.time()
##############################

etas = [0.0, 0.6, 1.1, 1.52, 1.7, 2.3, 2.5]
pts  = [20.,27.,60.,90.,130.,1000.]


##############################
nEta,nPt = len(etas),len(pts)
numParameters = (nEta-1)*(nPt-1)
##############################

def doIt(n, nss, name, outFile):
  rates,errors = runMinuit(numParameters)
  ratesN, errorsN = '%s_rates.txt' % name, '%s_errors.txt' % name ##### Can also be without ".txt" at the not to save as txt
  with open(ratesN, 'wb') as ratesF, open(errorsN, 'wb') as errorsF:
    np.savetxt(ratesF, rates)   ##### if no ".txt" is given above, here use just save()
    np.savetxt(errorsF, errors) ##### if no ".txt" is given above, here use just save()
    with open(outFile, 'a') as txtOut:
      txtOut.write('%s\n%s\n' % (ratesN, errorsN))
  reLabels = rutil.ratesErrorsBins(rates, errors, etas, pts)
  rutil.printRatesErrorsBins(name, reLabels)
  
##### This part was added for TH2 plotting
  #bName = rutil.beautifyName(name)
  rutil.writeToFile('%s.root' % name, rutil.hist2D(rates, errors, etas, pts, '%s_misid' % name))
#####

  
##############################
# This stuff below cannot be put into a function, 
# because Minuit needs to see global variables n and nss!!!

if __name__ == '__main__':
  import sys
  # txtFile, outFile = sys.argv[1], sys.argv[2]
  # with open(txtFile, 'rb') as txt:
  #   lines = txt.read().splitlines()
  # itLines = iter(lines)
  # for ncsv in itLines:
  #   nsscsv = next(itLines)
  #   print 'Estimating rates for ... %s and %s' % (ncsv, nsscsv)
  #   n, nss = rutil.toan(ncsv), rutil.toan(nsscsv)

  outFile = "Out.txt"

  nss = np.zeros(shape=(numParameters,numParameters))
  n = np.zeros(shape=(numParameters,numParameters))

  InputFile = ROOT.TFile("CFROOT/CFCR.root","open")
  for i1 in range(0,len(etas)-1):
    for i2 in range(0,len(etas)-1):
        for j1 in range(0,len(pts)-1):
            for j2 in range(0,len(pts)-1):
                try:
                  IntegralOS = InputFile.Get("Mll_ZW1_"+str(i1)+str(j1)+str(i2)+str(j2)).Integral()
                  IntegralSS = InputFile.Get("Mll_ZW0_"+str(i1)+str(j1)+str(i2)+str(j2)).Integral()

                  SideBandOS = InputFile.Get("Mll_Side1_"+str(i1)+str(j1)+str(i2)+str(j2)).Integral()
                  SideBandSS = InputFile.Get("Mll_Side0_"+str(i1)+str(j1)+str(i2)+str(j2)).Integral()

                  IntegralOS+=-1*SideBandOS
                  IntegralSS+=-1*SideBandSS
                  
                  print IntegralOS,IntegralSS

                  if IntegralOS<0 or IntegralSS<0:
                    IntegralOS=0;
                    IntegralSS=0;

                  print etas[i1],etas[i2],pts[j1],pts[j2],IntegralSS,IntegralOS,SideBandOS,SideBandSS
                  nss[i1*(len(pts)-1)+(j1),i2*(len(pts)-1)+(j2)]=IntegralSS
                  n[i1*(len(pts)-1)+(j1),i2*(len(pts)-1)+(j2)]=IntegralOS+IntegralSS
                except:
                  print "Problem in",etas[i1],etas[i2],pts[j1],pts[j2]
                  print "Mll_ZW1_"+str(i1)+str(j1)+str(i2)+str(j2)

                  
  doIt(n, nss, "test", outFile)




print("--- %s TIME ---" % (time.time() - start_time))


