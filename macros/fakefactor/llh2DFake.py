
'''
 TUAN M. NGUYEN (PhD Student; Supervisor: Jean-Francois Arguin)
 Universite de Montreal
 tuan.nguyen.manh@cern.ch
 
 Electron charge flip
'''



#######################################################
# Minuit
#######################################################
def logLlhPair(parameters):

  FtE, FtM  = parameters[0],parameters[1]


  expEE = FtE*(dAEe-AEe) + Iee
  expEM = FtM*(dAMe-AMe) + FtE*(dAEm-AEm) + Iem
  expMM = FtM*(dAMm-AMm) + Imm



  expEEMSFOS = FtE*(dAeemSFOS-AeemSFOS)+ IeemSFOS
  expMMESFOS = FtM*(dAmmeSFOS-AmmeSFOS)+ ImmeSFOS

  

  try:
    logLlh =0

    logLlh += dIee * log(expEE) - expEE - lgamma(dIee) 
    logLlh += dIem * log(expEM) - expEM - lgamma(dIem) 
    logLlh += dImm * log(expMM) - expMM - lgamma(dImm) 

    logLlh += dImmeSFOS * log(expMMESFOS) - expMMESFOS - lgamma(dImmeSFOS) 
    logLlh += dIeemSFOS * log(expEEMSFOS) - expEEMSFOS - lgamma(dIeemSFOS) 


    print dIee,expEE,dIem,expEM,dImm,expMM,dImmeSFOS,expMMESFOS,dIeemSFOS,expEEMSFOS


  except ValueError:
    logLlh = 0.0
  return logLlh

def negativeLog(n,parameters):
  
  negLog = -logLlhPair(parameters)
  return negLog

# Every parameter of fcn is an array
def fcn(numParameters, derivatives, function, parameters, internalFlag):
  function[0] = negativeLog(numParameters, parameters)

def ratesAndErrors(numParameters, minuit):
  rates = np.zeros(numParameters)
  rateErrors = np.zeros(numParameters)
  rate, rateError = Double(0), Double(0)
  for i in xrange(numParameters):
    minuit.GetParameter(i, rate, rateError)
    rates[i] = float(rate)
    rateErrors[i] = float(rateError)
  return rates, rateErrors

def runMinuit(numParameters):
  minuit = TMinuit(numParameters)
  minuit.SetPrintLevel(0) 
  
  minuit.SetFCN(fcn)
  arglist = np.zeros(numParameters) + 0.01
  print numParameters
  print arglist
  internalFlag, arglist[0] = Long(0), 0.5
  minuit.mnexcm("SET ERR", arglist, 1, internalFlag)
  
  initialValues = np.zeros(numParameters) + 0.001
  steps = np.zeros(numParameters) + 0.0001
  
  for i in xrange(numParameters):
    name = "epsilon%s" % i
    minuit.mnparm(i, name, initialValues[i], steps[i], 0, 1, internalFlag)
  
  # arglist[0] = 2
  # minuit.mnexcm("SET STR", arglist, 1, internalFlag)
  
  #arglist[0], arglist[1] = 10000, 0.1
  minuit.mnexcm("SIMPLEX", arglist, 1, internalFlag)
  minuit.mnexcm("MIGRAD", arglist, 1, internalFlag)
  
  print "FIT STATUS is " +str(minuit.GetStatus())
  return ratesAndErrors(numParameters, minuit)


#######################################################
# Run
#######################################################
from ROOT import TMinuit, Long, Double

from math import log, lgamma
import numpy as np

import ROOT
#import rutil
import time

start_time = time.time()
##############################

etas = [0.0,2.5]
pts  = [20.,27.,1000]

##############################
nEta,nPt = len(etas),len(pts)
numParameters = 2*(nEta-1)*(nPt-1)
##############################

def doIt(name, outFile):
  rates,errors = runMinuit(numParameters)
  
  # print nIII0SFOS, nIII1SFOSHigh,nIIIBtagCR,nIIIZW
  # print dIII0SFOS, dIII1SFOSHigh,dIIIBtagCR,dIIIZW
  # print nIIAT0SFOS, nIIAZ1SFOSHigh,nIIAT1SFOSHigh, nIIAZBtagCR,nIIATBtagCR,nIIAZZW,nIIATZW
  # print dIIAT0SFOS, dIIAZ1SFOSHigh,dIIAT1SFOSHigh, dIIAZBtagCR,dIIATBtagCR,dIIAZZW,dIIATZW


  
  ratesN, errorsN = '%s_rates.txt' % name, '%s_errors.txt' % name ##### Can also be without ".txt" at the not to save as txt
  with open(ratesN, 'wb') as ratesF,open(errorsN, 'wb') as errorsF:
    np.savetxt(ratesF, rates)   ##### if no ".txt" is given above, here use just save()
    np.savetxt(errorsF, errors) ##### if no ".txt" is given above, here use just save()
    with open(outFile, 'a') as txtOut:
      txtOut.write('%s\n%s\n' % (ratesN, errorsN))
  #reLabels = rutil.ratesErrorsBins(rates, errors, etas, pts)
  #rutil.printRatesErrorsBins(name, reLabels)
  
##### This part was added for TH2 plotting
  #bName = rutil.beautifyName(name)
  #rutil.writeToFile('%s.root' % name, rutil.hist2D(rates, errors, etas, pts, '%s_misid' % name))
#####

  
##############################
# This stuff below cannot be put into a function, 
# because Minuit needs to see global variables n and nss!!!

if __name__ == '__main__':
  import sys 

  Iee = np.zeros(1)
  Iem = np.zeros(1)
  Imm = np.zeros(1)

  AEe = np.zeros(1)
  AEm = np.zeros(1)
  AMe = np.zeros(1)
  AMm = np.zeros(1)

  dIee = np.zeros(1)
  dIem = np.zeros(1)
  dImm = np.zeros(1)

  dAEe = np.zeros(1)
  dAEm = np.zeros(1)
  dAMe = np.zeros(1)
  dAMm = np.zeros(1)


  IeemSFOS = np.zeros(1)
  ImmeSFOS = np.zeros(1)
  AeemSFOS = np.zeros(1)
  AmmeSFOS = np.zeros(1)

  dIeemSFOS = np.zeros(1)
  dImmeSFOS = np.zeros(1)
  dAeemSFOS = np.zeros(1)
  dAmmeSFOS = np.zeros(1)




  BTagCR = ROOT.TFile("BTagCR.root","open")
  r0SFOS = ROOT.TFile("3l1B0SFOS.root","open")

  # BTagCR = ROOT.TFile("Root/BTag77CR.root","open")
  # r0SFOS = ROOT.TFile("Root/3l1B770SFOS.root","open")


  mcSets=["ttZ","WWW","tZ","ssWW","VVV","ttW","ZZ","WZ","Vgamma","Data","ChargeFlip"]
  #mcSets=["ttZ","WWW","tZ","ssWW","VVV","ttW","ZZ","SingleTop","WZ","Vgamma","Data","ChargeFlip"]
  
  #mcSets=["ttZ","WWW","tZ","ssWW","VVV","ttW","ZZ","SingleTop","WZ","Data"]
  
  for SET in mcSets:
    print SET

    if not "data" in SET.lower():

      nBin = BTagCR.Get("Nominal/QCD_Mjj_"+SET+"_0").GetNbinsX()

      AEe += BTagCR.Get("Nominal/QCD_Mjj_"+SET+"_0").Integral(0,nBin+1) 
      AEm += BTagCR.Get("Nominal/QCD_Mjj_"+SET+"_1").Integral(0,nBin+1) 
      AMe += BTagCR.Get("Nominal/QCD_Mjj_"+SET+"_2").Integral(0,nBin+1) 
      AMm += BTagCR.Get("Nominal/QCD_Mjj_"+SET+"_3").Integral(0,nBin+1) 
      Iee += BTagCR.Get("Nominal/Mjj_"+SET+"_0").Integral(0,nBin+1) 
      Iem += BTagCR.Get("Nominal/Mjj_"+SET+"_1").Integral(0,nBin+1) 
      Iem += BTagCR.Get("Nominal/Mjj_"+SET+"_2").Integral(0,nBin+1) 
      Imm += BTagCR.Get("Nominal/Mjj_"+SET+"_3").Integral(0,nBin+1) 
      

      AeemSFOS += r0SFOS.Get("Nominal/QCD_Mjj_"+SET+"_0").Integral(0,nBin+1) 
      AmmeSFOS += r0SFOS.Get("Nominal/QCD_Mjj_"+SET+"_2").Integral(0,nBin+1) 


      IeemSFOS += r0SFOS.Get("Nominal/Mjj_"+SET+"_0").Integral(0,nBin+1) 
      ImmeSFOS += r0SFOS.Get("Nominal/Mjj_"+SET+"_2").Integral(0,nBin+1) 





    else:
      dAEe += BTagCR.Get("Nominal/QCD_Mjj_"+SET+"_0").Integral(0,nBin+1) 
      dAEm += BTagCR.Get("Nominal/QCD_Mjj_"+SET+"_1").Integral(0,nBin+1) 
      dAMe += BTagCR.Get("Nominal/QCD_Mjj_"+SET+"_2").Integral(0,nBin+1) 
      dAMm += BTagCR.Get("Nominal/QCD_Mjj_"+SET+"_3").Integral(0,nBin+1) 
      dIee += BTagCR.Get("Nominal/Mjj_"+SET+"_0").Integral(0,nBin+1) 
      dIem += BTagCR.Get("Nominal/Mjj_"+SET+"_1").Integral(0,nBin+1) 
      dIem += BTagCR.Get("Nominal/Mjj_"+SET+"_2").Integral(0,nBin+1) 
      dImm += BTagCR.Get("Nominal/Mjj_"+SET+"_3").Integral(0,nBin+1) 


      dAeemSFOS += r0SFOS.Get("Nominal/QCD_Mjj_"+SET+"_0").Integral(0,nBin+1) 
      dAmmeSFOS += r0SFOS.Get("Nominal/QCD_Mjj_"+SET+"_2").Integral(0,nBin+1) 
      dIeemSFOS += r0SFOS.Get("Nominal/Mjj_"+SET+"_0").Integral(0,nBin+1) 
      dImmeSFOS += r0SFOS.Get("Nominal/Mjj_"+SET+"_2").Integral(0,nBin+1) 




  doIt("Fakes", "Fakes.txt")


  print dIee,Iee,dIem,Iem,dImm,Imm, dIeemSFOS, IeemSFOS, ImmeSFOS, dImmeSFOS
  print dAEe,dAEm,dAMe,dAMm,AEe,AEm,AMe,AMm

  print dAeemSFOS,AeemSFOS,IeemSFOS,dIeemSFOS
  print dAmmeSFOS,AmmeSFOS,ImmeSFOS,dImmeSFOS
  

print("--- %s TIME ---" % (time.time() - start_time))





#  LocalWords:  SFOS
