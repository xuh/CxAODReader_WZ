import ROOT
from array import array


ROOT.gStyle.SetOptStat(0)

FileName="FakesIDSub_e"

Name = "Electron"

etas = [0.0, 2.5]
pts  = [20., 50., 200] 
ptsTMP  = [20., 50., 200] 
ptsTMP  = [20., 50. ,200]  


# FileName="FakesIDSub_m"
# Name = "Muon"

# etas = [0.0, 2.5]
# pts  = [20.,40.,70.,1000] 
# ptsTMP  = [20.,40.,70.,120]  


InputPlot = ROOT.TFile(FileName+".root","open")
mHist = InputPlot.Get(FileName+"_misid")



colors = [ROOT.kRed,ROOT.kMagenta,ROOT.kBlue,ROOT.kCyan,ROOT.kGreen,ROOT.kYellow+3,ROOT.kBlack]


hPT = [ ROOT.TH1F("HistPT"+str(index),"HistPt"+str(index),len(pts)-1,array('f',ptsTMP)) for index in range(0,len(etas)-1)]

tPT = [ "" for index in range(0,len(etas)-1) ]
hETA =[ ROOT.TH1F("HistEta"+str(index),"HistEta"+str(index),len(etas)-1,array('f',etas)) for index in range(0,len(ptsTMP)-1)]
tETA = [ "" for index in range(0,len(pts)-1) ]

for i in range(0,len(etas)-1):
    for j in range(0,len(pts)-1):

        if i>0 or j>0:
            hPT[i].SetBinContent(j+1,mHist.GetBinContent(i+1,j+1))
            hPT[i].SetBinError(j+1,mHist.GetBinError(i+1,j+1))
        else:
            hPT[i].SetBinContent(j+1,mHist.GetBinContent(i+1,j+1)/2)
            hPT[i].SetBinError(j+1,mHist.GetBinError(i+1,j+1)/2)

        print mHist.GetBinContent(i+1,j+1),mHist.GetBinError(i+1,j+1)


        tPT[i] = str(etas[i])+" GeV < | #eta | < "+str(etas[i+1])+" GeV"

        
        hETA[j].SetBinContent(i+1,mHist.GetBinContent(i+1,j+1))
        hETA[j].SetBinError(i+1,mHist.GetBinError(i+1,j+1))

        tETA[j] = str(pts[j])+" GeV < | p_T | < "+str(pts[j+1])+" GeV"
        


Canvas = ROOT.TCanvas("c","canvas",600,600)
mylegend = ROOT.TLegend(0.59, 0.2, 0.86, 0.5)
mylegend.SetFillColor(0)
mylegend.SetBorderSize(0)

lLumi = ROOT.TLatex()
lLumi.SetNDC(1);
lLumi.SetTextFont(42);


hPT[0].SetTitle("")
hPT[0].GetXaxis().SetTitle("p_{T}")
hPT[0].GetYaxis().SetTitleOffset(1.5)
hPT[0].GetYaxis().SetTitle("#epsilon_{miss-ID}")

hPT[0].SetMinimum(0.00001)
hPT[0].SetMaximum(1)


Canvas.SetLogy()

Canvas.Draw()

for i in range(0,len(etas)-1):
    hPT[i].SetLineColor(colors[i])
    hPT[i].SetMarkerColor(colors[i])
    hPT[i].SetMarkerStyle(20)
    hPT[i].Draw("SAME")

    mylegend.AddEntry(hPT[i],tPT[i])

lLumi.DrawLatex(0.2,0.2,"#font[72]{ATLAS} Internal")
lLumi.DrawLatex(0.2,0.15,Name+" Fakes")
mylegend.Draw() 
Canvas.RedrawAxis()



Canvas.SaveAs("PT-Fakes"+Name+".png")






Canvas = ROOT.TCanvas("c","canvas",600,600)
mylegend = ROOT.TLegend(0.59, 0.2, 0.86, 0.5)
mylegend.SetFillColor(0)
mylegend.SetBorderSize(0)

lLumi = ROOT.TLatex()
lLumi.SetNDC(1);
lLumi.SetTextFont(42);


hETA[0].SetTitle("")
hETA[0].GetXaxis().SetTitle("#eta")
hETA[0].GetYaxis().SetTitleOffset(1.5)
hETA[0].GetYaxis().SetTitle("#epsilon_{miss-ID}")

hETA[0].SetMinimum(0.00001)
hETA[0].SetMaximum(1)


Canvas.SetLogy()

Canvas.Draw()

for i in range(0,len(pts)-1):
    hETA[i].SetLineColor(colors[i])
    hETA[i].SetMarkerColor(colors[i])
    hETA[i].SetMarkerStyle(20)
    hETA[i].Draw("SAME")

    mylegend.AddEntry(hETA[i],tETA[i])

lLumi.DrawLatex(0.2,0.2,"#font[72]{ATLAS} Internal")
lLumi.DrawLatex(0.2,0.15,Name+" Fakes")
mylegend.Draw()
Canvas.RedrawAxis()



Canvas.SaveAs("ETA-Fakes"+Name+".png")

