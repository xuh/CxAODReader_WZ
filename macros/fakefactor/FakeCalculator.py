import sys
import ROOT
import os
from math import sqrt,fabs
from array import array
from WWWPlot import *
from MTree import MTree
import collections
import timeit


import DataSets # All Information about the datasets are stored here




def CFAnalyze(Region="CF CR",Plots={},doQCDnCF=[True,True],MakeEffPlots=True,MakeTables=True,Verbose=True,Blind=False, EventPrint=False):


    #Mendetory Root Settings
    #ROOT.gROOT.Macro('$ROOTCOREDIR/scripts/load_packages.C')
    ROOT.gROOT.SetBatch()
    ROOT.gStyle.SetOptStat(0)

    #Opening Work Files
    Folders = DataSets.DataFolder
#    Folder = "/net/s3_data_home/isiral/WWWCxAOD/submitDirOld/data-MVATree/"


    PrintFile = None
    if EventPrint:
        PrintFile = open("Print.txt","w")

    PlotFolder="CFPlots/"


    etaBin=[0.0, 0.6, 1.1, 1.52, 1.7, 2.3, 2.5]
    ptBin=[20.,27.,60.,90.,130.,1000.]


    RootFolder="CFROOT/"
    if not os.path.exists(RootFolder):
        os.mkdir(RootFolder)


    #Creating Output File
    OutFile = ROOT.TFile(RootFolder+Region.replace(" ","")+".root","create")




    hMll={}
    
    hMll["Side"]=[[],[]]
    hMll["ZW"]=[[],[]]
    for region in ["Side","ZW"]:
        for wReg in range(0,2):
            for i1 in range(0,len(etaBin)-1):
                hMll[region][wReg].append([])
                for j1 in range(0,len(ptBin)-1):
                    hMll[region][wReg][i1].append([])
                    for i2 in range(0,len(etaBin)-1):
                        hMll[region][wReg][i1][j1].append([])
                        for j2 in range(0,len(ptBin)-1):

                            hMll[region][wReg][i1][j1][i2].append(ROOT.TH1F("Mll_"+region+str(wReg)+"_"+str(i1)+str(j1)+str(i2)+str(j2),"Mll "+region+str(wReg)+" "+str(i1)+str(j1)+str(i2)+str(j2),200,50,150))
                            hMll[region][wReg][i1][j1][i2][j2].Sumw2()


    for wReg in [0,1]:
        for SetNumber in ["data15","data16","data17"]:


            SetName = "Data"
            if Verbose:
                print "Working on the MC Set",SetName


            print "Working on folders",Folders
            for Folder in Folders:
                FileList = os.listdir(Folder)


                #Locating the file
                fileName = [sFile for sFile in FileList if str(SetNumber) in sFile]


                #If multiples of the same MC number is found throw error and skip
                #For Data this is not valid
                if (not "data" in str(SetNumber)) and  len(fileName)!=1:
                    print "!Error",SetName,SetNumber,"Cannot be found or has multiples."
                    continue

                #Getting the Lumi correction, normally just returns 1, CxAOD reader handles this
                LumiWeight = DataSets.GetLumiWeight(SetNumber)
                #Makesure Lumi Correction is correct
                if LumiWeight < 0:
                    print "LumiWeight cannot be calculated for run",SetNumber
                    exit

                #Loops over all the files for the given Data/MC sample
                #For MC len(fileName)=1,(only one file per MC sample) so this loop only works on data, for MC it doens't do much.
                for tmpFile in fileName: 

                    #Gets the file and reads it and gets the TTree defined in MTree.py
                    nRoot = Folder + "/"+tmpFile
                    Tree = MTree(nRoot)
                    nentries = Tree.fTree.GetEntries()

                    #Loops over every entry getting the cuts
                    for TreeIndex in range(0,nentries):
                        Tree.fTree.GetEntry(TreeIndex)



                        #MC weight is calculated
                        weight=1.0
                        if not "data" in SetName.lower():
                            weight= Tree.Branches["weight"][0]*LumiWeight


                        #Cutflows per regoin is defined, it's in MTree
                        cuts = Tree.GetCuts(Region)

                        #Runs over the given cutflow
                        passRegion=True

                        Nel=Tree.Branches["Nel"][0]
                        Nmu=Tree.Branches["Nmu"][0]

                        if not (Nel==2 and Nmu==0): continue 
                        Chan=0

                        for tmpKeys in cuts.keys():
                                passRegion = passRegion and cuts[tmpKeys]

                        #Kill the event if not in the "REGION"
                        if not (passRegion): continue 

                        #Couting and filling the signal regions
                        if passRegion:                            


                            tmpSetName=""
                            wReg=0
                            if Tree.IsCF():
                                wReg=1

                            EtaInd=[-1,-1]
                            PtInd=[-1,-1]
                            for index in range(0,len(etaBin)-1):
                                if EtaInd[0]!=-1 and EtaInd[1]!=-1: break

                                if etaBin[index+1]>fabs(Tree.GetObject("Lep1Eta")) and etaBin[index]<=fabs(Tree.GetObject("Lep1Eta")):
                                    EtaInd[0]=index
                                if etaBin[index+1]>fabs(Tree.GetObject("Lep2Eta")) and etaBin[index]<=fabs(Tree.GetObject("Lep2Eta")):
                                    EtaInd[1]=index

                            for index in range(0,len(ptBin)-1):
                                if PtInd[0]!=-1 and PtInd[1]!=-1: break

                                if ptBin[index+1]>Tree.GetObject("Lep1Pt")*1e-3 and ptBin[index]<=Tree.GetObject("Lep1Pt")*1e-3:
                                    PtInd[0]=index
                                if ptBin[index+1]>Tree.GetObject("Lep2Pt")*1e-3 and ptBin[index]<=Tree.GetObject("Lep2Pt")*1e-3:
                                    PtInd[1]=index

                            if PtInd[0]==-1 or PtInd[1]==-1: continue
                            if EtaInd[0]==-1 or EtaInd[1]==-1: continue

                            region=""
                            if Tree.GetObject("Mll")>75.e3 and Tree.GetObject("Mll")<105.e3:
                                region="ZW"
                            elif (Tree.GetObject("Mll")<75.e3 and Tree.GetObject("Mll")>60.e3):
                                region="Side"
                            elif (Tree.GetObject("Mll")<120.e3 and Tree.GetObject("Mll")>105.e3):
                                region="Side"
                            else:
                                continue



                            hMll[region][wReg][EtaInd[0]][PtInd[0]][EtaInd[1]][PtInd[1]].Fill(Tree.GetObject("Mll")*1e-3,weight)


                            #print "Doing fill for",PtInd,EtaInd
    #Saving Eff Histograms
    OutFile.cd()

    #Saving Histograms

    tmpFolder=""
    tmpFolderQCD=""




    if not os.path.exists(PlotFolder):
        os.mkdir(PlotFolder)


    InfoMll={"NBins":20,"xmin":0,"xmax":400,"Correct":1e-3,"XAxis":"M_{#ell#ell}[GeV]","Logy":True,"YScale":100}


    YScale = InfoMll["YScale"] if "YScale" in InfoMll.keys() else 1.8
            #XAxis Name (Exp:  M_{jj} [GeV])
    XAxis= InfoMll["XAxis"] if "XAxis" in InfoMll.keys() else ""
            # Systematic Error (Percentage)
    Syst_Error=0
            #Extra Text in Legend
    tChan= ""
    if wReg==0:
        tChan=", SS"
    else:
        tChan=", OS"
    
    #Make Y axis in Log Scale
    Logy= InfoMll["Logy"] if "Logy" in InfoMll.keys() else False
    #Set custom XRange
    XRange=InfoMll["XRange"] if "XRange" in InfoMll.keys() else []
    # Changes the way ticks are done (Use with NonGeV)
    Multip=InfoMll["Multip"] if "Multip" in InfoMll.keys() else False
    # Changes Y Title to Entries instead of Entries/GeV (For Lepton Number etc)
    NonGeV=InfoMll["NonGeV"] if "NonGeV" in InfoMll.keys() else False
    # Overlaid plot like signal, used for aQGC's
    OverlayPlot=[]
    OverlayName=[]
    # Plot Overflow
    OverFlow=InfoMll["OverFlow"] if "OverFlow" in InfoMll.keys() else True







    for wReg in range(0,2):
        for i1 in range(0,len(etaBin)-1):
            for j1 in range(0,len(ptBin)-1):
                for i2 in range(0,len(etaBin)-1):
                    for j2 in range(0,len(ptBin)-1):




                        for region in ["Side","ZW"]:        

 
                            hMll[region][wReg][i1][j1][i2][j2].Write()
                            #hMll[region][wReg][i][j].SaveAs(tmpFolder+"Mll_"+region+"_"+str(wReg)+"_"+str(i)+str(j)+".png")
                            #Plot = {"Data":hMll[region][wReg][i][j].Clone()}


                            #ExtraText = ["W^{#pm}W^{#pm}W^{#mp}#rightarrow l#nul#nujj",region+" "+str(wReg)+" "+str(i)+str(j)]
                            #Canvas = WWWPlot(Plot,YScale,XAxis,Syst_Error,ExtraText,Logy,XRange,Blind,Multip,NonGeV,OverlayPlot,OverlayName,OverFlow)
                            #Canvas.Write("C_Mll_"+region+"_"+str(wReg)+"_"+str(i)+str(j))
                            #Canvas.SaveAs(tmpFolder+"Mll_"+region+"_"+str(wReg)+"_"+str(i)+str(j)+".png")
                            #Canvas.Close()
                            #del Canvas
                        
                        SideInteg = hMll["Side"][wReg][i1][j1][i2][j2].Integral()
                        BinVal = SideInteg/60
                        BinErr = 0
                        if BinVal>0:
                            BinErr = sqrt(BinVal)
                        hTMP = ROOT.TH1F("Subtract"+str(wReg)+str(i1)+str(j1)+str(i2)+str(j2),"Subtract"+str(wReg)+str(i1)+str(j1)+str(i2)+str(j2),200,50,150)
                        hTMP.Sumw2()
                        for Bin in range(51,111):
                            hTMP.SetBinContent(Bin,BinVal)
                            hTMP.SetBinError(Bin,BinErr)
                        SubedZW = hMll["ZW"][wReg][i1][j1][i2][j2].Clone("Mll_Sub_"+str(wReg)+"_"+str(i1)+str(j1)+str(i2)+str(j2))
                        SubedZW.Add(hTMP,-1)
                        SubedZW.Write("Mll_Sub_"+str(wReg)+"_"+str(i1)+str(j1)+str(i2)+str(j2))
                        #hMll["ZW"][wReg][i][j].SaveAs("Mll_Sub_"+str(wReg)+"_"+str(i)+str(j)+".png")
                        # Plot2 = {"Data":hMll["ZW"][wReg][i][j].Add(hTMP,-1)}

                        # hMll.Write("Mll_Sub_"+str(wReg)+"_"+str(i)+str(j))
                        # Canvas = WWWPlot(Plot2,YScale,XAxis,Syst_Error,ExtraText,Logy,XRange,Blind,Multip,NonGeV,OverlayPlot,OverlayName,OverFlow)
                        # Canvas.Write("C_Mll_Sub_"+"_"+str(wReg)+"_"+str(i)+str(j))
                        # Canvas.SaveAs(tmpFolder+"Mll_Sub_"+"_"+str(wReg)+"_"+str(i)+str(j)+".png")



                    

    OutFile.Close()








if __name__ == "__main__":


    
    Plots={}
    Plots["Lep1Pt"]={"NBins":20,"xmin":0,"xmax":400,"Correct":1e-3,"XAxis":"p_{T}(l_{1})[GeV]"}
    Plots["Lep2Pt"]={"NBins":20,"xmin":0,"xmax":400,"Correct":1e-3,"XAxis":"p_{T}(l_{2})[GeV]"}
    Plots["Mll"]={"NBins":20,"xmin":0,"xmax":400,"Correct":1e-3,"XAxis":"p_{T}(l_{2})[GeV]"}


    CFAnalyze("CF CR",Plots)

#  LocalWords:  WWWAnalyze
