import sys
import ROOT
import os
from math import sqrt
from array import array
from WWWPlot import *
from MTree import MTree
import collections

import DataSets # All Information about the datasets are stored here


def CalcFakes( Verbose=False, MakeTables=True, Plots={}):

    Region="0SFOS"
    #Region="Loose SR"

    #Mendetory Root Settings
    ROOT.gROOT.Macro('$ROOTCOREDIR/scripts/load_packages.C')
    ROOT.gROOT.SetBatch()
    ROOT.gStyle.SetOptStat(0)

    #Opening Work Files
    Folders = DataSets.DataFolder

    #Creating Output File
    OutFile = ROOT.TFile("Fakes.root","recreate")


    #ptBin=[15,25,35,70,400]
    #ptBin=[15,25,27,30,35,50,150]
    #ptBin=[15,25,27,30,50,150]
    etaBin=[0.,2.5]
    #etaBin=[0.,1.37,2.5]
    ptBin=[20,50,100]
    #ptBin=[25,27,30,35,40,45,50,55,60,65,70,75,80,85,90,95,100,105,110,115,120,125,130,135,140,145,150]

    # ptBin=[0,500]
    # etaBin=[0.,2.5]


    WorkRegion=["ID","Anti-ID"]


    DS=DataSets.FullMCSet.keys()+["Charge Flip L."]


    hPtEta={}
    Histograms={}
    for wReg in WorkRegion:

        Histograms[wReg] = collections.OrderedDict()
        hPtEta[wReg]=[collections.OrderedDict(),collections.OrderedDict(),collections.OrderedDict()] 
        for SetName in DS:
            for chan in range(0,3):
                hPtEta[wReg][chan][SetName] = ROOT.TH2F(SetName.replace(" ","").replace("#","")+"_"+wReg+"_"+str(chan),SetName.replace(" ","").replace("#","")+" "+wReg+" "+str(chan),len(ptBin)-1,array('f',ptBin),len(etaBin)-1,array('f',etaBin))
                hPtEta[wReg][chan][SetName].Sumw2()
        
        for plot in Plots.keys():
            Histograms[wReg][plot]=[collections.OrderedDict(),collections.OrderedDict(),collections.OrderedDict()] 
            for SetName in DS:
                for chan in range(0,3):
                    Histograms[wReg][plot][chan][SetName] = ROOT.TH1F(plot+"_"+SetName.replace(" ","").replace("#","")+"_"+str(chan)+"_"+str(wReg),plot+" "+SetName.replace(" ","").replace("#","")+" "+str(chan)+" "+str(wReg),Plots[plot]["NBins"],Plots[plot]["xmin"],Plots[plot]["xmax"])
                    Histograms[wReg][plot][chan][SetName].Sumw2()
            
            

    #Some Counters Defined
    EventCount={}
    EventRaw={}

    EventCount["Total Sum"]={"ID":[0.0,0.0,0.0],"Anti-ID":[0.0,0.0,0.0]}
    EventRaw["Total Sum"]={"ID":[0.0,0.0,0.0],"Anti-ID":[0.0,0.0,0.0]}
    EventCount["Charge Flip L."]={"ID":[0.0,0.0,0.0],"Anti-ID":[0.0,0.0,0.0]}
    EventRaw["Charge Flip L."]={"ID":[0.0,0.0,0.0],"Anti-ID":[0.0,0.0,0.0]}

  
    #Looping over MC groups
    for SetName,SetNumbers in DataSets.FullMCSet.items():
 
        if Verbose:
            print "Working on the MC Set",SetName

        #Initializing counters
        EventCount[SetName]={"ID":[0.0,0.0,0.0],"Anti-ID":[0.0,0.0,0.0]}
        EventRaw[SetName]={"ID":[0.0,0.0,0.0],"Anti-ID":[0.0,0.0,0.0]}



        print "Working on folders",Folders
        for Folder in Folders:
            FileList = os.listdir(Folder)



            #Looping over MC numbers inside the MC set. 
            for SetNumber in SetNumbers:

                #If blind skip the data


                #Locating the file
                fileName = [sFile for sFile in FileList if str(SetNumber) in sFile]


                #If multiples of the same MC number is found throw error and skip
                #For Data this is not valid
                if (not "data" in str(SetNumber)) and  len(fileName)!=1:
                    print "!Error",SetName,SetNumber,"Cannot be found or has multiples."
                    continue


                #Getting the Lumi correction, normally just returns 1, CxAOD reader handles this
                LumiWeight = DataSets.GetLumiWeight(SetNumber)
                #Makesure Lumi Correction is correct
                if LumiWeight < 0:
                    print "LumiWeight cannot be calculated for run",SetNumber
                    exit

                #Loops over all the files for the given Data/MC sample
                #For MC len(fileName)=1,(only one file per MC sample) so this loop only works on data, for MC it doens't do much.
                for tmpFile in fileName: 

                    #Gets the file and reads it and gets the TTree defined in MTree.py
                    nRoot = Folder + "/"+tmpFile
                    Tree = MTree(nRoot)
                    nentries = Tree.fTree.GetEntries()

                    #Starting doing cuts
                    Count = [0.0,0.0,0.0]
                    RawCount =[0.0,0.0,0.0]


                    #Loops over every entry getting the cuts
                    for TreeIndex in range(0,nentries):
                        Tree.fTree.GetEntry(TreeIndex)

                        #MC weight is calculated
                        weight=1.0
                        if not "data" in SetName.lower():
                            weight= Tree.Branches["weight"][0]

                        if not "data" in SetName.lower():
                            weight= weight*LumiWeight

                        #print weight
                        #Cutflows per regoin is defined, it's in MTree
                        cuts = Tree.GetCuts(Region)

                        #Runs over the given cutflow
                        passRegion=True

                        Nel=Tree.Branches["Nel"][0]
                        Nmu=Tree.Branches["Nmu"][0]


                        Chan=0
                        if Nel==2 and Nmu==1:
                            Chan=0
                        elif Nel==1 and Nmu==2:
                            Chan=1

                        #Fills the efficiency plot



                        for tmpKeys in cuts.keys():
                                passRegion = passRegion and cuts[tmpKeys]

                        #Kill the event if not in the "REGION"
                        if not (passRegion): continue 

                        #Couting and filling the signal regions
                        if passRegion:



                            wReg="ID"
                            if Tree.IsQCDTT():
                                wReg="Anti-ID"

                            tmpSetName=""


                            Count[Chan] += weight
                            RawCount[Chan] += 1
                            tmpSetName = SetName




                            if tmpSetName=="": continue



                            EventCount[tmpSetName][wReg][Chan]+=weight;
                            EventRaw[tmpSetName][wReg][Chan]+=1.0;
                            if not  "data" in tmpSetName.lower(): 
                                EventCount["Total Sum"][wReg][Chan]+=weight;
                                EventRaw["Total Sum"][wReg][Chan]+=1.0;

                            hPtEta[wReg][Chan][tmpSetName].Fill(Tree.GetObject("Lep1Pt")*1e-3,abs(Tree.GetObject("Lep1Eta")),weight)
                            for key in Histograms[wReg].keys():
                                Histograms[wReg][key][Chan][tmpSetName].Fill(Tree.GetObject(key)*Plots[key]["Correct"],weight)


                    #Debug verbose stream per sample
                    if Verbose:
                        print "SetNumber:",SetNumber,"Count:",Count,"Raw Count:",RawCount
                        print "LumiWeight:",LumiWeight
                        print 






    TotalBG={"ID":[None,None,None],"Anti-ID":[None,None,None]}
    TotalData={"ID":[None,None,None],"Anti-ID":[None,None,None]}

    for wReg in WorkRegion:
        for chan in range(0,3):


            for key in Histograms[wReg].keys():
                Histos=Histograms[wReg][key][chan]
                #YScale
                YScale = Plots[key]["YScale"] if "YScale" in Plots[key].keys() else 1.8
                #XAxis Name (Exp:  M_{jj} [GeV])
                XAxis= Plots[key]["XAxis"] if "XAxis" in Plots[key].keys() else ""
                # Systematic Error (Percentage)
                Syst_Error=0
                #Extra Text in Legend
                tChan= ""
                if chan==0:
                    tChan=", e"
                elif chan==1:
                    tChan=", #mu"

                ExtraText = ["Fakes Determination",Region+tChan]
                #Make Y axis in Log Scale
                Logy= Plots[key]["Logy"] if "Logy" in Plots[key].keys() else False
                #Set custom XRange
                XRange=Plots[key]["XRange"] if "XRange" in Plots[key].keys() else []
                # Changes the way ticks are done (Use with NonGeV)
                Multip=Plots[key]["Multip"] if "Multip" in Plots[key].keys() else False
                # Changes Y Title to Entries instead of Entries/GeV (For Lepton Number etc)
                NonGeV=Plots[key]["NonGeV"] if "NonGeV" in Plots[key].keys() else False
                # Overlaid plot like signal, used for aQGC's
                OverlayPlot=[]
                OverlayName=[]
                # Plot Overflow
                OverFlow=Plots[key]["OverFlow"] if "OverFlow" in Plots[key].keys() else True
                Blind=False
                Canvas = WWWPlot(Histos,YScale,XAxis,Syst_Error,ExtraText,Logy,XRange,Blind,Multip,NonGeV,OverlayPlot,OverlayName,OverFlow)
                Canvas.Write(key.replace(" ","").replace("#","")+str(chan)+" "+wReg)
                Canvas.SaveAs(Region.replace(" ","")+"_"+key.replace(" ","").replace("#","")+str(chan)+"_"+wReg+".png")
                Canvas.Close()
                del Canvas







            for SetName in DS:
                hPtEta[wReg][chan][SetName].Write() 

                if not "data" in SetName.lower():
                    if TotalBG[wReg][chan] is None:
                        TotalBG[wReg][chan]=hPtEta[wReg][chan][SetName].Clone()
                        TotalBG[wReg][chan].Sumw2()
                    else:
                        TotalBG[wReg][chan].Add(hPtEta[wReg][chan][SetName])
                else:
                    if TotalData[wReg][chan] is None:
                        TotalData[wReg][chan]=hPtEta[wReg][chan][SetName].Clone()
                        TotalData[wReg][chan].Sumw2()
                    else:
                        TotalData[wReg][chan].Add(hPtEta[wReg][chan][SetName])
                    
            TotalData[wReg][chan].Add(TotalBG[wReg][chan],-1)



    TotalData["ID"][0].Write("IDSub_e")
    TotalData["Anti-ID"][0].Write("AIDSub_e")
    TotalData["ID"][1].Write("IDSub_m")
    TotalData["Anti-ID"][1].Write("AIDSub_m")

    TotalData["ID"][0].Divide(TotalData["Anti-ID"][0])
    TotalData["ID"][1].Divide(TotalData["Anti-ID"][1])


    TotalData["ID"][0].Write("RatioID_e")
    TotalData["ID"][1].Write("RatioID_m")



    OutFile.cd()


    OutFile.Close()



    for wReg in WorkRegion:
        print
        print "Region",wReg
        LineData=""
        LineSum=""
        for key in EventCount:

            Error = 0.0
            line = key
            for Chan in range(0,3):
                if EventRaw[key][wReg][Chan]>0:
                    Error = 1/sqrt(EventRaw[key][wReg][Chan])
                line = line + " , "+str(round(EventCount[key][wReg][Chan],2))


            if "data" in key.lower(): 
                LineData=line
            elif "Total Sum"==key:
                LineSum=line
            else:
                print line
        print LineSum

        print LineData














if __name__ == "__main__":
    Plots={}
    Plots["Lep1Pt"]={"NBins":20,"xmin":0,"xmax":200,"Correct":1e-3,"XAxis":"p_{T}(l_{1})[GeV]","Logy":True,"YScale":5000}
    Plots["Jet1Pt"]={"NBins":20,"xmin":0,"xmax":200,"Correct":1e-3,"XAxis":"p_{T}(j_{1})[GeV]","Logy":True, "YScale":5000}
    Plots["Lep1Eta"]={"NBins":60,"xmin":-3,"xmax":3,"Correct":1,"XAxis":"#eta(l1)[GeV]","Logy":True,"YScale":5000}
    Plots["Jet1Eta"]={"NBins":60,"xmin":-3,"xmax":3,"Correct":1,"XAxis":"#eta(j1)[GeV]","Logy":True,"YScale":5000}
    Plots["met"]={"NBins":10,"xmin":0,"xmax":100,"Correct":1e-3,"XAxis":"E_{T}^{miss}[GeV]","Logy":True, "YScale":5000}
    Plots["Mll"]={"NBins":20,"xmin":0,"xmax":200,"Correct":1e-3,"XAxis":"E_{T}^{miss}[GeV]","Logy":True, "YScale":5000}
    Plots["MT"]={"NBins":10,"xmin":0,"xmax":100,"Correct":1e-3,"XAxis":"M_{T}[GeV]","Logy":True, "YScale":5000}
    Plots["MTMET"]={"NBins":20,"xmin":0,"xmax":200,"Correct":1e-3,"XAxis":"M_{T}+E_{T}^{miss}[GeV]","Logy":True, "YScale":5000}
    Plots["Njets"]={"NBins":10,"xmin":0,"xmax":10,"Correct":1,"XAxis":"Njets","Logy":True, "YScale":5000}
    Plots["Njets"]={"NBins":10,"xmin":0,"xmax":10,"Correct":1,"XAxis":"Njets","Logy":True, "YScale":5000}
    Plots["Nbjets"]={"NBins":10,"xmin":0,"xmax":10,"Correct":1,"XAxis":"Nbjets","Logy":True, "YScale":5000}

    Plots["LepJetEta"]={"NBins":15,"xmin":0,"xmax":6,"Correct":1,"XAxis":"d lJ","Logy":True, "YScale":5000}
    Plots["LepJetPhi"]={"NBins":15,"xmin":0,"xmax":6,"Correct":1,"XAxis":"d lJ","Logy":True, "YScale":5000}

    CalcFakes(False,True,Plots)
