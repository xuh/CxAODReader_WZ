import sys
import ROOT
import os
from math import sqrt,log,fabs
from array import array
from WWWPlot import *
from MTree import MTree
import collections



import DataSets # All Information about the datasets are stored here




def WWWOpt(Region="WWW SR"):


    #Mandetory Root Settings
    ROOT.gROOT.Macro('$ROOTCOREDIR/scripts/load_packages.C')
    ROOT.gROOT.SetBatch()
    ROOT.gStyle.SetOptStat(0)

    #Opening Work Files
    Folder = DataSets.DataFolder

    OutFile = ROOT.TFile("Opt_"+Region.replace(" ","")+".root","recreate")


    print "Working on folder",Folder
    FileList = os.listdir(Folder)
    # Discriminant Plot
    Plots={"NBins":16,"xmin":15,"xmax":335,"Correct":1e-3,"XAxis":"M_{jj}[GeV]"}
    DiscObj="Mjj"
    Optimize={}
    #Optimize["JJDEta"]={"xmin":0,"xmax":4,"NBins":40,"Region":"Lower"}
    # Optimize["Mjj"]={"xmin":0.e3,"xmax":400.e3,"NBins":40,"Region":"Lower"}
    #Optimize["Mlv"]={"xmin":0.e3,"xmax":400.e3,"NBins":40,"Region":"Upper"}
    # Optimize["WWPt"]={"xmin":0.e3,"xmax":500.e3,"NBins":50,"Region":"Upper"}
    #Optimize["DPhiMLL"]={"xmin":0,"xmax":3.1,"NBins":31,"Region":"Upper"}
    #Opttimize["METBalance"]={"xmin":0,"xmax":1,"NBins":20,"Region":"Upper"}
    #Optimize["met"]={"xmin":0,"xmax":400.e3,"NBins":40,"Region":"Upper"}
    Optimize["met"]={"xmin":0.e3,"xmax":400.e3,"NBins":40,"Region":"Upper"}
    # Optimize["SumPt"]={"xmin":0.e3,"xmax":400.e3,"NBins":40,"Region":"Lower"}
    # Optimize["Jet2Pt"]={"xmin":0.e3,"xmax":400.e3,"NBins":40,"Region":"Lower"}
    # Optimize["LLDR"]={"xmin":0,"xmax":5,"NBins":20,"Region":"Lower"}
    # Optimize["MaxLEta"]={"xmin":0,"xmax":3,"NBins":30,"Region":"Lower"}
    # Optimize["PtBalanceNoJ"]={"xmin":0,"xmax":2,"NBins":20,"Region":"Lower"}
    # Optimize["PtBalance"]={"xmin":0,"xmax":1,"NBins":20,"Region":"Upper"}
    # Optimize["Njets"]={"xmin":2,"xmax":10,"NBins":8,"Region":"Lower"}


    
    SigmaPlots={}
    for OptVar in Optimize.keys():


        SigmaPlots[OptVar]=[]
        
        for chan in range(0,3):
            SigmaPlots[OptVar].append(ROOT.TH1F(OptVar+"_"+Optimize[OptVar]["Region"]+"_"+str(chan),OptVar+"_"+str(chan),Optimize[OptVar]["NBins"],Optimize[OptVar]["xmin"],Optimize[OptVar]["xmin"]))

        
        SStep = fabs(Optimize[OptVar]["xmax"]-Optimize[OptVar]["xmin"])/Optimize[OptVar]["NBins"]
        
        RANGE = [ Optimize[OptVar]["xmin"]+Val*SStep for Val in range(0,Optimize[OptVar]["NBins"])] 




        for VStep in RANGE:

            Histograms = [collections.OrderedDict(),collections.OrderedDict(),collections.OrderedDict()] 

            for SetName in DataSets.FullMCSet.keys():
                for chan in range(0,3):
                    Histograms[chan][SetName] = ROOT.TH1F(SetName.replace(" ","").replace("#","")+"_"+str(chan),SetName.replace(" ","").replace("#","")+" "+str(chan),Plots["NBins"],Plots["xmin"],Plots["xmax"])



            #Looping over MC groups
            for SetName,SetNumbers in DataSets.FullMCSet.items():


                #Looping over MC numbers inside the MC set. 
                for SetNumber in SetNumbers:

                    #If blind skip the data
                    if "data" in str(SetNumber).lower():
                        continue


                    #Locating the file
                    fileName = [sFile for sFile in FileList if str(SetNumber) in sFile]


                    #If multiples of the same MC number is found throw error and skip
                    #For Data this is not valid
                    if (not "data" in str(SetNumber)) and  len(fileName)!=1:
                        print "!Error",SetName,SetNumber,"Cannot be found or has multiples."
                        continue


                    #Getting the Lumi correction, normally just returns 1, CxAOD reader handles this
                    LumiWeight = DataSets.GetLumiWeight(SetNumber)
                    #Makesure Lumi Correction is correct
                    if LumiWeight < 0:
                        print "LumiWeight cannot be calculated for run",SetNumber
                        exit

                    #Loops over all the files for the given Data/MC sample
                    #For MC len(fileName)=1,(only one file per MC sample) so this loop only works on data, for MC it doens't do much.
                    for tmpFile in fileName: 

                        #Gets the file and reads it and gets the TTree defined in MTree.py
                        nRoot = Folder + "/"+tmpFile
                        Tree = MTree(nRoot)
                        nentries = Tree.fTree.GetEntries()

                        #Loops over every entry getting the cuts
                        for TreeIndex in range(0,nentries):
                            Tree.fTree.GetEntry(TreeIndex)

                            #MC weight is calculated
                            weight=1.0
                            if not "data" in SetName.lower():
                                weight= Tree.Branches["weight"][0]*LumiWeight

                            #Cutflows per regoin is defined, it's in MTree
                            cuts = Tree.GetCuts(Region)

                            #Runs over the given cutflow
                            passRegion=True
                            for tmpKeys in cuts.keys():
                                passRegion = passRegion and cuts[tmpKeys]


                            #Kill the event if not in the "REGION"
                            if not (passRegion): continue 

                            #Couting and filling the signal regions
                            if passRegion:
                                Nel=Tree.Branches["Nel"][0]
                                Nmu=Tree.Branches["Nmu"][0]


                                Chan=0
                                if Nel==2 and Nmu==0:
                                    Chan=0
                                elif Nel==1 and Nmu==1:
                                    Chan=1

                                elif Nel==0 and Nmu==2:
                                    Chan=2

                                if (Optimize[OptVar]["Region"]=="Upper" and Tree.GetObject(OptVar) >= VStep) or (Optimize[OptVar]["Region"]=="Lower" and Tree.GetObject(OptVar) <= VStep) :
                                    Histograms[Chan][SetName].Fill(Tree.GetObject(DiscObj)*Plots["Correct"],weight)


            Sigma=[0,0,0]
            TotalBG=[0,0,0]
            TotalSig=[0,0,0]

            for Chan in range(0,3):
                #print "Channel",Chan
                for bin in range(0,Histograms[Chan]["WWW"].GetNbinsX()+2):
                    BG=0
                    Sig=0
                    BinVal=0
                    for SetName in DataSets.FullMCSet.keys():
                        if SetName=="WWW":
                            Sig+=Histograms[Chan][SetName].GetBinContent(bin)
                            BinVal=Histograms[Chan][SetName].GetBinCenter(bin)
                        else:
                            BG +=Histograms[Chan][SetName].GetBinContent(bin)
                    TotalBG[Chan] += BG
                    TotalSig[Chan]+= Sig
                    if BG>0:
                        Sigma[Chan]+= 2*(Sig+BG)*log(Sig/BG+1)-2*Sig
                    #print "Bin",BinVal,"Signal",Sig,"BG",BG,"Sigma",Sigma[Chan]
            del Histograms
            SigmaTotal=0
            for index in range(0,3):
                SigmaTotal+=Sigma[index]
                Sigma[index]=sqrt(Sigma[index])
                SigmaPlots[OptVar][index].Fill(VStep,Sigma[index])
            print "VStep",VStep,"Sigma",Sigma,sqrt(SigmaTotal)
            print "Total Sig",TotalSig,"Total BG",TotalBG

        for index in range(0,3):
            SigmaPlots[OptVar][index].Write()
    OutFile.Close()


if __name__ == "__main__":
    WWWOpt("WWW SR")
