#export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase

RunFolder=`pwd`
Framework='/atlas/data19/xuwenhao/WWWFramework_master_VHbb_1/build'
source /usr/local/bin/setup/cvmfs_atlas.sh
cd $Framework
asetup --restore
source */setup.sh
cd $RunFolder

Region=$1
SampleName=$2
SampleNum=$3
Folder=$4
SaveFolder=$5
Plots=$6 #"{'Mjj':{'NBins':20,'xmin':20,'xmax':320,'Correct':1e-3,'XAxis':'M_{jj}[GeV]'}}"
doQCD=$7 #"True"
doCF=$8 #"True"
EffPlots=$9 #"True"
Tables=${10} #"True"
Verbose=${11}  #"True"
Blind=${12} #"False"
EventPrint=${13} #"False"
Mode=${14} #"False"
Variation=${15} #"False"

echo ${13} ${14}

export Command="import AnalyzeSet; AnalyzeSet.RunSet('"$Region"', '"$SampleName"','"$SampleNum"','"$Folder"','"$SaveFolder"',"$Plots",[$doQCD,$doCF],$EffPlots,$Tables,$Verbose,$Blind,$EventPrint,'$Mode',$Variation);"
echo "$Command"
python -c "${Command}"

if [ $? -eq 0 ]; then
    echo "$Folder $SampleName $SampleNum" > $RunFolder/condor_logs/$SampleNum.Success

else
    echo "$Folder $SampleName $SampleNum" > $RunFolder/condor_logs/$SampleNum.Fail
fi
