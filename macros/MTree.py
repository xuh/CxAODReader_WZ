#This tools aim is to manage trees
import ROOT
import os
from array import array
import collections
from math import sqrt,exp,fabs


ReComputeFakes=False
FitFakes=True
if ReComputeFakes:
  FakeFile = ROOT.TFile("Fakes.root")
  hFFe = FakeFile.Get("RatioID_e");
  hFFm = FakeFile.Get("RatioID_m");



class MTree:
  fTree = None
  IntMu = array( 'f',[0])
  IntJet = array( 'f',[0])
  BranchTMVA = [
    "Nmu",
    "Njets",
    "Mll",
    "Mjj",
    "MWWW",
    "met",
    "metPhi",
    "Lep1E",
    "Lep2E",
    "Jet1E",
    "Jet2E",
    "Lep1Pt",
    "Lep1Phi",
    "Lep1Eta",
    "Lep2Pt",
    "Lep2Phi",
    "Lep2Eta",
    "Jet1Pt",
    "Jet1Phi",
    "Jet1Eta",
    "Jet1nrMuon",
    "Jet1nrEl",
    "Jet2nrMuon",
    "Jet2nrEl",
    "Jet2Pt",
    "Jet2Phi",
    "Jet2Eta",
    "DEtajj",
    "SumEta",
    "SumPt",
    "DRjj",
    "DEtajj",
    "DRll",
    "Lep1PtCone20",
    "Lep1TCone30",
    "Lep1TCone20",
    "Lep2PtCone20",
    "Lep2TCone30",
    "Lep2TCone20"
    ]

  Branches = {
    "weight": array( 'f', [ 0 ] ),
    "weightMC": array( 'f', [ 0 ] ),
    "weightJVT": array( 'f', [ 0 ] ),
    "weightBtag": array( 'f', [ 0 ] ),
    "weightEffSF": array( 'f', [ 0 ] ),
    "weightEffReco": array( 'f', [ 0 ] ),
    "weightIso": array( 'f', [ 0 ] ),
    "weightTTVA": array( 'f', [ 0 ] ),
    "weightTrig": array( 'f', [ 0 ] ),

    "passTrig": array( 'B', [ 0 ] ),
    # "passAuthor": array( 'B', [ 0 ] ),
    "passTrigMatch": array( 'B', [ 0 ] ),
	#"isNotBadJet": array( 'B', [ 0 ] ),
    "passSameSign": array( 'B', [ 0 ] ),
    "pass1ZLepton": array( 'B', [ 0 ] ),
    "pass1WLepton": array( 'B', [ 0 ] ),
    "pass2ZLepton": array( 'B', [ 0 ] ),
    "pass1Lepton": array( 'B', [ 0 ] ),
    "pass2Lepton": array( 'B', [ 0 ] ),
    "pass3ZLepton": array( 'B', [ 0 ] ),
    "pass3Lepton": array( 'B', [ 0 ] ),
    "pass4Lepton": array( 'B', [ 0 ] ),
    "passMET": array( 'B', [ 0 ] ),
    "passNJets": array( 'B', [ 0 ] ),
    "passLeptonPt": array( 'B', [ 0 ] ),
    "passJetPt": array( 'B', [ 0 ] ),
    "passJetEta": array( 'B', [ 0 ] ),
    "passMjj": array( 'B', [ 0 ] ),
    "passWSide": array( 'B', [ 0 ] ),
    "passDEtaJJ": array( 'B', [ 0 ] ),
    "passMll": array( 'B', [ 0 ] ),
    "passZVeto": array( 'B', [ 0 ] ),
    "passNBtag": array( 'B', [ 0 ] ),
    "passWWWSR": array( 'B', [ 0 ] ),
    "passWWWSRorigin": array( 'B', [ 0 ] ),
    "passSideBandCR": array( 'B', [ 0 ] ),
    "passSideBandCRorigin": array( 'B', [ 0 ] ),
    "passBtagCR": array( 'B', [ 0 ] ),
    "passl2jetCR": array( 'B', [ 0 ] ),
    "pass3lCR": array( 'B', [ 0 ] ),
    "pass3lVgamma": array( 'B', [ 0 ] ),
    "passZWindowCR": array( 'B', [ 0 ] ),
    "passJetLepOverlap": array( 'B', [ 0 ] ),
    "passCFCR": array( 'B', [ 0 ] ),

    "AllJetLepOverlap": array( 'i',[ 0 ]),


    "pass3lSR": array( 'B', [ 0 ] ),
    "pass3lBtag": array( 'B', [ 0 ] ),
    "pass3lTT": array( 'B', [ 0 ] ),
    "pass0SFOS": array( 'B', [ 0 ] ),
    "pass1SFOS": array( 'B', [ 0 ] ),

    "Lep1Flav": array( 'i',[ 0 ]),
    "Lep2Flav": array( 'i',[ 0 ]),
    "Lep3Flav": array( 'i',[ 0 ]),

    "TotalQ": array( 'i',[ 0 ]),
    "Lep1Q": array( 'i',[ 0 ]),
    "Lep2Q": array( 'i',[ 0 ]),
    "Lep3Q": array( 'i',[ 0 ]),


    "Nloose": array( 'i',[ 0 ]),
    "NNoBLel": array( 'i',[ 0 ]),
    "Nlooseel": array( 'i',[ 0 ]),
    "Nloosemu": array( 'i',[ 0 ]),
    "Nel": array( 'i',[ 0 ]),
    "Nmu": array( 'i',[ 0 ]),
    "Njets": array( 'i',[ 0 ]),
    "Nbjets": array( 'i',[ 0 ]),
    "Nbjets77": array( 'i',[ 0 ]),

    
    "Nleptons": array( 'i',[ 0 ]),
    "Mll": array( 'f',[ 0 ]),
    "Mllee": array( 'f',[ 0 ]),
    "MllSF": array( 'f',[ 0 ]),
    "Mlll": array( 'f',[ 0 ]),
    "mllSFOS": array( 'f',[ 0 ]),
    "Mll_forwardelectron": array( 'f',[ 0 ]),
    "Mjj": array( 'f',[ 0 ]),
    # "MWWW": array( 'f',[ 0 ]),
    "met": array( 'f',[ 0 ]),
    "vetoMuonPt": array( 'f',[ 0 ]),
    "vetoElectronPt": array( 'f',[ 0 ]),
    "Lep1PromptWeight": array( 'f',[ 0 ]),
    "Lep2PromptWeight": array( 'f',[ 0 ]),
    "LepPLVEl": array( 'f',[ 0 ]),
    "LepPLVMu": array( 'f',[ 0 ]),
    "Lep1innerTrackPt": array ( 'f', [0]),
    "Lep2innerTrackPt": array ( 'f', [0]),
    "Lep3innerTrackPt": array ( 'f', [0]),
    "Lep1ECIDSBDT": array( 'f',[ 0 ]),
    "Lep2ECIDSBDT": array( 'f',[ 0 ]),
    "LepECIDSBDT": array( 'f',[ 0 ]),
    "forwardElPt": array( 'f',[ 0 ]),
    "forwardElEta": array( 'f',[ 0 ]),
    "LepAddAmbiguity": array('i', [0]),
    "metPhi": array( 'f',[ 0 ]),
    "Lep1E": array( 'f',[ 0 ]),
    "Lep2E": array( 'f',[ 0 ]),
    "Lep3E": array( 'f',[ 0 ]),
    "LooseElE": array( 'f',[ 0 ]),
    "LooseMuE": array( 'f',[ 0 ]),
    "Jet1E": array( 'f',[ 0 ]),
    "Jet2E": array( 'f',[ 0 ]),
    "Lep1Pt": array( 'f',[ 0 ]),
    "Lep2Pt": array( 'f',[ 0 ]),
    "Lep3Pt": array( 'f',[ 0 ]),
    "LepOFPt": array( 'f',[ 0 ]),
    "LooseElPt": array( 'f',[ 0 ]),
    "LooseMuPt": array( 'f',[ 0 ]),
    "Jet1Pt": array( 'f',[ 0 ]),
    "Jet2Pt": array( 'f',[ 0 ]),
    "Lep1Phi": array( 'f',[ 0 ]),
    "Lep2Phi": array( 'f',[ 0 ]),
    "Lep3Phi": array( 'f',[ 0 ]),
    "LooseElPhi": array( 'f',[ 0 ]),
    "LooseMuPhi": array( 'f',[ 0 ]),
    "Jet1Phi": array( 'f',[ 0 ]),
    "Jet2Phi": array( 'f',[ 0 ]),
    "Lep1Eta": array( 'f',[ 0 ]),
    "Lep2Eta": array( 'f',[ 0 ]),
    "Lep3Eta": array( 'f',[ 0 ]),
    "LepOFEta": array( 'f',[ 0 ]),
    "LooseElEta": array( 'f',[ 0 ]),
    "LooseMuEta": array( 'f',[ 0 ]),
    "Jet1Eta": array( 'f',[ 0 ]),
    "Jet2Eta": array( 'f',[ 0 ]),
    "SumEta": array( 'f',[ 0 ]),
    "SumPt": array( 'f',[ 0 ]),
    "DRjj": array( 'f',[ 0 ]),
    "DRll": array( 'f',[ 0 ]),
    "QCDweight": array( 'f',[ 0 ]),
    "CFweight": array( 'f',[ 0 ]),
    "CFwBG": array( 'f',[ 0 ]),
    "CFStatSys": array( 'f',[ 0 ]),
    #"CFStatSys": array( 'f',[ 0 ]),
    # "MT": array( 'f',[ 0 ]),
    "DEtajj": array( 'f',[ 0 ]),
    "eventNumber" : array( 'i', [ 0 ] ),
    "runNumber" : array( 'i', [ 0 ] ),
    "Lep1isLoose": array( 'B', [ 0 ] ),
    "Lep2isLoose": array( 'B', [ 0 ] ),
    "Lep3isLoose": array( 'B', [ 0 ] ),







    }

  #Leptons = ROOT.vector('WWWParticle')()


  def __init__(self,FileName,Variation="Nominal"):
    #Opening the TTRee
    # If not file but folder
    if os.path.isdir(FileName):
      SampleList = os.listdir(FileName)
      self.fTree = ROOT.TChain(Variation)
      for Sample in SampleList:
        self.fTree.Add(FileName+"/"+Sample)
        
    else:
      self.fTree = ROOT.TChain(Variation)
      self.fTree.Add(FileName)


    for key in self.Branches.keys():
      self.fTree.SetBranchAddress(key,self.Branches[key])

  # # fTree.SetBranchAddress("Leptons",Leptons)   
    # self.TMVAWeight =  ROOT.TMVA.Reader("Silent")
    
    # for key in self.BranchTMVA:
    #     if key=="Nmu":
    #         self.IntMu[0]=float(self.Branches["Nmu"][0])
    #         self.TMVAWeight.AddVariable("Nmu",self.IntMu)
    #     elif key=="Njets":
    #         self.IntJet[0]=float(self.Branches["Njets"][0])
    #         self.TMVAWeight.AddVariable("Nmu",self.IntJet)
      
    #     else:
    #         self.TMVAWeight.AddVariable(key,self.Branches[key])


    
    # self.TMVAWeight.BookMVA("varcat","/net/s3_datad/isiral/TMVA/test/weights/TMVAClassification_BoDT.weights.xml")


  # def Delete(self):
  #     self.fTree.Delete()
  
  def IsQCD(self):
    return (self.Branches["Nlooseel"][0]+self.Branches["Nloosemu"][0])==1 and self.Branches["NNoBLel"][0]==0

  def IsNoBL(self):
    return (self.Branches["Nlooseel"][0]+self.Branches["Nloosemu"][0])==0 and self.Branches["NNoBLel"][0]==1



  def IsQCDTT(self):
    return ( (self.Branches["Nlooseel"][0]+self.Branches["Nloosemu"][0])==1 ) and ( ( self.Branches["Nmu"][0]==2  and  self.Branches["Nloosemu"][0]==1 ) or ( self.Branches["Nel"][0]==2 and  self.Branches["Nlooseel"][0]==1 ) ) and self.Branches["NNoBLel"][0]==0
  


  def NoBLWeight(self,Syst=""):
    if Syst=="VgammaEEE":
      #return 0.42
      #return 0.22 #PLVVeryTight
      return 0.15 #AddAmbiguity
    elif Syst=="VgammaStat":
      #return 0.27+0.024
      #return 0.17+0.015 #PLVVeryTight
      return 0.122+0.015 #AddAmbiguity
    elif Syst=="WvsZgamma":
      #return 0.27+0.27*0.13
      #return 0.17+0.17*0.13 #PLVVeryTight
      return 0.122+0.122*0.13 #AddAmbiguity
    else:
      #return 0.27
      #return 0.17 #PLVVeryTight
      return 0.122 #AddAmbiguity
  def QCDWeight(self,Syst=""):

    # Or gets new weights from given root file 
    if FitFakes:
      QCDWeight=-1

      if self.Branches["Nlooseel"][0]==1:
        A= self.Branches["LooseElPt"][0]*1e-3
      elif self.Branches["Nloosemu"][0]==1:
        A= self.Branches["LooseMuPt"][0]*1e-3

      #No binning Syst
      if Syst=="Binning_E" and self.Branches["Nlooseel"][0]==1:
          if A>=20 and A<50:
            QCDWeight = 0.0319
          elif A>=50:
            QCDWeight = 0.021

      elif Syst=="Binning_M" and self.Branches["Nloosemu"][0]==1:
          # if A>=20 and A<50:
          #     QCDWeight = 0.026
          # elif A>=50:
          #     QCDWeight = 0.49

          if A>=20 and A<40:
            QCDWeight = 0.024
          elif A>=40:
            QCDWeight = 0.21


      elif Syst=="Stat_E" and self.Branches["Nlooseel"][0]==1:
          #QCDWeight = 0.0246+0.0125
          #QCDWeight = 0.0151+0.0070 #PLVVeryTight
          QCDWeight = 0.0344+0.0106 #AddAmbiguity
  
      elif Syst=="Stat_M" and self.Branches["Nloosemu"][0]==1:
          #QCDWeight = 0.0377+0.0058
          #QCDWeight = 0.0279+0.0027 #PLVVeryTight
          QCDWeight = 0.0117+0.0013 #AddAmbiguity 

      elif Syst=="Btag77_E" and self.Branches["Nlooseel"][0]==1:
          QCDWeight = 0.0278

      elif Syst=="Btag77_M" and self.Branches["Nloosemu"][0]==1:
          QCDWeight = 0.0426

      else:
        #print Syst 
        if self.Branches["Nlooseel"][0]==1:
          #QCDWeight=0.0246
          #QCDWeight = 0.0151 #PLVVeryTight
          QCDWeight = 0.0344 #AddAmbiguity
        elif self.Branches["Nloosemu"][0]==1:
          #QCDWeight = 0.0377
          #QCDWeight = 0.0279 #PLVVeryTight
          QCDWeight = 0.0117 #AddAmbiguity


      if QCDWeight<0:
        QCDWeight=0


 
    elif ReComputeFakes: 
      QCDWeight=-1
      if self.Branches["Nlooseel"][0]==1:
        QCDWeight = hFFe.GetBinContent( hFFe.FindBin( min(self.Branches["LooseElPt"][0]*0.001,99)));
      elif self.Branches["Nloosemu"][0]==1:
        QCDWeight = hFFm.GetBinContent( hFFm.FindBin( min(self.Branches["LooseMuPt"][0]*0.001,49.99)) );


    else:
      QCDWeight=self.Branches["QCDweight"][0]


    if QCDWeight==-1:
      print "QCD Weight is -1, it means there is an error. Exiting"
      exit()


    return QCDWeight



  def IsCF(self):
    #print "Checking CF",self.Branches["passSameSign"][0]
    #if self.Branches["Nleptons"][0]==2:
    return self.Branches["passSameSign"][0]==0


  def IsCF3Lep(self):
    return abs(self.Branches["TotalQ"][0])==1 and self.Branches["pass1SFOS"][0] and self.Branches["Nel"][0]==2
    



  def CFweight(self,Syst="Nominal"):
    # if self.Branches["CFweight"][0]==1:
    #     print "CF Weight is -1, it means there is an error. Exiting"
    #     exit()

    if Syst is "CFStatSys":
      return self.Branches["CFStatSys"][0]
    elif Syst is "CFwBG":
      return self.Branches["CFwBG"][0]
    else:
      return self.Branches["CFweight"][0]




  #Returns the given object. (Either the branch or custom object)
  def GetObject(self,ObjectName="Mjj"):
    #Custom Objects: (They are much slower)
    #Example object of MWWW without MET

    if ObjectName=="Lep1PtRatio":
      return self.Branches["Lep1innerTrackPt"][0]/self.Branches["Lep1Pt"][0]
    if ObjectName=="Lep2PtRatio":
      return self.Branches["Lep2innerTrackPt"][0]/self.Branches["Lep2Pt"][0]
    if ObjectName=="Lep3PtRatio":
      return self.Branches["Lep3innerTrackPt"][0]/self.Branches["Lep3Pt"][0]
    
    if ObjectName=="Lep1EndCapPt":
      if abs(self.Branches["Lep1Eta"][0]) >= 1.52:
        return self.Branches["Lep1Pt"][0]
      else:
        return 0.

    if ObjectName=="Lep2EndCapPt":
      if abs(self.Branches["Lep2Eta"][0]) >= 1.52:
        return self.Branches["Lep2Pt"][0]
      else:
        return 0.



    if ObjectName=="SumLEta":

      return self.Branches["Lep1Eta"][0]+self.Branches["Lep2Eta"][0]

    elif ObjectName=="MllOS":
      ElecQ=0
      MuonQ=0
      Elec=[]
      Muon=[]

      if self.Branches["Lep1Flav"][0]==1:
        ElecQ+=self.Branches["Lep1Q"][0]
        Elec.append("Lep1")
      elif self.Branches["Lep1Flav"][0]==2:
        MuonQ+=self.Branches["Lep1Q"][0]
        Muon.append("Lep1")

      if self.Branches["Lep2Flav"][0]==1:
        ElecQ+=self.Branches["Lep2Q"][0]
        Elec.append("Lep2")

      elif self.Branches["Lep2Flav"][0]==2:
        MuonQ+=self.Branches["Lep2Q"][0]
        Muon.append("Lep2")

      if self.Branches["Lep3Flav"][0]==1:
        ElecQ+=self.Branches["Lep3Q"][0]
        Elec.append("Lep3")
      elif self.Branches["Lep3Flav"][0]==2:
        MuonQ+=self.Branches["Lep3Q"][0]
        Muon.append("Lep3")


      Lep1=ROOT.TLorentzVector()
      Lep2=ROOT.TLorentzVector()
      if abs(ElecQ)==0 and self.Branches["Nel"][0]==2:
        Lep1.SetPtEtaPhiE(self.Branches[Elec[0]+"Pt"][0],self.Branches[Elec[0]+"Eta"][0],self.Branches[Elec[0]+"Phi"][0],self.Branches[Elec[0]+"E"][0])
        Lep2.SetPtEtaPhiE(self.Branches[Elec[1]+"Pt"][0],self.Branches[Elec[1]+"Eta"][0],self.Branches[Elec[1]+"Phi"][0],self.Branches[Elec[1]+"E"][0])

      elif abs(MuonQ)==0 and self.Branches["Nmu"][0]==2:
        Lep1.SetPtEtaPhiE(self.Branches[Muon[0]+"Pt"][0],self.Branches[Muon[0]+"Eta"][0],self.Branches[Muon[0]+"Phi"][0],self.Branches[Muon[0]+"E"][0])
        Lep2.SetPtEtaPhiE(self.Branches[Muon[1]+"Pt"][0],self.Branches[Muon[1]+"Eta"][0],self.Branches[Muon[1]+"Phi"][0],self.Branches[Muon[1]+"E"][0])
      return (Lep1+Lep2).M()




    # elif ObjectName=="MllSF":
    #     ElecQ=0
    #     MuonQ=0
    #     Elec=[]
    #     Muon=[]

    #     if self.Branches["Lep1Flav"][0]==1:
    #         ElecQ+=1
    #         Elec.append("Lep1")
    #     elif self.Branches["Lep1Flav"][0]==2:
    #         MuonQ+=1
    #         Muon.append("Lep1")

    #     if self.Branches["Lep2Flav"][0]==1:
    #         ElecQ+=1
    #         Elec.append("Lep2")

    #     elif self.Branches["Lep2Flav"][0]==2:
    #         MuonQ+=1
    #         Muon.append("Lep2")

    #     if self.Branches["Lep3Flav"][0]==1:
    #         ElecQ+=1
    #         Elec.append("Lep3")
    #     elif self.Branches["Lep3Flav"][0]==2:
    #         MuonQ+=1
    #         Muon.append("Lep3")


    #     Lep1=ROOT.TLorentzVector()
    #     Lep2=ROOT.TLorentzVector()
    #     if abs(ElecQ)==2 and self.Branches["Nel"][0]==2:
    #         Lep1.SetPtEtaPhiE(self.Branches[Elec[0]+"Pt"][0],self.Branches[Elec[0]+"Eta"][0],self.Branches[Elec[0]+"Phi"][0],self.Branches[Elec[0]+"E"][0])
    #         Lep2.SetPtEtaPhiE(self.Branches[Elec[1]+"Pt"][0],self.Branches[Elec[1]+"Eta"][0],self.Branches[Elec[1]+"Phi"][0],self.Branches[Elec[1]+"E"][0])

    #     elif abs(MuonQ)==2 and self.Branches["Nmu"][0]==2:
    #         Lep1.SetPtEtaPhiE(self.Branches[Muon[0]+"Pt"][0],self.Branches[Muon[0]+"Eta"][0],self.Branches[Muon[0]+"Phi"][0],self.Branches[Muon[0]+"E"][0])
    #         Lep2.SetPtEtaPhiE(self.Branches[Muon[1]+"Pt"][0],self.Branches[Muon[1]+"Eta"][0],self.Branches[Muon[1]+"Phi"][0],self.Branches[Muon[1]+"E"][0])
    #     return (Lep1+Lep2).M()







    # if ObjectName=="Mlll":
    #     Lep1=ROOT.TLorentzVector()
    #     Lep2=ROOT.TLorentzVector()
    #     Lep3=ROOT.TLorentzVector()
    #     Lep1.SetPtEtaPhiE(self.Branches["Lep1Pt"][0],self.Branches["Lep1Eta"][0],self.Branches["Lep1Phi"][0],self.Branches["Lep1E"][0])
    #     Lep2.SetPtEtaPhiE(self.Branches["Lep2Pt"][0],self.Branches["Lep2Eta"][0],self.Branches["Lep2Phi"][0],self.Branches["Lep2E"][0])
    #     Lep3.SetPtEtaPhiE(self.Branches["Lep3Pt"][0],self.Branches["Lep3Eta"][0],self.Branches["Lep3Phi"][0],self.Branches["Lep3E"][0])
    #     return (Lep1+Lep2+Lep3).M()






    if ObjectName=="El1Pt":
      if self.Branches["Lep1Flav"][0]==1:
        return self.Branches["Lep1Pt"][0]
      elif self.Branches["Lep2Flav"][0]==1:
        return self.Branches["Lep2Pt"][0]
      elif self.Branches["Lep3Flav"][0]==1:
        return self.Branches["Lep3Pt"][0]
      else:
        return 0


    if ObjectName=="Mu1Pt":
      if self.Branches["Lep1Flav"][0]==2:
        return self.Branches["Lep1Pt"][0]
      elif self.Branches["Lep2Flav"][0]==2:
        return self.Branches["Lep2Pt"][0]
      elif self.Branches["Lep3Flav"][0]==2:
        return self.Branches["Lep3Pt"][0]
      else:
        return 0





    if ObjectName=="LooseLepPt":

      if self.Branches["Nlooseel"][0]==1:
        return self.GetObject("LooseElPt")
      elif self.Branches["Nloosemu"][0]==1:
        return self.GetObject("LooseMuPt")
      else:
        return 0
    if ObjectName=="Lep1PtCone30dPt":
      if self.Branches["Lep1Pt"][0]>0:
        return self.Branches["Lep1PtCone30"][0]/self.Branches["Lep1Pt"][0]
      else:
        return -99

    if ObjectName=="Lep1TCone30dPt":
      if self.Branches["Lep1Pt"][0]>0:
        return self.Branches["Lep1TCone30"][0]/self.Branches["Lep1Pt"][0]
      else:
        return -99


    if ObjectName=="Mll2":
      return self.Branches["Mll"][0]

    if ObjectName=="MDll":
        return self.Branches["Mll"][0]*self.Branches["DRll"][0]


    if ObjectName=="Lep1PtCone20dPt":
      if self.Branches["Lep1Pt"][0]>0:
        return self.Branches["Lep1PtCone20"][0]/self.Branches["Lep1Pt"][0]
      else:
        return -99

    if ObjectName=="Lep1TCone20dPt":
      if self.Branches["Lep1Pt"][0]>0:
        return self.Branches["Lep1TCone20"][0]/self.Branches["Lep1Pt"][0]
      else:
        return -99


    if ObjectName=="Lep2PtCone30dPt":
      if self.Branches["Lep2Pt"][0]>0:
        return self.Branches["Lep2PtCone30"][0]/self.Branches["Lep2Pt"][0]
      else:
        return -99

    if ObjectName=="Lep2TCone30dPt":
      if self.Branches["Lep2Pt"][0]>0:
        return self.Branches["Lep2TCone30"][0]/self.Branches["Lep2Pt"][0]
      else:
        return -99

    if ObjectName=="Lep2PtCone20dPt":
      if self.Branches["Lep2Pt"][0]>0:
        return self.Branches["Lep2PtCone20"][0]/self.Branches["Lep2Pt"][0]
      else:
        return -99

    if ObjectName=="Lep2TCone20dPt":
      if self.Branches["Lep2Pt"][0]>0:
        return self.Branches["Lep2TCone20"][0]/self.Branches["Lep2Pt"][0]
      else:
        return -99

    if ObjectName=="SFOS1":
      ElecQ=0
      MuonQ=0
      if self.Branches["Lep1Flav"][0]==1:
        ElecQ+=self.Branches["Lep1Q"][0]
      elif self.Branches["Lep1Flav"][0]==2:
        MuonQ+=self.Branches["Lep1Q"][0]

      if self.Branches["Lep2Flav"][0]==1:
        ElecQ+=self.Branches["Lep2Q"][0]
      elif self.Branches["Lep2Flav"][0]==2:
        MuonQ+=self.Branches["Lep2Q"][0]

      if self.Branches["Lep3Flav"][0]==1:
        ElecQ+=self.Branches["Lep3Q"][0]
      elif self.Branches["Lep3Flav"][0]==2:
        MuonQ+=self.Branches["Lep3Q"][0]
    
      return (abs(ElecQ)==0 and abs(MuonQ)==1) or (abs(ElecQ)==1 and abs(MuonQ)==0)





    if ObjectName=="MT2Lep":
      vLep1 = ROOT.TLorentzVector()
      vLep2 = ROOT.TLorentzVector()
      vMET = ROOT.TLorentzVector()

      vLep1.SetPtEtaPhiE(self.Branches["Lep1Pt"][0],self.Branches["Lep1Eta"][0],self.Branches["Lep1Phi"][0],self.Branches["Lep1E"][0])
      vLep2.SetPtEtaPhiE(self.Branches["Lep2Pt"][0],self.Branches["Lep2Eta"][0],self.Branches["Lep2Phi"][0],self.Branches["Lep2E"][0])
      vMET.SetPtEtaPhiM(self.Branches["met"][0],0,self.Branches["metPhi"][0],0)
      
      vLep1=vLep1
      return sqrt(2.*( (vLep1.Pt() * vMET.Pt() + vLep1.Pt() * vLep2.Pt() +vLep2.Pt() * vMET.Pt())-(vLep1.Px() * vMET.Px() + vLep1.Px() * vLep2.Px() +vLep2.Px() * vMET.Px())-(vLep1.Py() * vMET.Py() + vLep1.Py() * vLep2.Py() +vLep2.Py() * vMET.Py())))


    if ObjectName=="MllMET":
      vLep1 = ROOT.TLorentzVector()
      vLep2 = ROOT.TLorentzVector()
      vMET = ROOT.TLorentzVector()

      vLep1.SetPtEtaPhiE(self.Branches["Lep1Pt"][0],self.Branches["Lep1Eta"][0],self.Branches["Lep1Phi"][0],self.Branches["Lep1E"][0])
      vLep2.SetPtEtaPhiE(self.Branches["Lep2Pt"][0],self.Branches["Lep2Eta"][0],self.Branches["Lep2Phi"][0],self.Branches["Lep2E"][0])
      vMET.SetPtEtaPhiM(self.Branches["met"][0],0,self.Branches["metPhi"][0],0)
      
      vLep1=vLep1+vMET+vLep2
      return vLep1.M()

    if ObjectName=="PtllMET":
      vLep1 = ROOT.TLorentzVector()
      vLep2 = ROOT.TLorentzVector()
      vMET = ROOT.TLorentzVector()

      vLep1.SetPtEtaPhiE(self.Branches["Lep1Pt"][0],self.Branches["Lep1Eta"][0],self.Branches["Lep1Phi"][0],self.Branches["Lep1E"][0])
      vLep2.SetPtEtaPhiE(self.Branches["Lep2Pt"][0],self.Branches["Lep2Eta"][0],self.Branches["Lep2Phi"][0],self.Branches["Lep2E"][0])
      vMET.SetPtEtaPhiM(self.Branches["met"][0],0,self.Branches["metPhi"][0],0)
      
      vLep1=vLep1+vMET+vLep2
      return vLep1.Pt()




    if ObjectName=="MT":
      vLep1 = ROOT.TLorentzVector()
      vMET = ROOT.TLorentzVector()
      
      ElecQ=0
      MuonQ=0
      Elec=[]
      Muon=[]

      if self.Branches["Lep1Flav"][0]==1:
        ElecQ+=self.Branches["Lep1Q"][0]
        Elec.append("Lep1")
      elif self.Branches["Lep1Flav"][0]==2:
        MuonQ+=self.Branches["Lep1Q"][0]
        Muon.append("Lep1")

      if self.Branches["Lep2Flav"][0]==1:
        ElecQ+=self.Branches["Lep2Q"][0]
        Elec.append("Lep2")

      elif self.Branches["Lep2Flav"][0]==2:
        MuonQ+=self.Branches["Lep2Q"][0]
        Muon.append("Lep2")

      if self.Branches["Lep3Flav"][0]==1:
        ElecQ+=self.Branches["Lep3Q"][0]
        Elec.append("Lep3")
      elif self.Branches["Lep3Flav"][0]==2:
        MuonQ+=self.Branches["Lep3Q"][0]
        Muon.append("Lep3")
        
      if abs(ElecQ)==0 and self.Branches["Nel"][0]==2 and self.Branches["Nmu"][0]==1 :
        vLep1.SetPtEtaPhiE(self.Branches[Muon[0]+"Pt"][0],self.Branches[Muon[0]+"Eta"][0],self.Branches[Muon[0]+"Phi"][0],self.Branches[Muon[0]+"E"][0])
      if abs(MuonQ)==0 and self.Branches["Nmu"][0]==2 and self.Branches["Nel"][0]==1 :
        vLep1.SetPtEtaPhiE(self.Branches[Elec[0]+"Pt"][0],self.Branches[Elec[0]+"Eta"][0],self.Branches[Elec[0]+"Phi"][0],self.Branches[Elec[0]+"E"][0])
      

      vMET.SetPtEtaPhiM(self.Branches["met"][0],0,self.Branches["metPhi"][0],0)
      return sqrt(2.*(vLep1.Pt() * vMET.Pt() - vLep1.Px() * vMET.Px() -  vLep1.Py() * vMET.Py()));


    # if ObjectName=="MTMET":
    #     vLep1 = ROOT.TLorentzVector()
    #     vJet1 = ROOT.TLorentzVector()
    #     vJet2 = ROOT.TLorentzVector()
    #     vMET = ROOT.TLorentzVector()
    #     vLep1.SetPtEtaPhiE(self.Branches["Lep1Pt"][0],self.Branches["Lep1Eta"][0],self.Branches["Lep1Phi"][0],self.Branches["Lep1E"][0])
    #     # vJet1.SetPtEtaPhiE(self.Branches["Jet1Pt"][0],self.Branches["Jet1Eta"][0],self.Branches["Jet1Phi"][0],self.Branches["Jet1E"][0])
    #     # vJet2.SetPtEtaPhiE(self.Branches["Jet2Pt"][0],self.Branches["Jet2Eta"][0],self.Branches["Jet2Phi"][0],self.Branches["Jet2E"][0])
    #     vMET.SetPtEtaPhiM(self.Branches["met"][0],0,self.Branches["metPhi"][0],0)

    #     #vMWWW = vLep1+vMET
    #     # vMWWW = vMWWW+vJet1
    #     # vMWWW = vMWWW + vJet2
    #     return sqrt(2.*(vLep1.Pt() * vMET.Pt() - vLep1.Px() * vMET.Px() -  vLep1.Py() * vMET.Py())) + self.Branches["met"][0];

    if ObjectName=="MTMET":
      # vLep1 = ROOT.TLorentzVector()
      # vJet1 = ROOT.TLorentzVector()
      # vJet2 = ROOT.TLorentzVector()
      # vMET = ROOT.TLorentzVector()
      # vLep1.SetPtEtaPhiE(self.Branches["Lep1Pt"][0],self.Branches["Lep1Eta"][0],self.Branches["Lep1Phi"][0],self.Branches["Lep1E"][0])
      # # vJet1.SetPtEtaPhiE(self.Branches["Jet1Pt"][0],self.Branches["Jet1Eta"][0],self.Branches["Jet1Phi"][0],self.Branches["Jet1E"][0])
      # # vJet2.SetPtEtaPhiE(self.Branches["Jet2Pt"][0],self.Branches["Jet2Eta"][0],self.Branches["Jet2Phi"][0],self.Branches["Jet2E"][0])
      # vMET.SetPtEtaPhiM(self.Branches["met"][0],0,self.Branches["metPhi"][0],0)

      # #vMWWW = vLep1+vMET
      # # vMWWW = vMWWW+vJet1
      # # vMWWW = vMWWW + vJet2
      return self.Branches["MT"][0] + self.Branches["met"][0];
    if ObjectName=="OFLep":
      
      ElecQ=0
      MuonQ=0
      Elec=[]
      Muon=[]

      if self.Branches["Lep1Flav"][0]==1:
        ElecQ+=self.Branches["Lep1Q"][0]
        Elec.append("Lep1")
      elif self.Branches["Lep1Flav"][0]==2:
        MuonQ+=self.Branches["Lep1Q"][0]
        Muon.append("Lep1")

      if self.Branches["Lep2Flav"][0]==1:
        ElecQ+=self.Branches["Lep2Q"][0]
        Elec.append("Lep2")

      elif self.Branches["Lep2Flav"][0]==2:
        MuonQ+=self.Branches["Lep2Q"][0]
        Muon.append("Lep2")

      if self.Branches["Lep3Flav"][0]==1:
        ElecQ+=self.Branches["Lep3Q"][0]
        Elec.append("Lep3")
      elif self.Branches["Lep3Flav"][0]==2:
        MuonQ+=self.Branches["Lep3Q"][0]
        Muon.append("Lep3")


      if abs(ElecQ)==0 and self.Branches["Nel"][0]==2 and self.Branches["Nmu"][0]==1 :            
        return self.Branches[Muon[0]+"Pt"][0]
      elif abs(MuonQ)==0 and self.Branches["Nel"][0]==1 and self.Branches["Nmu"][0]==2 :            
        return self.Branches[Elec[0]+"Pt"][0]
      else:
        return 0



    if ObjectName=="LepEl":
      
      Elec=[]


      if self.Branches["Lep1Flav"][0]==1:
        Elec.append("Lep1")
      if self.Branches["Lep2Flav"][0]==1:
        Elec.append("Lep2")
      if self.Branches["Lep3Flav"][0]==1:
        Elec.append("Lep3")

      if len(Elec)>0:
        return self.Branches[Elec[0]+"Pt"][0]
      else:
        return 0



    if ObjectName=="LoJ":
      if self.Branches["DRll"][0]>0:
        return self.Branches["DRjj"][0]/self.Branches["DRll"][0]
      else:
        return -1

    elif ObjectName=="LepJetEta":
      return abs(self.Branches["Lep1Eta"][0]-self.Branches["Jet1Eta"][0])

    elif ObjectName=="q0q1":
      #print self.Branches["Lep1Q"][0] *self.Branches["Lep2Q"][0]
      return self.Branches["Lep1Q"][0] *self.Branches["Lep2Q"][0]


    #If Object is not overriden and in the main TTree branch, just returns that.
    elif ObjectName in self.Branches.keys():
      return self.Branches[ObjectName][0]
    else:
      print "No objects found for ", ObjectName, "exiting"
      exit()



  #Defenition of Cuts and SR/CR
  def GetCuts(self,Region="WWW SR"):
    cuts = collections.OrderedDict()


    if Region == "WWW SR":
      cuts["passWWWSR"]=self.Branches["passWWWSR"][0]

    elif Region == "Side Band CR":
      cuts["SideBand"]=self.Branches["passSideBandCR"][0]

    elif Region == "Less2Jets":
      cuts["All"]=self.Branches["passl2jetCR"][0] 

    elif Region == "MjjFullold":
      cuts["SideBand"]=self.Branches["passWWWSRorigin"][0] or self.Branches["passSideBandCRorigin"][0]

    elif Region == "MjjFull":
      cuts["SideBand"]=self.Branches["passWWWSR"][0] or self.Branches["passSideBandCR"][0]

    elif Region == "MjjFullwoFwdEl":
      cuts["SideBand"]=self.Branches["passWWWSR"][0] or self.Branches["passSideBandCR"][0]
      cuts["FwdEl"] = abs(self.Branches["Mll_forwardelectron"][0] - 91.e3) > 15.e3 

    elif Region == "MjjFullwithFwdEl":
      cuts["SideBand"]=self.Branches["passWWWSR"][0] or self.Branches["passSideBandCR"][0]
      cuts["FwdEl"] = abs(self.Branches["Mll_forwardelectron"][0] - 91.e3) <= 15.e3 

    elif Region == "MjjFull Lep1EndCap":
      cuts["SideBand"]=self.Branches["passWWWSR"][0] or self.Branches["passSideBandCR"][0]
      cuts["Lep1EndCap"]= abs(self.Branches["Lep1Eta"][0]) >= 1.52

    elif Region == "MjjFull Lep2EndCap":
      cuts["SideBand"]=self.Branches["passWWWSR"][0] or self.Branches["passSideBandCR"][0]
      cuts["Lep2EndCap"]= abs(self.Branches["Lep2Eta"][0]) >= 1.52


    elif Region == "WZ 1SFOS CR":
      cuts["All"]=self.Branches["pass3lCR"][0] 

    elif Region == "WZ2j CR":
      cuts["All"]=self.Branches["pass3lCR"][0] 
      cuts["met"]=self.Branches["met"][0]>55.e3
      cuts["Mlll"]=self.Branches["Mlll"][0]>110.e3
      cuts["NJets"]=self.Branches["passNJets"][0]

    elif Region == "WZ CR":
      cuts["All"]=self.Branches["pass3lCR"][0] 
      cuts["met"]=self.Branches["met"][0]>55.e3
      cuts["Mlll"]=self.Branches["Mlll"][0]>110.e3

    elif Region == "WZ CRwoFwdEl":
      cuts["All"]=self.Branches["pass3lCR"][0] 
      cuts["met"]=self.Branches["met"][0]>55.e3
      cuts["Mlll"]=self.Branches["Mlll"][0]>110.e3
      cuts["FwdEl"] = abs(self.Branches["Mll_forwardelectron"][0] - 91.e3) > 15.e3 

    elif Region == "WZ Vgamma CR":
      cuts["Gamma"]=self.Branches["pass3lVgamma"][0] 


    elif Region == "BTag CR":
      cuts["All"]=self.Branches["passBtagCR"][0]

    elif Region == "BTag CR NonPrompt3":
      cuts["All"]=self.Branches["passBtagCR"][0]


    elif Region == "ZWindow CR":
      cuts["All"]=self.Branches["passZWindowCR"][0]


    elif Region == "3l SR":
      cuts["All"]=self.Branches["pass3lSR"][0]
      cuts["0SFOS"]=self.Branches["pass0SFOS"][0] or self.IsCF3Lep()


    elif Region == "3l 1B 1SFOS" :
      cuts["All"]=self.Branches["pass3lBtag"][0]
      MllSF=self.GetObject("MllSF")
      cuts["Checks"]= self.Branches["passJetEta"][0] and self.Branches["passJetPt"][0]# and self.Branches["Nbjets"][0]==1
      cuts["0SFOS"]=(self.Branches["pass1SFOS"][0] and (MllSF>110.e3 or MllSF<70.e3))


    elif Region == "3l 1B 1SFOS High" :
      cuts["All"]=self.Branches["pass3lBtag"][0]
      MllSF=self.GetObject("MllSF")
      cuts["Checks"]= self.Branches["passJetEta"][0] and self.Branches["passJetPt"][0] and self.Branches["Nbjets"][0]==1
      cuts["0SFOS"]=(self.Branches["pass1SFOS"][0] and (MllSF>110.e3))

    elif Region == "3l 1B 1SFOS Low" :
      cuts["All"]=self.Branches["pass3lBtag"][0]
      MllSF=self.GetObject("MllSF")
      cuts["Checks"]= self.Branches["passJetEta"][0] and self.Branches["passJetPt"][0] and self.Branches["Nbjets"][0]==1
      cuts["0SFOS"]=(self.Branches["pass1SFOS"][0] and (MllSF<70.e3))


    elif Region == "3l 1B ZWin" :
      cuts["All"]=self.Branches["pass3lBtag"][0]
      #cuts["TotalQ"]=abs(self.Branches["TotalQ"][0])==1
      #cuts["0SFOS"]=self.Branches["pass0SFOS"][0] or self.Branches["pass1SFOS"][0] 
      MllSF=self.GetObject("MllSF")
      cuts["Checks"]= self.Branches["passJetEta"][0] and self.Branches["passJetPt"][0] and self.Branches["Nbjets"][0]==1
      cuts["0SFOS"]=(self.Branches["pass1SFOS"][0] and (MllSF<110.e3 and MllSF>70.e3))


    elif Region == "3l 1B 0SFOS" :
      cuts["All"]=self.Branches["pass3lBtag"][0]
      #cuts["TotalQ"]=abs(self.Branches["TotalQ"][0])==1
      cuts["JetEta"]=self.Branches["passJetEta"][0]
      cuts["JetPt"]=self.Branches["passJetPt"][0]
      cuts["VetoBJet"]=self.Branches["Nbjets"][0]==1
      cuts["0SFOS"]=self.Branches["pass0SFOS"][0]# or self.Branches["pass1SFOS"][0] 




      #cuts["0SFOS"]=self.Branches["pass0SFOS"][0]



    elif Region == "3l 1B 1SFOS LowMET" :
      cuts["All"]=self.Branches["pass3lBtag"][0]
      cuts["MET"]=not self.Branches["passMET"][0]
      cuts["Checks"]= self.Branches["passJetEta"][0] and self.Branches["passJetPt"][0] and self.Branches["Nbjets"][0]==1
      cuts["0SFOS"]=self.Branches["pass1SFOS"][0] 

    elif Region == "3l 1B 1SFOS HighMET" :
      cuts["All"]=self.Branches["pass3lBtag"][0]
      cuts["MET"]=self.Branches["passMET"][0]
      cuts["Checks"]= self.Branches["passJetEta"][0] and self.Branches["passJetPt"][0] and self.Branches["Nbjets"][0]==1
      cuts["0SFOS"]=self.Branches["pass1SFOS"][0]





    elif Region == "CF CR":
      cuts["All"]=self.Branches["passCFCR"][0]
      cuts["Mll"]=self.Branches["Mll"][0]>60.e3 and self.Branches["Mll"][0]<120.e3

    
    elif Region == "Side Band Exp":
      cuts["All"]=True
      cuts["Trig"]=self.Branches["passTrig"][0]
      cuts["TrigMatch"]=self.Branches["passTrigMatch"][0]
      cuts["NJets"]=self.Branches["passNJets"][0]
      cuts["JetEta"]=self.Branches["passJetEta"][0]
      cuts["JetPt"]=self.Branches["passJetPt"][0]
      cuts["VetoBJet"]=self.Branches["passNBtag"][0]
      cuts["3Lepton"]=not self.Branches["pass3Lepton"][0] 
      cuts["NLep"]=self.Branches["Nleptons"][0]==2
      cuts["LeptonPt"]=self.Branches["passLeptonPt"][0]
      #cuts["NLooseLep"]=(self.Branches["Nlooseel"][0]+self.Branches["Nloosemu"][0])==0
      #cuts["SameSign"]=self.Branches["passSameSign"][0]
      cuts["Mll"]=self.Branches["passMll"][0]
      cuts["ZVeto"]= self.Branches["passZVeto"][0]
      cuts["MET"]=self.Branches["passMET"][0]
      cuts["Mjj"]=self.Branches["passWSide"][0]
      cuts["DEtaJJ"]=self.Branches["passDEtaJJ"][0]

    elif Region == "BTag77 CR":
      cuts["All"]=True
      cuts["Trig"]=self.Branches["passTrig"][0]
      cuts["TrigMatch"]=self.Branches["passTrigMatch"][0]
      cuts["NJets"]=self.Branches["passNJets"][0]
      cuts["JetPt"]=self.Branches["passJetPt"][0]
      cuts["JetEta"]=self.Branches["passJetEta"][0]
      cuts["VetoBJet"]=self.Branches["Nbjets77"][0]==1            
      cuts["3Lepton"]=not self.Branches["pass3Lepton"][0] 
      cuts["NLep"]=self.Branches["Nleptons"][0]==2
      #cuts["NLooseLep"]=(self.Branches["Nlooseel"][0]+self.Branches["Nloosemu"][0])==0
      #cuts["SameSign"]=self.Branches["passSameSign"][0]
      cuts["LeptonPt"]=self.Branches["passLeptonPt"][0]
      cuts["Mll"]=self.Branches["passMll"][0]
      cuts["ZVeto"]=self.Branches["passZVeto"][0]
      cuts["MET"]=self.Branches["passMET"][0] 
      cuts["Mjj"]=self.Branches["passMjj"][0] or self.Branches["passWSide"][0]
      cuts["DEtaJJ"]=self.Branches["passDEtaJJ"][0]







    elif Region == "MjjSS":
      cuts["All"]=True
      cuts["Trig"]=self.Branches["passTrig"][0]
      cuts["TrigMatch"]=self.Branches["passTrigMatch"][0]
      cuts["NJets"]=self.Branches["passNJets"][0]
      cuts["JetEta"]=self.Branches["passJetEta"][0]
      cuts["JetPt"]=self.Branches["passJetPt"][0]
      cuts["VetoBJet"]=self.Branches["passNBtag"][0]
      cuts["3Lepton"]=not self.Branches["pass3Lepton"][0] 
      cuts["NLep"]=self.Branches["Nleptons"][0]==2
      cuts["LeptonPt"]=self.Branches["passLeptonPt"][0]
      #cuts["NLooseLep"]=(self.Branches["Nlooseel"][0]+self.Branches["Nloosemu"][0])==0
      #cuts["SameSign"]=self.Branches["passSameSign"][0]
      cuts["Mll"]=self.Branches["passMll"][0]
      cuts["ZVeto"]= self.Branches["passZVeto"][0]
      cuts["MET"]=self.Branches["passMET"][0]
      cuts["Mjj"]=self.Branches["Mjj"][0]>300.e3
      cuts["DEtaJJ"]=self.Branches["passDEtaJJ"][0]





    elif Region == "3l SR EXP":
      
      cuts["All"]=True
      cuts["Trig"]=self.Branches["passTrig"][0]
      cuts["TrigMatch"]=self.Branches["passTrigMatch"][0]
      cuts["VetoBJet"]=self.Branches["Nbjets"][0]==0
      cuts["LeptonVeto"]=self.Branches["pass3Lepton"][0]
      cuts["LeptonVeto"]=not self.Branches["pass4Lepton"][0]
      cuts["LeptonPt"]=self.Branches["Lep1Pt"][0]>27.e3 
      cuts["TotalQ"]=abs(self.Branches["TotalQ"][0])==1
      cuts["0SFOS"]=self.Branches["pass0SFOS"][0] or self.IsCF3Lep()
      mllee = self.GetObject("Mllee")
      passMllee = (mllee > 100.e3 or mllee<80.e3) or self.Branches["Nel"][0]!=2
      cuts["Mllee"]= passMllee

    # elif Region == "3l 1B EXP" :
    #     cuts["All"]=True
    #     cuts["Trig"]=self.Branches["passTrig"][0]
    #     cuts["TrigMatch"]=self.Branches["passTrigMatch"][0]
    #     cuts["LeptonPt"]=self.Branches["Lep1Pt"][0]>27.e3 
    #     cuts["VetoBJet"]=self.Branches["Nbjets"][0]==1
    #     cuts["LeptonVeto"]=not self.Branches["pass4Lepton"][0]
    #     cuts["NLep"]=self.Branches["Nleptons"][0]==3
    #     cuts["TotalQ"]=abs(self.Branches["TotalQ"][0])==1
    #     cuts["0SFOS"]=self.Branches["pass0SFOS"][0] or self.IsCF3Lep()


    elif Region == "3l 1B77 0SFOS" :
      cuts["All"]=True
      cuts["Trig"]=self.Branches["passTrig"][0]
      cuts["TrigMatch"]=self.Branches["passTrigMatch"][0]
      cuts["LeptonPt"]=self.Branches["passLeptonPt"][0]
      cuts["VetoBJet"]=self.Branches["Nbjets77"][0]==1
      cuts["LeptonVeto"]=not self.Branches["pass4Lepton"][0]
      cuts["JetEta"]=self.Branches["passJetEta"][0]
      cuts["JetPt"]=self.Branches["passJetPt"][0]
      cuts["NLep"]=self.Branches["Nleptons"][0]==3
      cuts["TotalQ"]=abs(self.Branches["TotalQ"][0])==1
      cuts["0SFOS"]=self.Branches["pass0SFOS"][0]



    elif Region == "WZ VBS":    # modified by ws
      cuts["All"]=True
      cuts["Trig"]=self.Branches["passTrig"][0]
#      cuts["isNotBadJet"]=self.Branches["isNotBadJet"][0]
      cuts["1ZLepton"]=self.Branches["pass1ZLepton"][0]
      cuts["2ZLepton"]=self.Branches["pass2ZLepton"][0]
      cuts["3ZLepton"]=self.Branches["pass3ZLepton"][0]
      cuts["1WLepton"]=self.Branches["pass1WLepton"][0]
      cuts["LeptonPt"]=self.Branches["passLeptonPt"][0]
      cuts["ZZ veto"]=not self.Branches["pass4Lepton"][0] 
      cuts["SFOC"]=self.Branches["pass1SFOS"][0]
      cuts["Mll"]=fabs(self.Branches["mllSFOS"][0]-91.1876e3) < 10.e3 
      cuts["NJets"]=self.Branches["passNJets"][0]
      cuts["Mjj"]=self.Branches["Mjj"][0]>150.e3





    else:
      print "Region",Region,"not defined existing"
      exit()













    return cuts
