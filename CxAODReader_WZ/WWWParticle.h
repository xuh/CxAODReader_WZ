#ifndef CxAODReader_WZ_WWWParticle_H
#define CxAODReader_WZ_WWWParticle_H

#include "TLorentzVector.h"

#ifdef __MAKECINT__
#pragma link C++ class WWWParticle + ;
#endif

enum ParticleType { UNDEFINED, ELECTRON, MUON, JET };

class WWWParticle {
 public:
  // JL: testing
  int isPrompt = -1;
  int isFake = -1;
  int truthStatus = -1;

  TLorentzVector p4;
  //for WZ VBS
  bool isZ = false;
  bool isW = false;

  float innerTrackPt = 0.;
  ParticleType Type = ParticleType::UNDEFINED;
  bool TrigMatch = false;
  int charge = 0;

  float TopoEtCone20 = -999.;

  float PtCone20 = -999.;
  float PtVarCone20 = -999.;
  float PtVarCone30 = -999.;
  float PtVarCone40 = -999.;
  float PtVarCone20_TightTTVA_pt1000 = -999.;
  float PtVarCone30_TightTTVA_pt1000 = -999.;

  float d0 = 0;
  bool isLoose = false;
  bool isNoBL = false;
  float PromptVar = -1;
  float ECIDSBDT = 1;
  int Author = 0;
  int addAmbiguity = -2;
  WWWParticle(){};
  virtual ~WWWParticle(){};

  ClassDef(WWWParticle, 1);
};
#endif
