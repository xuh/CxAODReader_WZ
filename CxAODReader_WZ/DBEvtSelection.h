#ifndef CxAOD_DBEvtSelection_H
#define CxAOD_DBEvtSelection_H

#include <vector>

//#include "CxAODTools_VHbb/VHbbEvtSelection.h"
#include "CxAODTools/EventSelection.h"
//#include "CxAODTools_WWW/CommonProperties_WWW.h"

// structure holding kinematics of the result
struct DBResult {
  bool pass;
  std::vector<const xAOD::Electron*> el;
  std::vector<const xAOD::Electron*> fwdel;
  std::vector<const xAOD::Muon*> mu;
  std::vector<const xAOD::TauJet*> ta;
  const xAOD::MissingET* met;
  const xAOD::MissingET* mpt;
  //const xAOD::TauJet* tau;
  // const xAOD::Photon* pho;
  std::vector<const xAOD::Jet*> smallJets;
  // std::vector<const xAOD::Jet*> largeJets;
  // std::vector<const xAOD::Jet*> trackJets;
  // std::vector<const xAOD::TrackParticle*> TrackPart;
  // std::vector<const xAOD::TruthParticle*> truthParticles;
  // std::vector<const xAOD::Photon*> Photons;

  //temporary for compatibility
  // std::vector<const xAOD::Jet*> forwardJets;
  // std::vector<const xAOD::Jet*> signalJets;

  //bool isEl() { return el!=nullptr; }
};

class DBEvtSelection : public EventSelection {
 public:
  DBEvtSelection();
  //    DBEventSelection() = default;
  virtual ~DBEvtSelection() noexcept {}

  virtual DBResult result() { return m_result; }  // probably should be const ?

  //todo: add track jets to functions; check they are in the framework
  //These functions should be implemented in the 1,2,0,allhad derived classes
  // virtual bool passSelection(SelectionContainers& containers, bool isKinVar) = 0;

  // virtual bool passPreSelection(SelectionContainers& containers, bool isKinVar) = 0;

  // static bool sort_pt_phot(const xAOD::Photon* photA, const xAOD::Photon* photB) { return photA->pt() > photB->pt(); }

 protected:
  //need to decide on a lepton selection; for now they return 1
  //virtual int doDBLeptonSelection(const xAOD::ElectronContainer* electrons,
  //                           const xAOD::MuonContainer* muons,
  //                           const xAOD::Electron*& el1, const xAOD::Electron*& el2,
  //                           const xAOD::Muon*& mu1, const xAOD::Muon*& mu2);
  //virtual int doDBLeptonSelection(std::vector<const xAOD::Electron*>& el,
  //                           std::vector<const xAOD::Muon*>& mu);

  //virtual int doDBLeptonPreSelection(const xAOD::ElectronContainer* electrons,
  //                           const xAOD::MuonContainer* muons,
  //                           const xAOD::Electron*& el1, const xAOD::Electron*& el2,
  //                           const xAOD::Muon*& mu1, const xAOD::Muon*& mu2);
  //virtual int doDBLeptonPreSelection(const xAOD::ElectronContainer* electrons,
  //                           const xAOD::MuonContainer* muons,
  //                           std::vector<const xAOD::Electron*>& el,
  //                           std::vector<const xAOD::Muon*>& mu);

  virtual int doWWWLeptonPreSelection(const xAOD::ElectronContainer* electrons, const xAOD::ElectronContainer* forwardelectrons,
                                      const xAOD::MuonContainer* muons, const xAOD::TauJetContainer* taus,
                                      std::vector<const xAOD::Electron*>& el, std::vector<const xAOD::Electron*>& fwdel,
                                      std::vector<const xAOD::Muon*>& mu, std::vector<const xAOD::TauJet*>& ta);

  //    int doVHLeptonPreSelection(const xAOD::ElectronContainer* electrons,
  //			       const xAOD::MuonContainer* muons,
  //			       std::vector<const xAOD::Electron*>& el,
  //			       std::vector<const xAOD::Muon*>& mu);
  //    bool doVHJetSelection(const xAOD::JetContainer* jets,
  //			  const xAOD::JetContainer* fatjets,
  //			  const xAOD::JetContainer* trackjets);

  DBResult m_result;
  //    virtual bool passDBLeptonSelection(const xAOD::ElectronContainer* electrons,
  //                                     const xAOD::MuonContainer* muons,
  //                                     const xAOD::MissingET* met)=0;

  //    virtual bool passDBLeptonPreSelection(const xAOD::ElectronContainer* electrons,
  //                                        const xAOD::MuonContainer* muons,
  //                                        const xAOD::MissingET* met)=0;

  //move these to the different lepton implementations?
  //
  //virtual bool passKinematics();

  // TODO: merge these two definition
  // bool passJetCleaning(const xAOD::JetContainer* jets);
  // bool doJetCleaning(const xAOD::JetContainer* jets);

  virtual void clearResult();
  //move to derived class?
  //is it common to all selections? i.e. is there an unused memory location in the 1 lepton channel that there is no harm in calling clear() on?
};

#endif
