#ifndef CxAODReader_WZ_MVATree_WWW_H
#define CxAODReader_WZ_MVATree_WWW_H

#include <vector>
#include "CxAODReader/HistSvc.h"
#include "CxAODReader/MVATree.h"
#include "EventLoop/Worker.h"
#include "TFile.h"
#include "CxAODReader_WZ/WWWParticle.h"

#ifdef __MAKECINT__
#pragma link C++ class WWWParticle + ;
#pragma link C++ class vector < WWWParticle> + ;
#endif

class MVATree_WWW : public MVATree {
 protected:
  int m_analysisType;

  //virtual void AddBranch(TString name, Float_t* address, bool forReader);
  //virtual void AddBranch(TString name, Int_t* address, bool forReader);
  virtual void SetBranches() override;

  virtual void TransformVars() override;

 public:
  //  MVATree_lvqq(bool persistent, bool readMVA, int analysisType, EL::Worker *wk);
  MVATree_WWW(bool persistent, bool readMVA, int analysisType, EL::IWorker* wk, std::vector<std::string> variations,
              std::vector<HistSvc::WeightSyst>* weightSysts, bool nominalOnly);

  ~MVATree_WWW() {}

  virtual void Reset() override;
  virtual void Fill();
  virtual void ReadMVA();

  std::vector<HistSvc::WeightSyst>* m_weightSysts;
  EL::Worker* wk;  //

  Float_t mllSFOS;
  Float_t mtll;
  Float_t mtlll;
  Float_t mtLep1;
  Float_t mtLep2;
  Float_t mtLep3;
  Float_t dEtall;
  Float_t etalll;
  Float_t dPhill;
  Float_t philll;

  UInt_t runNumber;
  UInt_t eventNumber;
  Int_t Nleptons;
  Int_t Njets;
  Int_t Nbjets;
  Int_t Nbjets77;
  Int_t Nel;
  Int_t Nmu;

  Int_t NAllEl;
  Int_t NAllMu;

  Int_t NAllTa;
  Int_t Nloosetau;
  Int_t Nmediumtau;
  Int_t Ntighttau;
  Int_t TaunTracks;
  Float_t TauPt;
  Float_t TauE;
  Float_t TauEta;
  Float_t TauPhi;

  std::vector<int> truth_PdgId;
  Int_t Ntruthtau;

  bool passJetLepOverlap;
  Int_t AllJetLepOverlap;

  Int_t LepOFQ = 0;
  Int_t LepOFFlav = 0;
  bool LepOFisLoose = 0;
  bool LepOFisNoBL = 0;
  Float_t LepOFPt = 0;
  Float_t LepOFE = 0;
  Float_t LepOFEta = 0;
  Float_t LepOFPhi = 0;

  Int_t Lep1Q;
  Int_t Lep2Q;
  Int_t Lep3Q;

  Int_t TotalQ;

  // JL: testing
  Int_t Lep1isPrompt;
  Int_t Lep2isPrompt;
  Int_t Lep3isPrompt;
  Int_t Lep1isFake;
  Int_t Lep2isFake;
  Int_t Lep3isFake;
  Int_t Lep1truthStatus;
  Int_t Lep2truthStatus;
  Int_t Lep3truthStatus;

  bool Lep1isLoose;
  bool Lep2isLoose;
  bool Lep3isLoose;

  Int_t Lep1Author;
  Int_t Lep2Author;
  Int_t Lep3Author;

  bool passBasics;
  bool passWWWSR;
  bool passWWWSRorigin;
  bool passSideBandCR;
  bool passSideBandCRorigin;
  bool passBtagCR;
  bool passl2jetCR;
  bool pass3lCR;
  bool pass3lSR;
  bool pass3lBtag;
  bool pass3lVgamma;
  bool pass3lExpCR;
  bool passZWindowCR;
  bool passCFCR;

  Int_t Lep1Flav;
  Int_t Lep2Flav;
  Int_t Lep3Flav;

  Float_t vetoElectronPt;
  Float_t vetoElectronEta;
  Float_t vetoElectronPhi;

  Float_t vetoMuonPt;
  Float_t vetoMuonEta;
  Float_t vetoMuonPhi;

  Float_t Lep1PromptWeight;
  Float_t Lep2PromptWeight;
  Float_t LepPLVEl;
  Float_t LepPLVMu;
  Float_t Lep1ECIDSBDT;
  Float_t Lep2ECIDSBDT;
  Float_t LepECIDSBDT;

  Int_t LepAddAmbiguity;

  bool Lep1isNoBL;
  bool Lep2isNoBL;
  bool Lep3isNoBL;

  Int_t Jet1nrMuon;
  Int_t Jet1nrEl;
  Int_t Jet2nrMuon;
  Int_t Jet2nrEl;

  Float_t Jet1Pt;
  Float_t Jet1M;
  Float_t Jet1E;
  Float_t Jet1Eta;
  Float_t Jet1Phi;

  Float_t bJet1Pt;
  Float_t bJet1M;
  Float_t bJet1E;
  Float_t bJet1Eta;
  Float_t bJet1Phi;

  Float_t Jet2Pt;
  Float_t Jet2M;
  Float_t Jet2E;
  Float_t Jet2Eta;
  Float_t Jet2Phi;

  Float_t Lep1Pt;
  Float_t Lep1E;
  Float_t Lep1Eta;
  Float_t Lep1Phi;
  Float_t Lep1innerTrackPt;

  Float_t Lep2Pt;
  Float_t Lep2E;
  Float_t Lep2Eta;
  Float_t Lep2Phi;
  Float_t Lep2innerTrackPt;

  Float_t Lep1d0;
  Float_t Lep2d0;

  Float_t Lep1TopoEtCone20;
  Float_t Lep1PtCone20;
  Float_t Lep1PtVarCone20;
  Float_t Lep1PtVarCone30;
  Float_t Lep1PtVarCone40;
  Float_t Lep1PtVarCone20_TightTTVA_pt1000;
  Float_t Lep1PtVarCone30_TightTTVA_pt1000;

  Float_t Lep2TopoEtCone20;
  Float_t Lep2PtCone20;
  Float_t Lep2PtVarCone20;
  Float_t Lep2PtVarCone30;
  Float_t Lep2PtVarCone40;
  Float_t Lep2PtVarCone20_TightTTVA_pt1000;
  Float_t Lep2PtVarCone30_TightTTVA_pt1000;

  Float_t LooseMuPt;
  Float_t LooseMuE;
  Float_t LooseMuEta;
  Float_t LooseMuPhi;

  Float_t LooseElPt;
  Float_t LooseElE;
  Float_t LooseElEta;
  Float_t LooseElPhi;

  Float_t Lep3Pt;
  Float_t Lep3E;
  Float_t Lep3Eta;
  Float_t Lep3Phi;
  Float_t Lep3innerTrackPt;

  Float_t tLep1Pt;
  Float_t tLep1E;
  Float_t tLep1Eta;
  Float_t tLep1Phi;
  int tLep1Type;

  Float_t tLep2Pt;
  Float_t tLep2E;
  Float_t tLep2Eta;
  Float_t tLep2Phi;
  int tLep2Type;

  Float_t tLep3Pt;
  Float_t tLep3E;
  Float_t tLep3Eta;
  Float_t tLep3Phi;
  int tLep3Type;

  Float_t tNeut1Pt;
  Float_t tNeut1E;
  Float_t tNeut1Eta;
  Float_t tNeut1Phi;
  int tNeut1Type;

  Float_t tNeut2Pt;
  Float_t tNeut2E;
  Float_t tNeut2Eta;
  Float_t tNeut2Phi;
  int tNeut2Type;

  Float_t QCDweight;
  Float_t CFweight;
  Float_t CFStatSys;
  Float_t CFwBG;
  Int_t Nloose;
  Int_t Nlooseel;
  Int_t NNoBLel;
  Int_t Nloosemu;

  Float_t met;
  Float_t metSig;
  Float_t metSigRatio;
  Float_t mpt;
  Float_t MT;
  Float_t MTtrack;
  Float_t metPhi;
  Float_t mptPhi;
  Float_t weight;

  Float_t weightMC;
  Float_t weightSys;
  Float_t weightJVT;
  Float_t weightBtag;
  Float_t weightEffSF;
  Float_t weightEffReco;
  Float_t weightIso;
  Float_t weightTTVA;
  Float_t weightTrig;

  Float_t Mll;
  Float_t Mllee;
  Float_t Mllmm;
  Float_t MllSF;
  Float_t Mll_forwardelectron;
  Float_t forwardElEta;
  Float_t forwardElPt;
  Float_t Mlll;
  Float_t Mjj;
  Float_t MWWW;

  Float_t Ptll;
  Float_t Ptjj;
  Float_t PtWWW;

  Float_t Etall;
  Float_t Etajj;
  Float_t DEtajj;
  Float_t EtaWWW;

  Float_t Phill;
  Float_t Phijj;
  Float_t PhiWWW;

  Float_t SumEta;
  Float_t SumPt;

  Float_t DRjj;
  Float_t DRll;
  Float_t DRj1l1;
  Float_t DRj2l2;
  Float_t DRj1l2;
  Float_t DRj2l1;

  bool passSameSign;
  bool pass3lTT;
  bool pass0SFOS;
  bool pass1SFOS;

  bool pass1ZLepton;
  bool pass1Lepton;
  bool pass2Lepton;
  bool isNotBadJet;
  bool pass1WLepton;
  bool pass2ZLepton;
  bool pass3ZLepton;
  bool pass3Lepton;
  bool pass3Leptonorigin;
  bool pass4Lepton;
  bool pass3LRegion;
  bool passMET;
  bool passNJets;
  bool passLeptonPt;
  bool passLeptonPtLow;
  bool passJetPt;
  bool passJetEta;
  bool passMjj;
  bool passWSide;
  bool passDEtaJJ;
  bool passMll;
  bool passZVeto;
  bool passNBtag;
  bool passTrig;
  bool passTrigMatch;
  bool passLepJetPhi;
  bool passLepbJetPhi;
  bool passAuthor;
  bool passPhoton;

  float LepJetPhi;
  float LepMPTPhi;

  int Jet1NTrkPt500PV;
  int Jet1NTrkPt1000PV;

  int Jet2NTrkPt500PV;
  int Jet2NTrkPt1000PV;

  std::vector<WWWParticle> Leptons;  //  = new std::vector<xAOD::Muon>;
  /* std::vector<WWWParticle> Electrons; // = new std::vector<xAOD::Electron>;  */
  std::vector<WWWParticle> Jets;  // = new std::vector<xAOD::Electron>;
};

#endif
