import os
import sys

directory= str(sys.argv[1])
directory=" "+directory
fList = os.listdir(str(sys.argv[1]))

DSIDS={}
for sfile in fList:
    if "mc16_13" in sfile:
        DSID = sfile.split("mc16_13TeV.")[1].split(".")[0]
        print DSID
        
        if not DSID in DSIDS.keys():
            DSIDS[DSID]=[sfile]
        else:
            DSIDS[DSID].append(sfile)

print DSIDS

for DSID in DSIDS.keys():

    files=DSIDS[DSID]
    index=0
    print
    dorun=True
    while dorun:
        index+=1
        if len(files)>25:
            
            tmpfiles = files[0:25]
            files = files[25:]
        else:
            tmpfiles=files
            dorun=False
        
        command = "hadd "+"user.isiral.mc16_13TeV."+DSID+".ISMET._CxAOD-"+str(index)+".root"+directory+directory.join(tmpfiles)
        
        os.system(command)

